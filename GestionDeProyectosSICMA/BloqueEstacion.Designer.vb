﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class BloqueEstacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BloqueEstacion))
        Me.FF28 = New System.Windows.Forms.DateTimePicker()
        Me.FF27 = New System.Windows.Forms.DateTimePicker()
        Me.FF26 = New System.Windows.Forms.DateTimePicker()
        Me.FF25 = New System.Windows.Forms.DateTimePicker()
        Me.FF24 = New System.Windows.Forms.DateTimePicker()
        Me.FF23 = New System.Windows.Forms.DateTimePicker()
        Me.FF22 = New System.Windows.Forms.DateTimePicker()
        Me.FF21 = New System.Windows.Forms.DateTimePicker()
        Me.FF20 = New System.Windows.Forms.DateTimePicker()
        Me.FF19 = New System.Windows.Forms.DateTimePicker()
        Me.FF18 = New System.Windows.Forms.DateTimePicker()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.FF17 = New System.Windows.Forms.DateTimePicker()
        Me.FI28 = New System.Windows.Forms.DateTimePicker()
        Me.FI27 = New System.Windows.Forms.DateTimePicker()
        Me.FI26 = New System.Windows.Forms.DateTimePicker()
        Me.FI25 = New System.Windows.Forms.DateTimePicker()
        Me.FI24 = New System.Windows.Forms.DateTimePicker()
        Me.FI23 = New System.Windows.Forms.DateTimePicker()
        Me.FI22 = New System.Windows.Forms.DateTimePicker()
        Me.FI21 = New System.Windows.Forms.DateTimePicker()
        Me.FI20 = New System.Windows.Forms.DateTimePicker()
        Me.FI19 = New System.Windows.Forms.DateTimePicker()
        Me.FI18 = New System.Windows.Forms.DateTimePicker()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.FI17 = New System.Windows.Forms.DateTimePicker()
        Me.H17 = New System.Windows.Forms.TextBox()
        Me.H18 = New System.Windows.Forms.TextBox()
        Me.H19 = New System.Windows.Forms.TextBox()
        Me.H20 = New System.Windows.Forms.TextBox()
        Me.H21 = New System.Windows.Forms.TextBox()
        Me.H22 = New System.Windows.Forms.TextBox()
        Me.H23 = New System.Windows.Forms.TextBox()
        Me.H24 = New System.Windows.Forms.TextBox()
        Me.H25 = New System.Windows.Forms.TextBox()
        Me.H26 = New System.Windows.Forms.TextBox()
        Me.H27 = New System.Windows.Forms.TextBox()
        Me.H28 = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.FF16 = New System.Windows.Forms.DateTimePicker()
        Me.FF15 = New System.Windows.Forms.DateTimePicker()
        Me.FF14 = New System.Windows.Forms.DateTimePicker()
        Me.FF13 = New System.Windows.Forms.DateTimePicker()
        Me.FF12 = New System.Windows.Forms.DateTimePicker()
        Me.FF11 = New System.Windows.Forms.DateTimePicker()
        Me.FF10 = New System.Windows.Forms.DateTimePicker()
        Me.FF9 = New System.Windows.Forms.DateTimePicker()
        Me.FF8 = New System.Windows.Forms.DateTimePicker()
        Me.FF7 = New System.Windows.Forms.DateTimePicker()
        Me.FF6 = New System.Windows.Forms.DateTimePicker()
        Me.FF5 = New System.Windows.Forms.DateTimePicker()
        Me.FF4 = New System.Windows.Forms.DateTimePicker()
        Me.FF3 = New System.Windows.Forms.DateTimePicker()
        Me.FF2 = New System.Windows.Forms.DateTimePicker()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.FF1 = New System.Windows.Forms.DateTimePicker()
        Me.FI16 = New System.Windows.Forms.DateTimePicker()
        Me.FI15 = New System.Windows.Forms.DateTimePicker()
        Me.FI14 = New System.Windows.Forms.DateTimePicker()
        Me.FI13 = New System.Windows.Forms.DateTimePicker()
        Me.FI12 = New System.Windows.Forms.DateTimePicker()
        Me.FI11 = New System.Windows.Forms.DateTimePicker()
        Me.FI10 = New System.Windows.Forms.DateTimePicker()
        Me.FI9 = New System.Windows.Forms.DateTimePicker()
        Me.FI8 = New System.Windows.Forms.DateTimePicker()
        Me.FI7 = New System.Windows.Forms.DateTimePicker()
        Me.FI6 = New System.Windows.Forms.DateTimePicker()
        Me.FI5 = New System.Windows.Forms.DateTimePicker()
        Me.FI4 = New System.Windows.Forms.DateTimePicker()
        Me.FI3 = New System.Windows.Forms.DateTimePicker()
        Me.FI2 = New System.Windows.Forms.DateTimePicker()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.FI1 = New System.Windows.Forms.DateTimePicker()
        Me.H16 = New System.Windows.Forms.TextBox()
        Me.H15 = New System.Windows.Forms.TextBox()
        Me.H14 = New System.Windows.Forms.TextBox()
        Me.H13 = New System.Windows.Forms.TextBox()
        Me.H12 = New System.Windows.Forms.TextBox()
        Me.H11 = New System.Windows.Forms.TextBox()
        Me.H10 = New System.Windows.Forms.TextBox()
        Me.H9 = New System.Windows.Forms.TextBox()
        Me.H8 = New System.Windows.Forms.TextBox()
        Me.H7 = New System.Windows.Forms.TextBox()
        Me.H6 = New System.Windows.Forms.TextBox()
        Me.H5 = New System.Windows.Forms.TextBox()
        Me.H4 = New System.Windows.Forms.TextBox()
        Me.H3 = New System.Windows.Forms.TextBox()
        Me.H2 = New System.Windows.Forms.TextBox()
        Me.H1 = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.BE28 = New System.Windows.Forms.Label()
        Me.CheckBox28 = New System.Windows.Forms.CheckBox()
        Me.BE27 = New System.Windows.Forms.Label()
        Me.CheckBox27 = New System.Windows.Forms.CheckBox()
        Me.BE26 = New System.Windows.Forms.Label()
        Me.CheckBox26 = New System.Windows.Forms.CheckBox()
        Me.BE25 = New System.Windows.Forms.Label()
        Me.CheckBox25 = New System.Windows.Forms.CheckBox()
        Me.BE24 = New System.Windows.Forms.Label()
        Me.CheckBox24 = New System.Windows.Forms.CheckBox()
        Me.BE23 = New System.Windows.Forms.Label()
        Me.CheckBox23 = New System.Windows.Forms.CheckBox()
        Me.BE22 = New System.Windows.Forms.Label()
        Me.CheckBox22 = New System.Windows.Forms.CheckBox()
        Me.BE21 = New System.Windows.Forms.Label()
        Me.CheckBox21 = New System.Windows.Forms.CheckBox()
        Me.BE20 = New System.Windows.Forms.Label()
        Me.CheckBox20 = New System.Windows.Forms.CheckBox()
        Me.BE19 = New System.Windows.Forms.Label()
        Me.CheckBox19 = New System.Windows.Forms.CheckBox()
        Me.BE18 = New System.Windows.Forms.Label()
        Me.CheckBox18 = New System.Windows.Forms.CheckBox()
        Me.BE17 = New System.Windows.Forms.Label()
        Me.CheckBox17 = New System.Windows.Forms.CheckBox()
        Me.BE16 = New System.Windows.Forms.Label()
        Me.CheckBox16 = New System.Windows.Forms.CheckBox()
        Me.BE15 = New System.Windows.Forms.Label()
        Me.CheckBox15 = New System.Windows.Forms.CheckBox()
        Me.BE14 = New System.Windows.Forms.Label()
        Me.CheckBox14 = New System.Windows.Forms.CheckBox()
        Me.BE13 = New System.Windows.Forms.Label()
        Me.CheckBox13 = New System.Windows.Forms.CheckBox()
        Me.BE12 = New System.Windows.Forms.Label()
        Me.CheckBox12 = New System.Windows.Forms.CheckBox()
        Me.BE11 = New System.Windows.Forms.Label()
        Me.CheckBox11 = New System.Windows.Forms.CheckBox()
        Me.BE10 = New System.Windows.Forms.Label()
        Me.CheckBox10 = New System.Windows.Forms.CheckBox()
        Me.BE9 = New System.Windows.Forms.Label()
        Me.CheckBox9 = New System.Windows.Forms.CheckBox()
        Me.BE8 = New System.Windows.Forms.Label()
        Me.CheckBox8 = New System.Windows.Forms.CheckBox()
        Me.BE7 = New System.Windows.Forms.Label()
        Me.CheckBox7 = New System.Windows.Forms.CheckBox()
        Me.BE6 = New System.Windows.Forms.Label()
        Me.CheckBox6 = New System.Windows.Forms.CheckBox()
        Me.BE5 = New System.Windows.Forms.Label()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.BE4 = New System.Windows.Forms.Label()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.BE3 = New System.Windows.Forms.Label()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.BE2 = New System.Windows.Forms.Label()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.BE1 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.BE = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.NB = New System.Windows.Forms.TextBox()
        Me.GC = New System.Windows.Forms.Button()
        Me.JCBE = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'FF28
        '
        Me.FF28.CustomFormat = "yyyy-MM-dd"
        Me.FF28.Enabled = False
        Me.FF28.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF28.Location = New System.Drawing.Point(1076, 466)
        Me.FF28.Name = "FF28"
        Me.FF28.Size = New System.Drawing.Size(95, 20)
        Me.FF28.TabIndex = 418
        Me.FF28.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF27
        '
        Me.FF27.CustomFormat = "yyyy-MM-dd"
        Me.FF27.Enabled = False
        Me.FF27.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF27.Location = New System.Drawing.Point(1076, 440)
        Me.FF27.Name = "FF27"
        Me.FF27.Size = New System.Drawing.Size(95, 20)
        Me.FF27.TabIndex = 417
        Me.FF27.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF26
        '
        Me.FF26.CustomFormat = "yyyy-MM-dd"
        Me.FF26.Enabled = False
        Me.FF26.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF26.Location = New System.Drawing.Point(1076, 414)
        Me.FF26.Name = "FF26"
        Me.FF26.Size = New System.Drawing.Size(95, 20)
        Me.FF26.TabIndex = 416
        Me.FF26.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF25
        '
        Me.FF25.CustomFormat = "yyyy-MM-dd"
        Me.FF25.Enabled = False
        Me.FF25.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF25.Location = New System.Drawing.Point(1076, 388)
        Me.FF25.Name = "FF25"
        Me.FF25.Size = New System.Drawing.Size(95, 20)
        Me.FF25.TabIndex = 415
        Me.FF25.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF24
        '
        Me.FF24.CustomFormat = "yyyy-MM-dd"
        Me.FF24.Enabled = False
        Me.FF24.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF24.Location = New System.Drawing.Point(1076, 362)
        Me.FF24.Name = "FF24"
        Me.FF24.Size = New System.Drawing.Size(95, 20)
        Me.FF24.TabIndex = 414
        Me.FF24.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF23
        '
        Me.FF23.CustomFormat = "yyyy-MM-dd"
        Me.FF23.Enabled = False
        Me.FF23.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF23.Location = New System.Drawing.Point(1076, 335)
        Me.FF23.Name = "FF23"
        Me.FF23.Size = New System.Drawing.Size(95, 20)
        Me.FF23.TabIndex = 413
        Me.FF23.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF22
        '
        Me.FF22.CustomFormat = "yyyy-MM-dd"
        Me.FF22.Enabled = False
        Me.FF22.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF22.Location = New System.Drawing.Point(1076, 310)
        Me.FF22.Name = "FF22"
        Me.FF22.Size = New System.Drawing.Size(95, 20)
        Me.FF22.TabIndex = 412
        Me.FF22.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF21
        '
        Me.FF21.CustomFormat = "yyyy-MM-dd"
        Me.FF21.Enabled = False
        Me.FF21.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF21.Location = New System.Drawing.Point(1076, 285)
        Me.FF21.Name = "FF21"
        Me.FF21.Size = New System.Drawing.Size(95, 20)
        Me.FF21.TabIndex = 411
        Me.FF21.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF20
        '
        Me.FF20.CustomFormat = "yyyy-MM-dd"
        Me.FF20.Enabled = False
        Me.FF20.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF20.Location = New System.Drawing.Point(1076, 259)
        Me.FF20.Name = "FF20"
        Me.FF20.Size = New System.Drawing.Size(95, 20)
        Me.FF20.TabIndex = 410
        Me.FF20.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF19
        '
        Me.FF19.CustomFormat = "yyyy-MM-dd"
        Me.FF19.Enabled = False
        Me.FF19.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF19.Location = New System.Drawing.Point(1076, 233)
        Me.FF19.Name = "FF19"
        Me.FF19.Size = New System.Drawing.Size(95, 20)
        Me.FF19.TabIndex = 409
        Me.FF19.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF18
        '
        Me.FF18.CustomFormat = "yyyy-MM-dd"
        Me.FF18.Enabled = False
        Me.FF18.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF18.Location = New System.Drawing.Point(1076, 206)
        Me.FF18.Name = "FF18"
        Me.FF18.Size = New System.Drawing.Size(95, 20)
        Me.FF18.TabIndex = 408
        Me.FF18.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label38.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label38.Location = New System.Drawing.Point(1099, 84)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(59, 38)
        Me.Label38.TabIndex = 407
        Me.Label38.Text = "Fecha " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  Fin" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'FF17
        '
        Me.FF17.CustomFormat = "yyyy-MM-dd"
        Me.FF17.Enabled = False
        Me.FF17.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF17.Location = New System.Drawing.Point(1076, 180)
        Me.FF17.Name = "FF17"
        Me.FF17.Size = New System.Drawing.Size(95, 20)
        Me.FF17.TabIndex = 406
        Me.FF17.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI28
        '
        Me.FI28.CustomFormat = "yyyy-MM-dd"
        Me.FI28.Enabled = False
        Me.FI28.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI28.Location = New System.Drawing.Point(977, 466)
        Me.FI28.Name = "FI28"
        Me.FI28.Size = New System.Drawing.Size(95, 20)
        Me.FI28.TabIndex = 401
        Me.FI28.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI27
        '
        Me.FI27.CustomFormat = "yyyy-MM-dd"
        Me.FI27.Enabled = False
        Me.FI27.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI27.Location = New System.Drawing.Point(977, 440)
        Me.FI27.Name = "FI27"
        Me.FI27.Size = New System.Drawing.Size(95, 20)
        Me.FI27.TabIndex = 400
        Me.FI27.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI26
        '
        Me.FI26.CustomFormat = "yyyy-MM-dd"
        Me.FI26.Enabled = False
        Me.FI26.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI26.Location = New System.Drawing.Point(977, 414)
        Me.FI26.Name = "FI26"
        Me.FI26.Size = New System.Drawing.Size(95, 20)
        Me.FI26.TabIndex = 399
        Me.FI26.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI25
        '
        Me.FI25.CustomFormat = "yyyy-MM-dd"
        Me.FI25.Enabled = False
        Me.FI25.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI25.Location = New System.Drawing.Point(977, 388)
        Me.FI25.Name = "FI25"
        Me.FI25.Size = New System.Drawing.Size(95, 20)
        Me.FI25.TabIndex = 398
        Me.FI25.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI24
        '
        Me.FI24.CustomFormat = "yyyy-MM-dd"
        Me.FI24.Enabled = False
        Me.FI24.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI24.Location = New System.Drawing.Point(977, 362)
        Me.FI24.Name = "FI24"
        Me.FI24.Size = New System.Drawing.Size(95, 20)
        Me.FI24.TabIndex = 397
        Me.FI24.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI23
        '
        Me.FI23.CustomFormat = "yyyy-MM-dd"
        Me.FI23.Enabled = False
        Me.FI23.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI23.Location = New System.Drawing.Point(977, 335)
        Me.FI23.Name = "FI23"
        Me.FI23.Size = New System.Drawing.Size(95, 20)
        Me.FI23.TabIndex = 396
        Me.FI23.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI22
        '
        Me.FI22.CustomFormat = "yyyy-MM-dd"
        Me.FI22.Enabled = False
        Me.FI22.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI22.Location = New System.Drawing.Point(977, 310)
        Me.FI22.Name = "FI22"
        Me.FI22.Size = New System.Drawing.Size(95, 20)
        Me.FI22.TabIndex = 395
        Me.FI22.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI21
        '
        Me.FI21.CustomFormat = "yyyy-MM-dd"
        Me.FI21.Enabled = False
        Me.FI21.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI21.Location = New System.Drawing.Point(977, 285)
        Me.FI21.Name = "FI21"
        Me.FI21.Size = New System.Drawing.Size(95, 20)
        Me.FI21.TabIndex = 394
        Me.FI21.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI20
        '
        Me.FI20.CustomFormat = "yyyy-MM-dd"
        Me.FI20.Enabled = False
        Me.FI20.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI20.Location = New System.Drawing.Point(977, 259)
        Me.FI20.Name = "FI20"
        Me.FI20.Size = New System.Drawing.Size(95, 20)
        Me.FI20.TabIndex = 393
        Me.FI20.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI19
        '
        Me.FI19.CustomFormat = "yyyy-MM-dd"
        Me.FI19.Enabled = False
        Me.FI19.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI19.Location = New System.Drawing.Point(977, 233)
        Me.FI19.Name = "FI19"
        Me.FI19.Size = New System.Drawing.Size(95, 20)
        Me.FI19.TabIndex = 392
        Me.FI19.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI18
        '
        Me.FI18.CustomFormat = "yyyy-MM-dd"
        Me.FI18.Enabled = False
        Me.FI18.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI18.Location = New System.Drawing.Point(977, 206)
        Me.FI18.Name = "FI18"
        Me.FI18.Size = New System.Drawing.Size(95, 20)
        Me.FI18.TabIndex = 391
        Me.FI18.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label39.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label39.Location = New System.Drawing.Point(1000, 84)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(59, 38)
        Me.Label39.TabIndex = 390
        Me.Label39.Text = "Fecha " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Inicio"
        '
        'FI17
        '
        Me.FI17.CustomFormat = "yyyy-MM-dd"
        Me.FI17.Enabled = False
        Me.FI17.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI17.Location = New System.Drawing.Point(977, 180)
        Me.FI17.Name = "FI17"
        Me.FI17.Size = New System.Drawing.Size(95, 20)
        Me.FI17.TabIndex = 389
        Me.FI17.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'H17
        '
        Me.H17.Enabled = False
        Me.H17.Location = New System.Drawing.Point(897, 180)
        Me.H17.Name = "H17"
        Me.H17.Size = New System.Drawing.Size(69, 20)
        Me.H17.TabIndex = 388
        '
        'H18
        '
        Me.H18.Enabled = False
        Me.H18.Location = New System.Drawing.Point(897, 205)
        Me.H18.Name = "H18"
        Me.H18.Size = New System.Drawing.Size(69, 20)
        Me.H18.TabIndex = 387
        '
        'H19
        '
        Me.H19.Enabled = False
        Me.H19.Location = New System.Drawing.Point(897, 231)
        Me.H19.Name = "H19"
        Me.H19.Size = New System.Drawing.Size(69, 20)
        Me.H19.TabIndex = 386
        '
        'H20
        '
        Me.H20.Enabled = False
        Me.H20.Location = New System.Drawing.Point(898, 257)
        Me.H20.Name = "H20"
        Me.H20.Size = New System.Drawing.Size(69, 20)
        Me.H20.TabIndex = 385
        '
        'H21
        '
        Me.H21.Enabled = False
        Me.H21.Location = New System.Drawing.Point(897, 283)
        Me.H21.Name = "H21"
        Me.H21.Size = New System.Drawing.Size(69, 20)
        Me.H21.TabIndex = 384
        '
        'H22
        '
        Me.H22.Enabled = False
        Me.H22.Location = New System.Drawing.Point(898, 309)
        Me.H22.Name = "H22"
        Me.H22.Size = New System.Drawing.Size(69, 20)
        Me.H22.TabIndex = 383
        '
        'H23
        '
        Me.H23.Enabled = False
        Me.H23.Location = New System.Drawing.Point(898, 335)
        Me.H23.Name = "H23"
        Me.H23.Size = New System.Drawing.Size(69, 20)
        Me.H23.TabIndex = 382
        '
        'H24
        '
        Me.H24.Enabled = False
        Me.H24.Location = New System.Drawing.Point(898, 361)
        Me.H24.Name = "H24"
        Me.H24.Size = New System.Drawing.Size(69, 20)
        Me.H24.TabIndex = 381
        '
        'H25
        '
        Me.H25.Enabled = False
        Me.H25.Location = New System.Drawing.Point(898, 388)
        Me.H25.Name = "H25"
        Me.H25.Size = New System.Drawing.Size(69, 20)
        Me.H25.TabIndex = 380
        '
        'H26
        '
        Me.H26.Enabled = False
        Me.H26.Location = New System.Drawing.Point(898, 413)
        Me.H26.Name = "H26"
        Me.H26.Size = New System.Drawing.Size(69, 20)
        Me.H26.TabIndex = 379
        '
        'H27
        '
        Me.H27.Enabled = False
        Me.H27.Location = New System.Drawing.Point(897, 439)
        Me.H27.Name = "H27"
        Me.H27.Size = New System.Drawing.Size(69, 20)
        Me.H27.TabIndex = 378
        '
        'H28
        '
        Me.H28.Enabled = False
        Me.H28.Location = New System.Drawing.Point(898, 465)
        Me.H28.Name = "H28"
        Me.H28.Size = New System.Drawing.Size(69, 20)
        Me.H28.TabIndex = 377
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label40.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label40.Location = New System.Drawing.Point(902, 84)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(64, 19)
        Me.Label40.TabIndex = 372
        Me.Label40.Text = "  Horas"
        '
        'FF16
        '
        Me.FF16.CustomFormat = "yyyy-MM-dd"
        Me.FF16.Enabled = False
        Me.FF16.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF16.Location = New System.Drawing.Point(1076, 155)
        Me.FF16.Name = "FF16"
        Me.FF16.Size = New System.Drawing.Size(95, 20)
        Me.FF16.TabIndex = 371
        Me.FF16.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF15
        '
        Me.FF15.CustomFormat = "yyyy-MM-dd"
        Me.FF15.Enabled = False
        Me.FF15.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF15.Location = New System.Drawing.Point(1076, 129)
        Me.FF15.Name = "FF15"
        Me.FF15.Size = New System.Drawing.Size(95, 20)
        Me.FF15.TabIndex = 370
        Me.FF15.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF14
        '
        Me.FF14.CustomFormat = "yyyy-MM-dd"
        Me.FF14.Enabled = False
        Me.FF14.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF14.Location = New System.Drawing.Point(513, 488)
        Me.FF14.Name = "FF14"
        Me.FF14.Size = New System.Drawing.Size(95, 20)
        Me.FF14.TabIndex = 369
        Me.FF14.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF13
        '
        Me.FF13.CustomFormat = "yyyy-MM-dd"
        Me.FF13.Enabled = False
        Me.FF13.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF13.Location = New System.Drawing.Point(513, 462)
        Me.FF13.Name = "FF13"
        Me.FF13.Size = New System.Drawing.Size(95, 20)
        Me.FF13.TabIndex = 368
        Me.FF13.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF12
        '
        Me.FF12.CustomFormat = "yyyy-MM-dd"
        Me.FF12.Enabled = False
        Me.FF12.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF12.Location = New System.Drawing.Point(513, 436)
        Me.FF12.Name = "FF12"
        Me.FF12.Size = New System.Drawing.Size(95, 20)
        Me.FF12.TabIndex = 367
        Me.FF12.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF11
        '
        Me.FF11.CustomFormat = "yyyy-MM-dd"
        Me.FF11.Enabled = False
        Me.FF11.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF11.Location = New System.Drawing.Point(513, 410)
        Me.FF11.Name = "FF11"
        Me.FF11.Size = New System.Drawing.Size(95, 20)
        Me.FF11.TabIndex = 366
        Me.FF11.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF10
        '
        Me.FF10.CustomFormat = "yyyy-MM-dd"
        Me.FF10.Enabled = False
        Me.FF10.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF10.Location = New System.Drawing.Point(513, 384)
        Me.FF10.Name = "FF10"
        Me.FF10.Size = New System.Drawing.Size(95, 20)
        Me.FF10.TabIndex = 365
        Me.FF10.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF9
        '
        Me.FF9.CustomFormat = "yyyy-MM-dd"
        Me.FF9.Enabled = False
        Me.FF9.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF9.Location = New System.Drawing.Point(513, 358)
        Me.FF9.Name = "FF9"
        Me.FF9.Size = New System.Drawing.Size(95, 20)
        Me.FF9.TabIndex = 364
        Me.FF9.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF8
        '
        Me.FF8.CustomFormat = "yyyy-MM-dd"
        Me.FF8.Enabled = False
        Me.FF8.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF8.Location = New System.Drawing.Point(513, 332)
        Me.FF8.Name = "FF8"
        Me.FF8.Size = New System.Drawing.Size(95, 20)
        Me.FF8.TabIndex = 363
        Me.FF8.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF7
        '
        Me.FF7.CustomFormat = "yyyy-MM-dd"
        Me.FF7.Enabled = False
        Me.FF7.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF7.Location = New System.Drawing.Point(513, 305)
        Me.FF7.Name = "FF7"
        Me.FF7.Size = New System.Drawing.Size(95, 20)
        Me.FF7.TabIndex = 362
        Me.FF7.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF6
        '
        Me.FF6.CustomFormat = "yyyy-MM-dd"
        Me.FF6.Enabled = False
        Me.FF6.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF6.Location = New System.Drawing.Point(513, 259)
        Me.FF6.Name = "FF6"
        Me.FF6.Size = New System.Drawing.Size(95, 20)
        Me.FF6.TabIndex = 361
        Me.FF6.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF5
        '
        Me.FF5.CustomFormat = "yyyy-MM-dd"
        Me.FF5.Enabled = False
        Me.FF5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF5.Location = New System.Drawing.Point(513, 234)
        Me.FF5.Name = "FF5"
        Me.FF5.Size = New System.Drawing.Size(95, 20)
        Me.FF5.TabIndex = 360
        Me.FF5.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF4
        '
        Me.FF4.CustomFormat = "yyyy-MM-dd"
        Me.FF4.Enabled = False
        Me.FF4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF4.Location = New System.Drawing.Point(513, 208)
        Me.FF4.Name = "FF4"
        Me.FF4.Size = New System.Drawing.Size(95, 20)
        Me.FF4.TabIndex = 359
        Me.FF4.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF3
        '
        Me.FF3.CustomFormat = "yyyy-MM-dd"
        Me.FF3.Enabled = False
        Me.FF3.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF3.Location = New System.Drawing.Point(513, 182)
        Me.FF3.Name = "FF3"
        Me.FF3.Size = New System.Drawing.Size(95, 20)
        Me.FF3.TabIndex = 358
        Me.FF3.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FF2
        '
        Me.FF2.CustomFormat = "yyyy-MM-dd"
        Me.FF2.Enabled = False
        Me.FF2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF2.Location = New System.Drawing.Point(513, 155)
        Me.FF2.Name = "FF2"
        Me.FF2.Size = New System.Drawing.Size(95, 20)
        Me.FF2.TabIndex = 357
        Me.FF2.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label37.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label37.Location = New System.Drawing.Point(525, 80)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(59, 38)
        Me.Label37.TabIndex = 356
        Me.Label37.Text = "Fecha " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  Fin" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'FF1
        '
        Me.FF1.CustomFormat = "yyyy-MM-dd"
        Me.FF1.Enabled = False
        Me.FF1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FF1.Location = New System.Drawing.Point(513, 129)
        Me.FF1.Name = "FF1"
        Me.FF1.Size = New System.Drawing.Size(95, 20)
        Me.FF1.TabIndex = 355
        Me.FF1.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI16
        '
        Me.FI16.CustomFormat = "yyyy-MM-dd"
        Me.FI16.Enabled = False
        Me.FI16.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI16.Location = New System.Drawing.Point(977, 155)
        Me.FI16.Name = "FI16"
        Me.FI16.Size = New System.Drawing.Size(95, 20)
        Me.FI16.TabIndex = 354
        Me.FI16.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI15
        '
        Me.FI15.CustomFormat = "yyyy-MM-dd"
        Me.FI15.Enabled = False
        Me.FI15.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI15.Location = New System.Drawing.Point(977, 129)
        Me.FI15.Name = "FI15"
        Me.FI15.Size = New System.Drawing.Size(95, 20)
        Me.FI15.TabIndex = 353
        Me.FI15.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI14
        '
        Me.FI14.CustomFormat = "yyyy-MM-dd"
        Me.FI14.Enabled = False
        Me.FI14.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI14.Location = New System.Drawing.Point(414, 488)
        Me.FI14.Name = "FI14"
        Me.FI14.Size = New System.Drawing.Size(95, 20)
        Me.FI14.TabIndex = 352
        Me.FI14.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI13
        '
        Me.FI13.CustomFormat = "yyyy-MM-dd"
        Me.FI13.Enabled = False
        Me.FI13.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI13.Location = New System.Drawing.Point(414, 462)
        Me.FI13.Name = "FI13"
        Me.FI13.Size = New System.Drawing.Size(95, 20)
        Me.FI13.TabIndex = 351
        Me.FI13.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI12
        '
        Me.FI12.CustomFormat = "yyyy-MM-dd"
        Me.FI12.Enabled = False
        Me.FI12.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI12.Location = New System.Drawing.Point(414, 436)
        Me.FI12.Name = "FI12"
        Me.FI12.Size = New System.Drawing.Size(95, 20)
        Me.FI12.TabIndex = 350
        Me.FI12.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI11
        '
        Me.FI11.CustomFormat = "yyyy-MM-dd"
        Me.FI11.Enabled = False
        Me.FI11.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI11.Location = New System.Drawing.Point(414, 410)
        Me.FI11.Name = "FI11"
        Me.FI11.Size = New System.Drawing.Size(95, 20)
        Me.FI11.TabIndex = 349
        Me.FI11.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI10
        '
        Me.FI10.CustomFormat = "yyyy-MM-dd"
        Me.FI10.Enabled = False
        Me.FI10.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI10.Location = New System.Drawing.Point(414, 384)
        Me.FI10.Name = "FI10"
        Me.FI10.Size = New System.Drawing.Size(95, 20)
        Me.FI10.TabIndex = 348
        Me.FI10.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI9
        '
        Me.FI9.CustomFormat = "yyyy-MM-dd"
        Me.FI9.Enabled = False
        Me.FI9.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI9.Location = New System.Drawing.Point(414, 358)
        Me.FI9.Name = "FI9"
        Me.FI9.Size = New System.Drawing.Size(95, 20)
        Me.FI9.TabIndex = 347
        Me.FI9.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI8
        '
        Me.FI8.CustomFormat = "yyyy-MM-dd"
        Me.FI8.Enabled = False
        Me.FI8.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI8.Location = New System.Drawing.Point(414, 332)
        Me.FI8.Name = "FI8"
        Me.FI8.Size = New System.Drawing.Size(95, 20)
        Me.FI8.TabIndex = 346
        Me.FI8.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI7
        '
        Me.FI7.CustomFormat = "yyyy-MM-dd"
        Me.FI7.Enabled = False
        Me.FI7.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI7.Location = New System.Drawing.Point(414, 305)
        Me.FI7.Name = "FI7"
        Me.FI7.Size = New System.Drawing.Size(95, 20)
        Me.FI7.TabIndex = 345
        Me.FI7.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI6
        '
        Me.FI6.CustomFormat = "yyyy-MM-dd"
        Me.FI6.Enabled = False
        Me.FI6.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI6.Location = New System.Drawing.Point(414, 259)
        Me.FI6.Name = "FI6"
        Me.FI6.Size = New System.Drawing.Size(95, 20)
        Me.FI6.TabIndex = 344
        Me.FI6.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI5
        '
        Me.FI5.CustomFormat = "yyyy-MM-dd"
        Me.FI5.Enabled = False
        Me.FI5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI5.Location = New System.Drawing.Point(414, 234)
        Me.FI5.Name = "FI5"
        Me.FI5.Size = New System.Drawing.Size(95, 20)
        Me.FI5.TabIndex = 343
        Me.FI5.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI4
        '
        Me.FI4.CustomFormat = "yyyy-MM-dd"
        Me.FI4.Enabled = False
        Me.FI4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI4.Location = New System.Drawing.Point(414, 208)
        Me.FI4.Name = "FI4"
        Me.FI4.Size = New System.Drawing.Size(95, 20)
        Me.FI4.TabIndex = 342
        Me.FI4.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI3
        '
        Me.FI3.CustomFormat = "yyyy-MM-dd"
        Me.FI3.Enabled = False
        Me.FI3.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI3.Location = New System.Drawing.Point(414, 182)
        Me.FI3.Name = "FI3"
        Me.FI3.Size = New System.Drawing.Size(95, 20)
        Me.FI3.TabIndex = 341
        Me.FI3.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'FI2
        '
        Me.FI2.CustomFormat = "yyyy-MM-dd"
        Me.FI2.Enabled = False
        Me.FI2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI2.Location = New System.Drawing.Point(414, 155)
        Me.FI2.Name = "FI2"
        Me.FI2.Size = New System.Drawing.Size(95, 20)
        Me.FI2.TabIndex = 340
        Me.FI2.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label36.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Location = New System.Drawing.Point(426, 80)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(59, 38)
        Me.Label36.TabIndex = 339
        Me.Label36.Text = "Fecha " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Inicio"
        '
        'FI1
        '
        Me.FI1.CustomFormat = "yyyy-MM-dd"
        Me.FI1.Enabled = False
        Me.FI1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FI1.Location = New System.Drawing.Point(414, 129)
        Me.FI1.Name = "FI1"
        Me.FI1.Size = New System.Drawing.Size(95, 20)
        Me.FI1.TabIndex = 338
        Me.FI1.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'H16
        '
        Me.H16.Enabled = False
        Me.H16.Location = New System.Drawing.Point(897, 154)
        Me.H16.Name = "H16"
        Me.H16.Size = New System.Drawing.Size(69, 20)
        Me.H16.TabIndex = 337
        '
        'H15
        '
        Me.H15.Enabled = False
        Me.H15.Location = New System.Drawing.Point(897, 128)
        Me.H15.Name = "H15"
        Me.H15.Size = New System.Drawing.Size(69, 20)
        Me.H15.TabIndex = 336
        '
        'H14
        '
        Me.H14.Enabled = False
        Me.H14.Location = New System.Drawing.Point(339, 488)
        Me.H14.Name = "H14"
        Me.H14.Size = New System.Drawing.Size(69, 20)
        Me.H14.TabIndex = 335
        '
        'H13
        '
        Me.H13.Enabled = False
        Me.H13.Location = New System.Drawing.Point(339, 462)
        Me.H13.Name = "H13"
        Me.H13.Size = New System.Drawing.Size(69, 20)
        Me.H13.TabIndex = 334
        '
        'H12
        '
        Me.H12.Enabled = False
        Me.H12.Location = New System.Drawing.Point(339, 436)
        Me.H12.Name = "H12"
        Me.H12.Size = New System.Drawing.Size(69, 20)
        Me.H12.TabIndex = 333
        '
        'H11
        '
        Me.H11.Enabled = False
        Me.H11.Location = New System.Drawing.Point(339, 410)
        Me.H11.Name = "H11"
        Me.H11.Size = New System.Drawing.Size(69, 20)
        Me.H11.TabIndex = 332
        '
        'H10
        '
        Me.H10.Enabled = False
        Me.H10.Location = New System.Drawing.Point(339, 384)
        Me.H10.Name = "H10"
        Me.H10.Size = New System.Drawing.Size(69, 20)
        Me.H10.TabIndex = 331
        '
        'H9
        '
        Me.H9.Enabled = False
        Me.H9.Location = New System.Drawing.Point(339, 358)
        Me.H9.Name = "H9"
        Me.H9.Size = New System.Drawing.Size(69, 20)
        Me.H9.TabIndex = 330
        '
        'H8
        '
        Me.H8.Enabled = False
        Me.H8.Location = New System.Drawing.Point(339, 332)
        Me.H8.Name = "H8"
        Me.H8.Size = New System.Drawing.Size(69, 20)
        Me.H8.TabIndex = 329
        '
        'H7
        '
        Me.H7.Enabled = False
        Me.H7.Location = New System.Drawing.Point(339, 306)
        Me.H7.Name = "H7"
        Me.H7.Size = New System.Drawing.Size(69, 20)
        Me.H7.TabIndex = 328
        '
        'H6
        '
        Me.H6.Enabled = False
        Me.H6.Location = New System.Drawing.Point(339, 259)
        Me.H6.Name = "H6"
        Me.H6.Size = New System.Drawing.Size(69, 20)
        Me.H6.TabIndex = 327
        '
        'H5
        '
        Me.H5.Enabled = False
        Me.H5.Location = New System.Drawing.Point(339, 233)
        Me.H5.Name = "H5"
        Me.H5.Size = New System.Drawing.Size(69, 20)
        Me.H5.TabIndex = 326
        '
        'H4
        '
        Me.H4.Enabled = False
        Me.H4.Location = New System.Drawing.Point(339, 207)
        Me.H4.Name = "H4"
        Me.H4.Size = New System.Drawing.Size(69, 20)
        Me.H4.TabIndex = 325
        '
        'H3
        '
        Me.H3.Enabled = False
        Me.H3.Location = New System.Drawing.Point(339, 181)
        Me.H3.Name = "H3"
        Me.H3.Size = New System.Drawing.Size(69, 20)
        Me.H3.TabIndex = 324
        '
        'H2
        '
        Me.H2.Enabled = False
        Me.H2.Location = New System.Drawing.Point(339, 155)
        Me.H2.Name = "H2"
        Me.H2.Size = New System.Drawing.Size(69, 20)
        Me.H2.TabIndex = 323
        '
        'H1
        '
        Me.H1.Enabled = False
        Me.H1.Location = New System.Drawing.Point(339, 129)
        Me.H1.Name = "H1"
        Me.H1.Size = New System.Drawing.Size(69, 20)
        Me.H1.TabIndex = 322
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label35.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label35.Location = New System.Drawing.Point(335, 83)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(64, 19)
        Me.Label35.TabIndex = 321
        Me.Label35.Text = "  Horas"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label34.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label34.Location = New System.Drawing.Point(682, 84)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(224, 38)
        Me.Label34.TabIndex = 320
        Me.Label34.Text = "Seleccionar las actividades " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "que apliquen al proyecto:"
        '
        'BE28
        '
        Me.BE28.AutoSize = True
        Me.BE28.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE28.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE28.Location = New System.Drawing.Point(704, 470)
        Me.BE28.Name = "BE28"
        Me.BE28.Size = New System.Drawing.Size(110, 19)
        Me.BE28.TabIndex = 311
        Me.BE28.Text = "Entrega final."
        '
        'CheckBox28
        '
        Me.CheckBox28.AutoSize = True
        Me.CheckBox28.Location = New System.Drawing.Point(683, 475)
        Me.CheckBox28.Name = "CheckBox28"
        Me.CheckBox28.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox28.TabIndex = 310
        Me.CheckBox28.UseVisualStyleBackColor = True
        '
        'BE27
        '
        Me.BE27.AutoSize = True
        Me.BE27.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE27.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE27.Location = New System.Drawing.Point(705, 442)
        Me.BE27.Name = "BE27"
        Me.BE27.Size = New System.Drawing.Size(160, 19)
        Me.BE27.TabIndex = 309
        Me.BE27.Text = "Probar produciendo."
        '
        'CheckBox27
        '
        Me.CheckBox27.AutoSize = True
        Me.CheckBox27.Location = New System.Drawing.Point(684, 447)
        Me.CheckBox27.Name = "CheckBox27"
        Me.CheckBox27.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox27.TabIndex = 308
        Me.CheckBox27.UseVisualStyleBackColor = True
        '
        'BE26
        '
        Me.BE26.AutoSize = True
        Me.BE26.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE26.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE26.Location = New System.Drawing.Point(705, 418)
        Me.BE26.Name = "BE26"
        Me.BE26.Size = New System.Drawing.Size(139, 19)
        Me.BE26.TabIndex = 307
        Me.BE26.Text = "Entregar/Instalar."
        '
        'CheckBox26
        '
        Me.CheckBox26.AutoSize = True
        Me.CheckBox26.Location = New System.Drawing.Point(684, 423)
        Me.CheckBox26.Name = "CheckBox26"
        Me.CheckBox26.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox26.TabIndex = 306
        Me.CheckBox26.UseVisualStyleBackColor = True
        '
        'BE25
        '
        Me.BE25.AutoSize = True
        Me.BE25.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE25.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE25.Location = New System.Drawing.Point(704, 392)
        Me.BE25.Name = "BE25"
        Me.BE25.Size = New System.Drawing.Size(175, 19)
        Me.BE25.TabIndex = 305
        Me.BE25.Text = "Validar con el cliente."
        '
        'CheckBox25
        '
        Me.CheckBox25.AutoSize = True
        Me.CheckBox25.Location = New System.Drawing.Point(683, 397)
        Me.CheckBox25.Name = "CheckBox25"
        Me.CheckBox25.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox25.TabIndex = 304
        Me.CheckBox25.UseVisualStyleBackColor = True
        '
        'BE24
        '
        Me.BE24.AutoSize = True
        Me.BE24.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE24.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE24.Location = New System.Drawing.Point(704, 366)
        Me.BE24.Name = "BE24"
        Me.BE24.Size = New System.Drawing.Size(173, 19)
        Me.BE24.TabIndex = 303
        Me.BE24.Text = "Validar internamente."
        '
        'CheckBox24
        '
        Me.CheckBox24.AutoSize = True
        Me.CheckBox24.Location = New System.Drawing.Point(683, 371)
        Me.CheckBox24.Name = "CheckBox24"
        Me.CheckBox24.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox24.TabIndex = 302
        Me.CheckBox24.UseVisualStyleBackColor = True
        '
        'BE23
        '
        Me.BE23.AutoSize = True
        Me.BE23.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE23.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE23.Location = New System.Drawing.Point(704, 339)
        Me.BE23.Name = "BE23"
        Me.BE23.Size = New System.Drawing.Size(126, 19)
        Me.BE23.TabIndex = 301
        Me.BE23.Text = "Pruebas/Debug."
        '
        'CheckBox23
        '
        Me.CheckBox23.AutoSize = True
        Me.CheckBox23.Location = New System.Drawing.Point(683, 344)
        Me.CheckBox23.Name = "CheckBox23"
        Me.CheckBox23.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox23.TabIndex = 300
        Me.CheckBox23.UseVisualStyleBackColor = True
        '
        'BE22
        '
        Me.BE22.AutoSize = True
        Me.BE22.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE22.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE22.Location = New System.Drawing.Point(705, 316)
        Me.BE22.Name = "BE22"
        Me.BE22.Size = New System.Drawing.Size(141, 19)
        Me.BE22.TabIndex = 299
        Me.BE22.Text = "Programar Robot."
        '
        'CheckBox22
        '
        Me.CheckBox22.AutoSize = True
        Me.CheckBox22.Location = New System.Drawing.Point(684, 321)
        Me.CheckBox22.Name = "CheckBox22"
        Me.CheckBox22.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox22.TabIndex = 298
        Me.CheckBox22.UseVisualStyleBackColor = True
        '
        'BE21
        '
        Me.BE21.AutoSize = True
        Me.BE21.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE21.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE21.Location = New System.Drawing.Point(705, 289)
        Me.BE21.Name = "BE21"
        Me.BE21.Size = New System.Drawing.Size(183, 19)
        Me.BE21.TabIndex = 297
        Me.BE21.Text = "Programar servomotor."
        '
        'CheckBox21
        '
        Me.CheckBox21.AutoSize = True
        Me.CheckBox21.Location = New System.Drawing.Point(684, 294)
        Me.CheckBox21.Name = "CheckBox21"
        Me.CheckBox21.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox21.TabIndex = 296
        Me.CheckBox21.UseVisualStyleBackColor = True
        '
        'BE20
        '
        Me.BE20.AutoSize = True
        Me.BE20.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE20.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE20.Location = New System.Drawing.Point(704, 261)
        Me.BE20.Name = "BE20"
        Me.BE20.Size = New System.Drawing.Size(127, 19)
        Me.BE20.TabIndex = 295
        Me.BE20.Text = "Programar HMI."
        '
        'CheckBox20
        '
        Me.CheckBox20.AutoSize = True
        Me.CheckBox20.Location = New System.Drawing.Point(683, 266)
        Me.CheckBox20.Name = "CheckBox20"
        Me.CheckBox20.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox20.TabIndex = 294
        Me.CheckBox20.UseVisualStyleBackColor = True
        '
        'BE19
        '
        Me.BE19.AutoSize = True
        Me.BE19.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE19.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE19.Location = New System.Drawing.Point(705, 239)
        Me.BE19.Name = "BE19"
        Me.BE19.Size = New System.Drawing.Size(127, 19)
        Me.BE19.TabIndex = 293
        Me.BE19.Text = "Programar PLC."
        '
        'CheckBox19
        '
        Me.CheckBox19.AutoSize = True
        Me.CheckBox19.Location = New System.Drawing.Point(684, 244)
        Me.CheckBox19.Name = "CheckBox19"
        Me.CheckBox19.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox19.TabIndex = 292
        Me.CheckBox19.UseVisualStyleBackColor = True
        '
        'BE18
        '
        Me.BE18.AutoSize = True
        Me.BE18.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE18.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE18.Location = New System.Drawing.Point(705, 210)
        Me.BE18.Name = "BE18"
        Me.BE18.Size = New System.Drawing.Size(71, 19)
        Me.BE18.TabIndex = 291
        Me.BE18.Text = "Cablear."
        '
        'CheckBox18
        '
        Me.CheckBox18.AutoSize = True
        Me.CheckBox18.Location = New System.Drawing.Point(684, 215)
        Me.CheckBox18.Name = "CheckBox18"
        Me.CheckBox18.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox18.TabIndex = 290
        Me.CheckBox18.UseVisualStyleBackColor = True
        '
        'BE17
        '
        Me.BE17.AutoSize = True
        Me.BE17.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE17.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE17.Location = New System.Drawing.Point(705, 184)
        Me.BE17.Name = "BE17"
        Me.BE17.Size = New System.Drawing.Size(160, 19)
        Me.BE17.TabIndex = 289
        Me.BE17.Text = "Ensamble eléctrico."
        '
        'CheckBox17
        '
        Me.CheckBox17.AutoSize = True
        Me.CheckBox17.Location = New System.Drawing.Point(684, 189)
        Me.CheckBox17.Name = "CheckBox17"
        Me.CheckBox17.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox17.TabIndex = 288
        Me.CheckBox17.UseVisualStyleBackColor = True
        '
        'BE16
        '
        Me.BE16.AutoSize = True
        Me.BE16.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE16.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE16.Location = New System.Drawing.Point(705, 160)
        Me.BE16.Name = "BE16"
        Me.BE16.Size = New System.Drawing.Size(188, 19)
        Me.BE16.TabIndex = 287
        Me.BE16.Text = "Armar panel de control."
        '
        'CheckBox16
        '
        Me.CheckBox16.AutoSize = True
        Me.CheckBox16.Location = New System.Drawing.Point(684, 165)
        Me.CheckBox16.Name = "CheckBox16"
        Me.CheckBox16.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox16.TabIndex = 286
        Me.CheckBox16.UseVisualStyleBackColor = True
        '
        'BE15
        '
        Me.BE15.AutoSize = True
        Me.BE15.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE15.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE15.Location = New System.Drawing.Point(705, 134)
        Me.BE15.Name = "BE15"
        Me.BE15.Size = New System.Drawing.Size(165, 19)
        Me.BE15.TabIndex = 285
        Me.BE15.Text = "Ensamble Mecánico."
        '
        'CheckBox15
        '
        Me.CheckBox15.AutoSize = True
        Me.CheckBox15.Location = New System.Drawing.Point(684, 139)
        Me.CheckBox15.Name = "CheckBox15"
        Me.CheckBox15.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox15.TabIndex = 284
        Me.CheckBox15.UseVisualStyleBackColor = True
        '
        'BE14
        '
        Me.BE14.AutoSize = True
        Me.BE14.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE14.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE14.Location = New System.Drawing.Point(40, 492)
        Me.BE14.Name = "BE14"
        Me.BE14.Size = New System.Drawing.Size(164, 19)
        Me.BE14.TabIndex = 283
        Me.BE14.Text = "Preparar materiales."
        '
        'CheckBox14
        '
        Me.CheckBox14.AutoSize = True
        Me.CheckBox14.Location = New System.Drawing.Point(19, 497)
        Me.CheckBox14.Name = "CheckBox14"
        Me.CheckBox14.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox14.TabIndex = 282
        Me.CheckBox14.UseVisualStyleBackColor = True
        '
        'BE13
        '
        Me.BE13.AutoSize = True
        Me.BE13.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE13.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE13.Location = New System.Drawing.Point(40, 466)
        Me.BE13.Name = "BE13"
        Me.BE13.Size = New System.Drawing.Size(222, 19)
        Me.BE13.TabIndex = 281
        Me.BE13.Text = "Entrega 100% subcontratos."
        '
        'CheckBox13
        '
        Me.CheckBox13.AutoSize = True
        Me.CheckBox13.Location = New System.Drawing.Point(19, 471)
        Me.CheckBox13.Name = "CheckBox13"
        Me.CheckBox13.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox13.TabIndex = 280
        Me.CheckBox13.UseVisualStyleBackColor = True
        '
        'BE12
        '
        Me.BE12.AutoSize = True
        Me.BE12.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE12.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE12.Location = New System.Drawing.Point(40, 440)
        Me.BE12.Name = "BE12"
        Me.BE12.Size = New System.Drawing.Size(204, 19)
        Me.BE12.TabIndex = 279
        Me.BE12.Text = "Entrega 100% materiales."
        '
        'CheckBox12
        '
        Me.CheckBox12.AutoSize = True
        Me.CheckBox12.Location = New System.Drawing.Point(19, 445)
        Me.CheckBox12.Name = "CheckBox12"
        Me.CheckBox12.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox12.TabIndex = 278
        Me.CheckBox12.UseVisualStyleBackColor = True
        '
        'BE11
        '
        Me.BE11.AutoSize = True
        Me.BE11.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE11.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE11.Location = New System.Drawing.Point(40, 414)
        Me.BE11.Name = "BE11"
        Me.BE11.Size = New System.Drawing.Size(178, 19)
        Me.BE11.TabIndex = 277
        Me.BE11.Text = "Ordenar subcontratos."
        '
        'CheckBox11
        '
        Me.CheckBox11.AutoSize = True
        Me.CheckBox11.Location = New System.Drawing.Point(19, 419)
        Me.CheckBox11.Name = "CheckBox11"
        Me.CheckBox11.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox11.TabIndex = 276
        Me.CheckBox11.UseVisualStyleBackColor = True
        '
        'BE10
        '
        Me.BE10.AutoSize = True
        Me.BE10.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE10.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE10.Location = New System.Drawing.Point(40, 388)
        Me.BE10.Name = "BE10"
        Me.BE10.Size = New System.Drawing.Size(164, 19)
        Me.BE10.TabIndex = 275
        Me.BE10.Text = "Comprar materiales."
        '
        'CheckBox10
        '
        Me.CheckBox10.AutoSize = True
        Me.CheckBox10.Location = New System.Drawing.Point(19, 393)
        Me.CheckBox10.Name = "CheckBox10"
        Me.CheckBox10.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox10.TabIndex = 274
        Me.CheckBox10.UseVisualStyleBackColor = True
        '
        'BE9
        '
        Me.BE9.AutoSize = True
        Me.BE9.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE9.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE9.Location = New System.Drawing.Point(40, 362)
        Me.BE9.Name = "BE9"
        Me.BE9.Size = New System.Drawing.Size(123, 19)
        Me.BE9.TabIndex = 273
        Me.BE9.Text = "BOM Eléctrico."
        '
        'CheckBox9
        '
        Me.CheckBox9.AutoSize = True
        Me.CheckBox9.Location = New System.Drawing.Point(19, 367)
        Me.CheckBox9.Name = "CheckBox9"
        Me.CheckBox9.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox9.TabIndex = 272
        Me.CheckBox9.UseVisualStyleBackColor = True
        '
        'BE8
        '
        Me.BE8.AutoSize = True
        Me.BE8.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE8.Location = New System.Drawing.Point(40, 336)
        Me.BE8.Name = "BE8"
        Me.BE8.Size = New System.Drawing.Size(139, 19)
        Me.BE8.TabIndex = 271
        Me.BE8.Text = "Diseño Eléctrico."
        '
        'CheckBox8
        '
        Me.CheckBox8.AutoSize = True
        Me.CheckBox8.Location = New System.Drawing.Point(19, 336)
        Me.CheckBox8.Name = "CheckBox8"
        Me.CheckBox8.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox8.TabIndex = 270
        Me.CheckBox8.UseVisualStyleBackColor = True
        '
        'BE7
        '
        Me.BE7.AutoSize = True
        Me.BE7.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE7.Location = New System.Drawing.Point(40, 310)
        Me.BE7.Name = "BE7"
        Me.BE7.Size = New System.Drawing.Size(127, 19)
        Me.BE7.TabIndex = 269
        Me.BE7.Text = "BOM Mecánico."
        '
        'CheckBox7
        '
        Me.CheckBox7.AutoSize = True
        Me.CheckBox7.Location = New System.Drawing.Point(19, 310)
        Me.CheckBox7.Name = "CheckBox7"
        Me.CheckBox7.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox7.TabIndex = 268
        Me.CheckBox7.UseVisualStyleBackColor = True
        '
        'BE6
        '
        Me.BE6.AutoSize = True
        Me.BE6.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE6.Location = New System.Drawing.Point(40, 263)
        Me.BE6.Name = "BE6"
        Me.BE6.Size = New System.Drawing.Size(195, 38)
        Me.BE6.TabIndex = 267
        Me.BE6.Text = "Revisión del diseño con " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "el cliente/Visto bueno."
        '
        'CheckBox6
        '
        Me.CheckBox6.AutoSize = True
        Me.CheckBox6.Location = New System.Drawing.Point(19, 263)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox6.TabIndex = 266
        Me.CheckBox6.UseVisualStyleBackColor = True
        '
        'BE5
        '
        Me.BE5.AutoSize = True
        Me.BE5.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE5.Location = New System.Drawing.Point(40, 237)
        Me.BE5.Name = "BE5"
        Me.BE5.Size = New System.Drawing.Size(144, 19)
        Me.BE5.TabIndex = 265
        Me.BE5.Text = "Diseño mecánico."
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        Me.CheckBox5.Location = New System.Drawing.Point(19, 237)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox5.TabIndex = 264
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'BE4
        '
        Me.BE4.AutoSize = True
        Me.BE4.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE4.Location = New System.Drawing.Point(40, 211)
        Me.BE4.Name = "BE4"
        Me.BE4.Size = New System.Drawing.Size(297, 19)
        Me.BE4.TabIndex = 263
        Me.BE4.Text = "Comprar material (Lead times largos)."
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        Me.CheckBox4.Location = New System.Drawing.Point(19, 211)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox4.TabIndex = 262
        Me.CheckBox4.UseVisualStyleBackColor = True
        '
        'BE3
        '
        Me.BE3.AutoSize = True
        Me.BE3.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE3.Location = New System.Drawing.Point(40, 185)
        Me.BE3.Name = "BE3"
        Me.BE3.Size = New System.Drawing.Size(283, 19)
        Me.BE3.TabIndex = 261
        Me.BE3.Text = "BOM preliminar (Lead times largos)."
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Location = New System.Drawing.Point(19, 185)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox3.TabIndex = 260
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'BE2
        '
        Me.BE2.AutoSize = True
        Me.BE2.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE2.Location = New System.Drawing.Point(40, 159)
        Me.BE2.Name = "BE2"
        Me.BE2.Size = New System.Drawing.Size(152, 19)
        Me.BE2.TabIndex = 259
        Me.BE2.Text = "Estudio preliminar."
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(19, 159)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox2.TabIndex = 258
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'BE1
        '
        Me.BE1.AutoSize = True
        Me.BE1.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BE1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE1.Location = New System.Drawing.Point(40, 133)
        Me.BE1.Name = "BE1"
        Me.BE1.Size = New System.Drawing.Size(294, 19)
        Me.BE1.TabIndex = 257
        Me.BE1.Text = "Levantamiento de información - SOW."
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(19, 133)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox1.TabIndex = 256
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial Black", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Location = New System.Drawing.Point(14, 86)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(224, 38)
        Me.Label4.TabIndex = 255
        Me.Label4.Text = "Seleccionar las actividades " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "que apliquen al proyecto:"
        '
        'BE
        '
        Me.BE.AutoSize = True
        Me.BE.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BE.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE.Location = New System.Drawing.Point(442, 9)
        Me.BE.Name = "BE"
        Me.BE.Size = New System.Drawing.Size(258, 38)
        Me.BE.TabIndex = 254
        Me.BE.Text = "Bloque Estación"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label41.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label41.Location = New System.Drawing.Point(15, 57)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(182, 23)
        Me.Label41.TabIndex = 423
        Me.Label41.Text = "Nombre del Bloque:"
        '
        'NB
        '
        Me.NB.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.NB.Location = New System.Drawing.Point(204, 50)
        Me.NB.Name = "NB"
        Me.NB.Size = New System.Drawing.Size(250, 30)
        Me.NB.TabIndex = 424
        '
        'GC
        '
        Me.GC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GC.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GC.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GC.Image = CType(resources.GetObject("GC.Image"), System.Drawing.Image)
        Me.GC.Location = New System.Drawing.Point(1111, 492)
        Me.GC.Name = "GC"
        Me.GC.Size = New System.Drawing.Size(60, 60)
        Me.GC.TabIndex = 425
        Me.GC.UseVisualStyleBackColor = True
        '
        'JCBE
        '
        Me.JCBE.AutoSize = True
        Me.JCBE.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold)
        Me.JCBE.ForeColor = System.Drawing.Color.RoyalBlue
        Me.JCBE.Location = New System.Drawing.Point(698, 9)
        Me.JCBE.Name = "JCBE"
        Me.JCBE.Size = New System.Drawing.Size(0, 38)
        Me.JCBE.TabIndex = 432
        Me.JCBE.Visible = False
        '
        'BloqueEstacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1184, 611)
        Me.Controls.Add(Me.JCBE)
        Me.Controls.Add(Me.GC)
        Me.Controls.Add(Me.NB)
        Me.Controls.Add(Me.Label41)
        Me.Controls.Add(Me.FF28)
        Me.Controls.Add(Me.FF27)
        Me.Controls.Add(Me.FF26)
        Me.Controls.Add(Me.FF25)
        Me.Controls.Add(Me.FF24)
        Me.Controls.Add(Me.FF23)
        Me.Controls.Add(Me.FF22)
        Me.Controls.Add(Me.FF21)
        Me.Controls.Add(Me.FF20)
        Me.Controls.Add(Me.FF19)
        Me.Controls.Add(Me.FF18)
        Me.Controls.Add(Me.Label38)
        Me.Controls.Add(Me.FF17)
        Me.Controls.Add(Me.FI28)
        Me.Controls.Add(Me.FI27)
        Me.Controls.Add(Me.FI26)
        Me.Controls.Add(Me.FI25)
        Me.Controls.Add(Me.FI24)
        Me.Controls.Add(Me.FI23)
        Me.Controls.Add(Me.FI22)
        Me.Controls.Add(Me.FI21)
        Me.Controls.Add(Me.FI20)
        Me.Controls.Add(Me.FI19)
        Me.Controls.Add(Me.FI18)
        Me.Controls.Add(Me.Label39)
        Me.Controls.Add(Me.FI17)
        Me.Controls.Add(Me.H17)
        Me.Controls.Add(Me.H18)
        Me.Controls.Add(Me.H19)
        Me.Controls.Add(Me.H20)
        Me.Controls.Add(Me.H21)
        Me.Controls.Add(Me.H22)
        Me.Controls.Add(Me.H23)
        Me.Controls.Add(Me.H24)
        Me.Controls.Add(Me.H25)
        Me.Controls.Add(Me.H26)
        Me.Controls.Add(Me.H27)
        Me.Controls.Add(Me.H28)
        Me.Controls.Add(Me.Label40)
        Me.Controls.Add(Me.FF16)
        Me.Controls.Add(Me.FF15)
        Me.Controls.Add(Me.FF14)
        Me.Controls.Add(Me.FF13)
        Me.Controls.Add(Me.FF12)
        Me.Controls.Add(Me.FF11)
        Me.Controls.Add(Me.FF10)
        Me.Controls.Add(Me.FF9)
        Me.Controls.Add(Me.FF8)
        Me.Controls.Add(Me.FF7)
        Me.Controls.Add(Me.FF6)
        Me.Controls.Add(Me.FF5)
        Me.Controls.Add(Me.FF4)
        Me.Controls.Add(Me.FF3)
        Me.Controls.Add(Me.FF2)
        Me.Controls.Add(Me.Label37)
        Me.Controls.Add(Me.FF1)
        Me.Controls.Add(Me.FI16)
        Me.Controls.Add(Me.FI15)
        Me.Controls.Add(Me.FI14)
        Me.Controls.Add(Me.FI13)
        Me.Controls.Add(Me.FI12)
        Me.Controls.Add(Me.FI11)
        Me.Controls.Add(Me.FI10)
        Me.Controls.Add(Me.FI9)
        Me.Controls.Add(Me.FI8)
        Me.Controls.Add(Me.FI7)
        Me.Controls.Add(Me.FI6)
        Me.Controls.Add(Me.FI5)
        Me.Controls.Add(Me.FI4)
        Me.Controls.Add(Me.FI3)
        Me.Controls.Add(Me.FI2)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.FI1)
        Me.Controls.Add(Me.H16)
        Me.Controls.Add(Me.H15)
        Me.Controls.Add(Me.H14)
        Me.Controls.Add(Me.H13)
        Me.Controls.Add(Me.H12)
        Me.Controls.Add(Me.H11)
        Me.Controls.Add(Me.H10)
        Me.Controls.Add(Me.H9)
        Me.Controls.Add(Me.H8)
        Me.Controls.Add(Me.H7)
        Me.Controls.Add(Me.H6)
        Me.Controls.Add(Me.H5)
        Me.Controls.Add(Me.H4)
        Me.Controls.Add(Me.H3)
        Me.Controls.Add(Me.H2)
        Me.Controls.Add(Me.H1)
        Me.Controls.Add(Me.Label35)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.BE28)
        Me.Controls.Add(Me.CheckBox28)
        Me.Controls.Add(Me.BE27)
        Me.Controls.Add(Me.CheckBox27)
        Me.Controls.Add(Me.BE26)
        Me.Controls.Add(Me.CheckBox26)
        Me.Controls.Add(Me.BE25)
        Me.Controls.Add(Me.CheckBox25)
        Me.Controls.Add(Me.BE24)
        Me.Controls.Add(Me.CheckBox24)
        Me.Controls.Add(Me.BE23)
        Me.Controls.Add(Me.CheckBox23)
        Me.Controls.Add(Me.BE22)
        Me.Controls.Add(Me.CheckBox22)
        Me.Controls.Add(Me.BE21)
        Me.Controls.Add(Me.CheckBox21)
        Me.Controls.Add(Me.BE20)
        Me.Controls.Add(Me.CheckBox20)
        Me.Controls.Add(Me.BE19)
        Me.Controls.Add(Me.CheckBox19)
        Me.Controls.Add(Me.BE18)
        Me.Controls.Add(Me.CheckBox18)
        Me.Controls.Add(Me.BE17)
        Me.Controls.Add(Me.CheckBox17)
        Me.Controls.Add(Me.BE16)
        Me.Controls.Add(Me.CheckBox16)
        Me.Controls.Add(Me.BE15)
        Me.Controls.Add(Me.CheckBox15)
        Me.Controls.Add(Me.BE14)
        Me.Controls.Add(Me.CheckBox14)
        Me.Controls.Add(Me.BE13)
        Me.Controls.Add(Me.CheckBox13)
        Me.Controls.Add(Me.BE12)
        Me.Controls.Add(Me.CheckBox12)
        Me.Controls.Add(Me.BE11)
        Me.Controls.Add(Me.CheckBox11)
        Me.Controls.Add(Me.BE10)
        Me.Controls.Add(Me.CheckBox10)
        Me.Controls.Add(Me.BE9)
        Me.Controls.Add(Me.CheckBox9)
        Me.Controls.Add(Me.BE8)
        Me.Controls.Add(Me.CheckBox8)
        Me.Controls.Add(Me.BE7)
        Me.Controls.Add(Me.CheckBox7)
        Me.Controls.Add(Me.BE6)
        Me.Controls.Add(Me.CheckBox6)
        Me.Controls.Add(Me.BE5)
        Me.Controls.Add(Me.CheckBox5)
        Me.Controls.Add(Me.BE4)
        Me.Controls.Add(Me.CheckBox4)
        Me.Controls.Add(Me.BE3)
        Me.Controls.Add(Me.CheckBox3)
        Me.Controls.Add(Me.BE2)
        Me.Controls.Add(Me.CheckBox2)
        Me.Controls.Add(Me.BE1)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.BE)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "BloqueEstacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FF28 As DateTimePicker
    Friend WithEvents FF27 As DateTimePicker
    Friend WithEvents FF26 As DateTimePicker
    Friend WithEvents FF25 As DateTimePicker
    Friend WithEvents FF24 As DateTimePicker
    Friend WithEvents FF23 As DateTimePicker
    Friend WithEvents FF22 As DateTimePicker
    Friend WithEvents FF21 As DateTimePicker
    Friend WithEvents FF20 As DateTimePicker
    Friend WithEvents FF19 As DateTimePicker
    Friend WithEvents FF18 As DateTimePicker
    Friend WithEvents Label38 As Label
    Friend WithEvents FF17 As DateTimePicker
    Friend WithEvents FI28 As DateTimePicker
    Friend WithEvents FI27 As DateTimePicker
    Friend WithEvents FI26 As DateTimePicker
    Friend WithEvents FI25 As DateTimePicker
    Friend WithEvents FI24 As DateTimePicker
    Friend WithEvents FI23 As DateTimePicker
    Friend WithEvents FI22 As DateTimePicker
    Friend WithEvents FI21 As DateTimePicker
    Friend WithEvents FI20 As DateTimePicker
    Friend WithEvents FI19 As DateTimePicker
    Friend WithEvents FI18 As DateTimePicker
    Friend WithEvents Label39 As Label
    Friend WithEvents FI17 As DateTimePicker
    Friend WithEvents H17 As TextBox
    Friend WithEvents H18 As TextBox
    Friend WithEvents H19 As TextBox
    Friend WithEvents H20 As TextBox
    Friend WithEvents H21 As TextBox
    Friend WithEvents H22 As TextBox
    Friend WithEvents H23 As TextBox
    Friend WithEvents H24 As TextBox
    Friend WithEvents H25 As TextBox
    Friend WithEvents H26 As TextBox
    Friend WithEvents H27 As TextBox
    Friend WithEvents H28 As TextBox
    Friend WithEvents Label40 As Label
    Friend WithEvents FF16 As DateTimePicker
    Friend WithEvents FF15 As DateTimePicker
    Friend WithEvents FF14 As DateTimePicker
    Friend WithEvents FF13 As DateTimePicker
    Friend WithEvents FF12 As DateTimePicker
    Friend WithEvents FF11 As DateTimePicker
    Friend WithEvents FF10 As DateTimePicker
    Friend WithEvents FF9 As DateTimePicker
    Friend WithEvents FF8 As DateTimePicker
    Friend WithEvents FF7 As DateTimePicker
    Friend WithEvents FF6 As DateTimePicker
    Friend WithEvents FF5 As DateTimePicker
    Friend WithEvents FF4 As DateTimePicker
    Friend WithEvents FF3 As DateTimePicker
    Friend WithEvents FF2 As DateTimePicker
    Friend WithEvents Label37 As Label
    Friend WithEvents FF1 As DateTimePicker
    Friend WithEvents FI16 As DateTimePicker
    Friend WithEvents FI15 As DateTimePicker
    Friend WithEvents FI14 As DateTimePicker
    Friend WithEvents FI13 As DateTimePicker
    Friend WithEvents FI12 As DateTimePicker
    Friend WithEvents FI11 As DateTimePicker
    Friend WithEvents FI10 As DateTimePicker
    Friend WithEvents FI9 As DateTimePicker
    Friend WithEvents FI8 As DateTimePicker
    Friend WithEvents FI7 As DateTimePicker
    Friend WithEvents FI6 As DateTimePicker
    Friend WithEvents FI5 As DateTimePicker
    Friend WithEvents FI4 As DateTimePicker
    Friend WithEvents FI3 As DateTimePicker
    Friend WithEvents FI2 As DateTimePicker
    Friend WithEvents Label36 As Label
    Friend WithEvents FI1 As DateTimePicker
    Friend WithEvents H16 As TextBox
    Friend WithEvents H15 As TextBox
    Friend WithEvents H14 As TextBox
    Friend WithEvents H13 As TextBox
    Friend WithEvents H12 As TextBox
    Friend WithEvents H11 As TextBox
    Friend WithEvents H10 As TextBox
    Friend WithEvents H9 As TextBox
    Friend WithEvents H8 As TextBox
    Friend WithEvents H7 As TextBox
    Friend WithEvents H6 As TextBox
    Friend WithEvents H5 As TextBox
    Friend WithEvents H4 As TextBox
    Friend WithEvents H3 As TextBox
    Friend WithEvents H2 As TextBox
    Friend WithEvents H1 As TextBox
    Friend WithEvents Label35 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents BE28 As Label
    Friend WithEvents CheckBox28 As CheckBox
    Friend WithEvents BE27 As Label
    Friend WithEvents CheckBox27 As CheckBox
    Friend WithEvents BE26 As Label
    Friend WithEvents CheckBox26 As CheckBox
    Friend WithEvents BE25 As Label
    Friend WithEvents CheckBox25 As CheckBox
    Friend WithEvents BE24 As Label
    Friend WithEvents CheckBox24 As CheckBox
    Friend WithEvents BE23 As Label
    Friend WithEvents CheckBox23 As CheckBox
    Friend WithEvents BE22 As Label
    Friend WithEvents CheckBox22 As CheckBox
    Friend WithEvents BE21 As Label
    Friend WithEvents CheckBox21 As CheckBox
    Friend WithEvents BE20 As Label
    Friend WithEvents CheckBox20 As CheckBox
    Friend WithEvents BE19 As Label
    Friend WithEvents CheckBox19 As CheckBox
    Friend WithEvents BE18 As Label
    Friend WithEvents CheckBox18 As CheckBox
    Friend WithEvents BE17 As Label
    Friend WithEvents CheckBox17 As CheckBox
    Friend WithEvents BE16 As Label
    Friend WithEvents CheckBox16 As CheckBox
    Friend WithEvents BE15 As Label
    Friend WithEvents CheckBox15 As CheckBox
    Friend WithEvents BE14 As Label
    Friend WithEvents CheckBox14 As CheckBox
    Friend WithEvents BE13 As Label
    Friend WithEvents CheckBox13 As CheckBox
    Friend WithEvents BE12 As Label
    Friend WithEvents CheckBox12 As CheckBox
    Friend WithEvents BE11 As Label
    Friend WithEvents CheckBox11 As CheckBox
    Friend WithEvents BE10 As Label
    Friend WithEvents CheckBox10 As CheckBox
    Friend WithEvents BE9 As Label
    Friend WithEvents CheckBox9 As CheckBox
    Friend WithEvents BE8 As Label
    Friend WithEvents CheckBox8 As CheckBox
    Friend WithEvents BE7 As Label
    Friend WithEvents CheckBox7 As CheckBox
    Friend WithEvents BE6 As Label
    Friend WithEvents CheckBox6 As CheckBox
    Friend WithEvents BE5 As Label
    Friend WithEvents CheckBox5 As CheckBox
    Friend WithEvents BE4 As Label
    Friend WithEvents CheckBox4 As CheckBox
    Friend WithEvents BE3 As Label
    Friend WithEvents CheckBox3 As CheckBox
    Friend WithEvents BE2 As Label
    Friend WithEvents CheckBox2 As CheckBox
    Friend WithEvents BE1 As Label
    Friend WithEvents CheckBox1 As CheckBox
    Friend WithEvents Label4 As Label
    Friend WithEvents BE As Label
    Friend WithEvents Label41 As Label
    Friend WithEvents NB As TextBox
    Friend WithEvents GC As Button
    Public WithEvents JCBE As Label
End Class
