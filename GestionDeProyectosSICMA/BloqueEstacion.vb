﻿Imports System.Data.Odbc

Public Class BloqueEstacion
    Private Sub BloqueEstacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FI1.Value = DateTime.Now
        FI2.Value = DateTime.Now
        FI3.Value = DateTime.Now
        FI4.Value = DateTime.Now
        FI5.Value = DateTime.Now
        FI6.Value = DateTime.Now
        FI7.Value = DateTime.Now
        FI8.Value = DateTime.Now
        FI9.Value = DateTime.Now
        FI10.Value = DateTime.Now
        FI11.Value = DateTime.Now
        FI12.Value = DateTime.Now
        FI13.Value = DateTime.Now
        FI14.Value = DateTime.Now
        FI15.Value = DateTime.Now
        FI16.Value = DateTime.Now
        FI17.Value = DateTime.Now
        FI18.Value = DateTime.Now
        FI19.Value = DateTime.Now
        FI20.Value = DateTime.Now
        FI21.Value = DateTime.Now
        FI22.Value = DateTime.Now
        FI23.Value = DateTime.Now
        FI24.Value = DateTime.Now
        FI25.Value = DateTime.Now
        FI26.Value = DateTime.Now
        FI27.Value = DateTime.Now
        FI28.Value = DateTime.Now

        FF1.Value = DateTime.Now
        FF2.Value = DateTime.Now
        FF3.Value = DateTime.Now
        FF4.Value = DateTime.Now
        FF5.Value = DateTime.Now
        FF6.Value = DateTime.Now
        FF7.Value = DateTime.Now
        FF8.Value = DateTime.Now
        FF9.Value = DateTime.Now
        FF10.Value = DateTime.Now
        FF11.Value = DateTime.Now
        FF12.Value = DateTime.Now
        FF13.Value = DateTime.Now
        FF14.Value = DateTime.Now
        FF15.Value = DateTime.Now
        FF16.Value = DateTime.Now
        FF17.Value = DateTime.Now
        FF18.Value = DateTime.Now
        FF19.Value = DateTime.Now
        FF20.Value = DateTime.Now
        FF21.Value = DateTime.Now
        FF22.Value = DateTime.Now
        FF23.Value = DateTime.Now
        FF24.Value = DateTime.Now
        FF25.Value = DateTime.Now
        FF26.Value = DateTime.Now
        FF27.Value = DateTime.Now
        FF28.Value = DateTime.Now
    End Sub
    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            H1.Enabled = True
            FI1.Enabled = True
            FF1.Enabled = True
        Else
            H1.Enabled = False
            FI1.Enabled = False
            FF1.Enabled = False
        End If
    End Sub
    Private Sub CheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then
            H2.Enabled = True
            FI2.Enabled = True
            FF2.Enabled = True
        Else
            H2.Enabled = False
            FI2.Enabled = False
            FF2.Enabled = False
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then
            H3.Enabled = True
            FI3.Enabled = True
            FF3.Enabled = True
        Else
            H3.Enabled = False
            FI3.Enabled = False
            FF3.Enabled = False
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox4.CheckedChanged
        If CheckBox4.Checked = True Then
            H4.Enabled = True
            FI4.Enabled = True
            FF4.Enabled = True
        Else
            H4.Enabled = False
            FI4.Enabled = False
            FF4.Enabled = False
        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox5.CheckedChanged
        If CheckBox5.Checked = True Then
            H5.Enabled = True
            FI5.Enabled = True
            FF5.Enabled = True
        Else
            H5.Enabled = False
            FI5.Enabled = False
            FF5.Enabled = False
        End If
    End Sub

    Private Sub CheckBox6_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox6.CheckedChanged
        If CheckBox6.Checked = True Then
            H6.Enabled = True
            FI6.Enabled = True
            FF6.Enabled = True
        Else
            H6.Enabled = False
            FI6.Enabled = False
            FF6.Enabled = False
        End If
    End Sub

    Private Sub CheckBox7_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox7.CheckedChanged
        If CheckBox7.Checked = True Then
            H7.Enabled = True
            FI7.Enabled = True
            FF7.Enabled = True
        Else
            H7.Enabled = False
            FI7.Enabled = False
            FF7.Enabled = False
        End If
    End Sub

    Private Sub CheckBox8_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox8.CheckedChanged
        If CheckBox8.Checked = True Then
            H8.Enabled = True
            FI8.Enabled = True
            FF8.Enabled = True
        Else
            H8.Enabled = False
            FI8.Enabled = False
            FF8.Enabled = False
        End If
    End Sub

    Private Sub CheckBox9_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox9.CheckedChanged
        If CheckBox9.Checked = True Then
            H9.Enabled = True
            FI9.Enabled = True
            FF9.Enabled = True
        Else
            H9.Enabled = False
            FI9.Enabled = False
            FF9.Enabled = False
        End If
    End Sub

    Private Sub CheckBox10_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox10.CheckedChanged
        If CheckBox10.Checked = True Then
            H10.Enabled = True
            FI10.Enabled = True
            FF10.Enabled = True
        Else
            H10.Enabled = False
            FI10.Enabled = False
            FF10.Enabled = False
        End If
    End Sub

    Private Sub CheckBox11_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox11.CheckedChanged
        If CheckBox11.Checked = True Then
            H11.Enabled = True
            FI11.Enabled = True
            FF11.Enabled = True
        Else
            H11.Enabled = False
            FI11.Enabled = False
            FF11.Enabled = False
        End If
    End Sub

    Private Sub CheckBox12_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox12.CheckedChanged
        If CheckBox12.Checked = True Then
            H12.Enabled = True
            FI12.Enabled = True
            FF12.Enabled = True
        Else
            H12.Enabled = False
            FI12.Enabled = False
            FF12.Enabled = False
        End If
    End Sub

    Private Sub CheckBox13_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox13.CheckedChanged
        If CheckBox13.Checked = True Then
            H13.Enabled = True
            FI13.Enabled = True
            FF13.Enabled = True
        Else
            H13.Enabled = False
            FI13.Enabled = False
            FF13.Enabled = False
        End If
    End Sub

    Private Sub CheckBox14_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox14.CheckedChanged
        If CheckBox14.Checked = True Then
            H14.Enabled = True
            FI14.Enabled = True
            FF14.Enabled = True
        Else
            H14.Enabled = False
            FI14.Enabled = False
            FF14.Enabled = False
        End If
    End Sub

    Private Sub CheckBox15_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox15.CheckedChanged
        If CheckBox15.Checked = True Then
            H15.Enabled = True
            FI15.Enabled = True
            FF15.Enabled = True
        Else
            H15.Enabled = False
            FI15.Enabled = False
            FF15.Enabled = False
        End If
    End Sub

    Private Sub CheckBox16_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox16.CheckedChanged
        If CheckBox16.Checked = True Then
            H16.Enabled = True
            FI16.Enabled = True
            FF16.Enabled = True
        Else
            H16.Enabled = False
            FI16.Enabled = False
            FF16.Enabled = False
        End If
    End Sub

    Private Sub CheckBox17_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox17.CheckedChanged
        If CheckBox17.Checked = True Then
            H17.Enabled = True
            FI17.Enabled = True
            FF17.Enabled = True
        Else
            H17.Enabled = False
            FI17.Enabled = False
            FF17.Enabled = False
        End If
    End Sub

    Private Sub CheckBox18_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox18.CheckedChanged
        If CheckBox18.Checked = True Then
            H18.Enabled = True
            FI18.Enabled = True
            FF18.Enabled = True
        Else
            H18.Enabled = False
            FI18.Enabled = False
            FF18.Enabled = False
        End If
    End Sub

    Private Sub CheckBox19_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox19.CheckedChanged
        If CheckBox19.Checked = True Then
            H19.Enabled = True
            FI19.Enabled = True
            FF19.Enabled = True
        Else
            H19.Enabled = False
            FI19.Enabled = False
            FF19.Enabled = False
        End If
    End Sub

    Private Sub CheckBox20_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox20.CheckedChanged
        If CheckBox20.Checked = True Then
            H20.Enabled = True
            FI20.Enabled = True
            FF20.Enabled = True
        Else
            H20.Enabled = False
            FI20.Enabled = False
            FF20.Enabled = False
        End If
    End Sub

    Private Sub CheckBox21_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox21.CheckedChanged
        If CheckBox21.Checked = True Then
            H21.Enabled = True
            FI21.Enabled = True
            FF21.Enabled = True
        Else
            H21.Enabled = False
            FI21.Enabled = False
            FF21.Enabled = False
        End If
    End Sub

    Private Sub CheckBox22_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox22.CheckedChanged
        If CheckBox22.Checked = True Then
            H22.Enabled = True
            FI22.Enabled = True
            FF22.Enabled = True
        Else
            H22.Enabled = False
            FI22.Enabled = False
            FF22.Enabled = False
        End If
    End Sub

    Private Sub CheckBox23_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox23.CheckedChanged
        If CheckBox23.Checked = True Then
            H23.Enabled = True
            FI23.Enabled = True
            FF23.Enabled = True
        Else
            H23.Enabled = False
            FI23.Enabled = False
            FF23.Enabled = False
        End If
    End Sub

    Private Sub CheckBox24_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox24.CheckedChanged
        If CheckBox24.Checked = True Then
            H24.Enabled = True
            FI24.Enabled = True
            FF24.Enabled = True
        Else
            H24.Enabled = False
            FI24.Enabled = False
            FF24.Enabled = False
        End If
    End Sub

    Private Sub CheckBox25_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox25.CheckedChanged
        If CheckBox25.Checked = True Then
            H25.Enabled = True
            FI25.Enabled = True
            FF25.Enabled = True
        Else
            H25.Enabled = False
            FI25.Enabled = False
            FF25.Enabled = False
        End If
    End Sub

    Private Sub CheckBox26_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox26.CheckedChanged
        If CheckBox26.Checked = True Then
            H26.Enabled = True
            FI26.Enabled = True
            FF26.Enabled = True
        Else
            H26.Enabled = False
            FI26.Enabled = False
            FF26.Enabled = False
        End If
    End Sub

    Private Sub CheckBox27_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox27.CheckedChanged
        If CheckBox27.Checked = True Then
            H27.Enabled = True
            FI27.Enabled = True
            FF27.Enabled = True
        Else
            H27.Enabled = False
            FI27.Enabled = False
            FF27.Enabled = False
        End If
    End Sub

    Private Sub CheckBox28_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox28.CheckedChanged
        If CheckBox28.Checked = True Then
            H28.Enabled = True
            FI28.Enabled = True
            FF28.Enabled = True
        Else
            H28.Enabled = False
            FI28.Enabled = False
            FF28.Enabled = False
        End If
    End Sub

    Private Sub GC_Click(sender As Object, e As EventArgs) Handles GC.Click
        If CheckBox1.Checked = True Then
            If NB.Text = "" And H1.Text = "" And FI1.Text = "" And FF1.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H1.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI1.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF1.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE1.Text & "' ,'" & H1.Text & "','" & FI1.Text & "','" & FF1.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox2.Checked = True Then
            If NB.Text = "" And H2.Text = "" And FI2.Text = "" And FF2.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H2.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI2.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF2.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE2.Text & "' ,'" & H2.Text & "','" & FI2.Text & "','" & FF2.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox3.Checked = True Then
            If NB.Text = "" And H3.Text = "" And FI3.Text = "" And FF3.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H3.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI3.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF3.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE3.Text & "' ,'" & H3.Text & "','" & FI3.Text & "','" & FF3.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox4.Checked = True Then
            If NB.Text = "" And H4.Text = "" And FI4.Text = "" And FF4.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H4.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI4.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF4.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE4.Text & "' ,'" & H4.Text & "','" & FI4.Text & "','" & FF4.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox5.Checked = True Then
            If NB.Text = "" And H5.Text = "" And FI5.Text = "" And FF5.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H5.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI5.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF5.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE5.Text & "' ,'" & H5.Text & "','" & FI5.Text & "','" & FF5.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox6.Checked = True Then
            If NB.Text = "" And H6.Text = "" And FI6.Text = "" And FF6.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H6.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI6.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF6.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE6.Text & "' ,'" & H6.Text & "','" & FI6.Text & "','" & FF6.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox7.Checked = True Then
            If NB.Text = "" And H7.Text = "" And FI7.Text = "" And FF7.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H7.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI7.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF7.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE7.Text & "' ,'" & H7.Text & "','" & FI7.Text & "','" & FF7.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox8.Checked = True Then
            If NB.Text = "" And H8.Text = "" And FI8.Text = "" And FF8.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H8.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI8.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF8.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE8.Text & "' ,'" & H8.Text & "','" & FI8.Text & "','" & FF8.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox9.Checked = True Then
            If NB.Text = "" And H9.Text = "" And FI9.Text = "" And FF9.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H9.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI9.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF9.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE9.Text & "' ,'" & H9.Text & "','" & FI9.Text & "','" & FF9.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox10.Checked = True Then
            If NB.Text = "" And H10.Text = "" And FI10.Text = "" And FF10.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H10.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI10.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF10.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE10.Text & "' ,'" & H10.Text & "','" & FI10.Text & "','" & FF10.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox11.Checked = True Then
            If NB.Text = "" And H11.Text = "" And FI11.Text = "" And FF11.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H11.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI11.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF11.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE11.Text & "' ,'" & H11.Text & "','" & FI1.Text & "','" & FF11.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox12.Checked = True Then
            If NB.Text = "" And H12.Text = "" And FI12.Text = "" And FF12.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H12.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI12.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF12.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE12.Text & "' ,'" & H12.Text & "','" & FI12.Text & "','" & FF12.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox13.Checked = True Then
            If NB.Text = "" And H13.Text = "" And FI13.Text = "" And FF13.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H13.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI13.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF13.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE13.Text & "' ,'" & H13.Text & "','" & FI13.Text & "','" & FF13.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox14.Checked = True Then
            If NB.Text = "" And H14.Text = "" And FI14.Text = "" And FF14.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H14.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI14.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF14.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE14.Text & "' ,'" & H14.Text & "','" & FI14.Text & "','" & FF14.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox15.Checked = True Then
            If NB.Text = "" And H15.Text = "" And FI15.Text = "" And FF15.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H15.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI15.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF15.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE15.Text & "' ,'" & H15.Text & "','" & FI15.Text & "','" & FF15.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox16.Checked = True Then
            If NB.Text = "" And H16.Text = "" And FI16.Text = "" And FF16.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H16.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI16.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF16.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE16.Text & "' ,'" & H16.Text & "','" & FI16.Text & "','" & FF16.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox17.Checked = True Then
            If NB.Text = "" And H17.Text = "" And FI17.Text = "" And FF17.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H17.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI17.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF17.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE17.Text & "' ,'" & H17.Text & "','" & FI17.Text & "','" & FF17.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox18.Checked = True Then
            If NB.Text = "" And H18.Text = "" And FI18.Text = "" And FF18.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H18.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI18.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF18.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE18.Text & "' ,'" & H18.Text & "','" & FI18.Text & "','" & FF18.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox19.Checked = True Then
            If NB.Text = "" And H19.Text = "" And FI19.Text = "" And FF19.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H19.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI19.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF19.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE19.Text & "' ,'" & H19.Text & "','" & FI19.Text & "','" & FF19.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox20.Checked = True Then
            If NB.Text = "" And H20.Text = "" And FI20.Text = "" And FF20.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H20.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI20.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF20.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE20.Text & "' ,'" & H20.Text & "','" & FI20.Text & "','" & FF20.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox21.Checked = True Then
            If NB.Text = "" And H21.Text = "" And FI21.Text = "" And FF21.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H21.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI21.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF21.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE21.Text & "' ,'" & H21.Text & "','" & FI21.Text & "','" & FF21.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox22.Checked = True Then
            If NB.Text = "" And H22.Text = "" And FI22.Text = "" And FF22.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H22.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI22.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF22.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE22.Text & "' ,'" & H22.Text & "','" & FI22.Text & "','" & FF22.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If


        If CheckBox23.Checked = True Then
            If NB.Text = "" And H23.Text = "" And FI23.Text = "" And FF23.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H23.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI23.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF23.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE23.Text & "' ,'" & H23.Text & "','" & FI23.Text & "','" & FF23.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox24.Checked = True Then
            If NB.Text = "" And H24.Text = "" And FI24.Text = "" And FF24.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H24.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI24.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF24.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE24.Text & "' ,'" & H24.Text & "','" & FI24.Text & "','" & FF24.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox25.Checked = True Then
            If NB.Text = "" And H25.Text = "" And FI25.Text = "" And FF25.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H25.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI25.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF25.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE25.Text & "' ,'" & H25.Text & "','" & FI25.Text & "','" & FF25.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox26.Checked = True Then
            If NB.Text = "" And H26.Text = "" And FI26.Text = "" And FF26.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H26.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI26.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF26.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE26.Text & "' ,'" & H26.Text & "','" & FI26.Text & "','" & FF26.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox27.Checked = True Then
            If NB.Text = "" And H27.Text = "" And FI27.Text = "" And FF27.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H27.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI27.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF27.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE27.Text & "' ,'" & H27.Text & "','" & FI27.Text & "','" & FF27.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If CheckBox28.Checked = True Then
            If NB.Text = "" And H28.Text = "" And FI28.Text = "" And FF28.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If NB.Text = "" Then
                    MsgBox("El campo Nombre se encuentra vacio.")
                Else
                    If H28.Text = "" Then
                        MsgBox("El campo Horas se encuentra vacio.")
                    Else
                        If FI28.Text = "" Then
                            MsgBox("El campo Fecha Inicio se encuentra vacio.")
                        Else
                            If FF28.Text = "" Then
                                MsgBox("El campo Fecha Fin se encuentra vacio.")
                            Else
                                Try
                                    Dim guardarBE As New OdbcCommand("INSERT INTO gestióndeproyectossicma.bloqueestacion (job, nombre, actividad, horas, fechaI, fechaF) VALUES ('" & JCBE.Text & "','" & NB.Text & "', '" & BE28.Text & "' ,'" & H28.Text & "','" & FI28.Text & "','" & FF28.Text & "')", cn)
                                    guardarBE.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If
        MessageBox.Show("Los datos del Bloque se han guardado Satisfactoriamente.")
    End Sub

    Private Sub H1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H1.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H2.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H3.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H4_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H4.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H5_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H5.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H6_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H6.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H7_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H7.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H8_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H8.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H9_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H9.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H10_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H10.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H11_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H11.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H12_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H12.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H13_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H13.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H14_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H14.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H15_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H15.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H16_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H16.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H17_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H17.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H18_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H18.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H19_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H19.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H20_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H20.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H21_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H21.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H22_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H22.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H23_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H23.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H24_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H24.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H25_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H25.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H26_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H26.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H27_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H27.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub H28_KeyPress(sender As Object, e As KeyPressEventArgs) Handles H28.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub
End Class