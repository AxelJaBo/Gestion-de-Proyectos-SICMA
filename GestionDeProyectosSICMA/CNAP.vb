﻿Imports System.Data.Odbc

Public Class CNAP
    Private Sub CNAP_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Llenar()
    End Sub

    Public Sub Llenar()
        Dim dax = New OdbcDataAdapter("SELECT Cod_act FROM gestióndeproyectossicma.actividades WHERE (Nombre='" & MAP.Text.ToString & "' AND Status='En Proceso') OR (Nombre='" & MAP.Text.ToString & "' AND Status='Sin Comenzar') ORDER BY idactividades ASC", cn)
        Dim dtx = New DataTable
        dax.Fill(dtx)
        codigo.DataSource = dtx
        codigo.DisplayMember = "Cod_act"
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Llenar()
        Nota.Text = ""
        Comentario.Text = ""
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If codigo.Text = "" Then
            MsgBox("El campo Código se encuentra vacio.")
        Else
            Dim consultaractividad As New OdbcCommand("SELECT Comentario, Nota FROM gestióndeproyectossicma.actividades WHERE Cod_act='" & codigo.Text.ToArray & "'", cn)
            Dim datosactividad As OdbcDataReader
            Try
                datosactividad = consultaractividad.ExecuteReader
                If datosactividad.Read Then
                    Comentario.Text() = datosactividad.Item("Comentario").ToString()
                    Nota.Text() = datosactividad.Item("Nota").ToString()
                Else
                    MessageBox.Show("Esta Actividad no se encuentra Registrada.")
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If codigo.Text = "" And Comentario.Text = "" And Nota.Text = "" Then
            MsgBox("Los campos se encuentran vacios.")
        Else
            If codigo.Text = "" Then
                MsgBox("El campo Código se encuentra vacio.")
            Else
                If Comentario.Text = "" Then
                    MsgBox("El campo Comentario se encuentra vacio.")
                Else
                    If Nota.Text = "" Then
                        MsgBox("El campo Nota se encuentra vacio.")
                    Else
                        Dim consultaractividad3 As New OdbcCommand("SELECT Cod_act FROM gestióndeproyectossicma.actividades WHERE Cod_act ='" & codigo.Text.ToArray & "'", cn)
                        Dim datosactividad3 As OdbcDataReader
                        Try
                            datosactividad3 = consultaractividad3.ExecuteReader
                            If datosactividad3.Read Then
                                MessageBox.Show("Esta Actividad se ha Modificado Correctamente.")
                                Try
                                    Dim modificarVX As New OdbcCommand("UPDATE gestióndeproyectossicma.actividades SET Comentario='" & Comentario.Text.ToArray & "', Nota='" & Nota.Text.ToArray & "' WHERE Cod_act ='" & codigo.Text.ToArray & "'", cn)
                                    modificarVX.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            Else
                                MessageBox.Show("Esta Actividad no se encuentra Registrada.")
                            End If
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                        Llenar()
                    End If
                End If
            End If
        End If
    End Sub
End Class