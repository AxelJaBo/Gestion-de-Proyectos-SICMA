﻿Imports System.Data.Odbc

Module Conexion
    Public conectado As Boolean
    Public cn As OdbcConnection
    Public Sub conectar()
        Try
            cn = New OdbcConnection("dsn=GPS")
            cn.Open()
            If cn.State = ConnectionState.Open Then
                conectado = True
            Else
                conectado = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, "Error de conexion.")
        End Try
    End Sub
    Public Sub desconectar()
        Try
            If cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
                conectado = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, "Error de conexion.")
        End Try
    End Sub
End Module
