﻿Imports System.Data.Odbc
Public Class Form1
    'En esta parte del código te pregunta si desea salir del programa.'
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim Msg As MsgBoxResult
        Msg = MsgBox("¿Esta seguro que desea salir?", vbYesNo, "Salir del programa")
        If Msg = MsgBoxResult.Yes Then
            Application.ExitThread()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call conectar()
    End Sub

    'En esta parte del código es donde podra iniciar sesión dependiendo el tipo de usuario, los cuales son: Administrador, Ventas y Proyectos;'
    'por otra parte también se evalua si ya se ha iniciado sesión con algún usuario, esto para evitar que un usuario pueda iniciar sesión varias veces en el sistema.'
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Usuario.Text = "" And Contraseña.Text = "" Then
            MsgBox("Los campos se encuentran vacios.")
        Else
            If Usuario.Text = "" Then
                MsgBox("El campo Usuario se encuentra vacio.")
            Else
                If Contraseña.Text = "" Then
                    MsgBox("El campo Contraseña se encuentra vacio.")
                Else
                    Dim consultar As New OdbcCommand("SELECT nombre, usuario, contraseña, Tipo_Usuario, control_user FROM gestióndeproyectossicma.usuarios where usuario =  '" & Usuario.Text.ToArray & "'" & "AND contraseña= '" & Contraseña.Text & "'", cn)
                    Dim datos As OdbcDataReader
                    Try
                        datos = consultar.ExecuteReader
                        If datos.Read Then
                            If datos.Item("control_user").ToString() = "0" Then
                                Tipo.Text = datos.Item("nombre").ToString()
                                Try
                                    Dim controldeusuario As New OdbcCommand("UPDATE gestióndeproyectossicma.usuarios SET control_user='1' where nombre ='" & Tipo.Text.ToArray & "'", cn)
                                    controldeusuario.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                                If datos.Item("Tipo_Usuario").ToString() = "Ventas" Then
                                    MessageBox.Show("Se ha Iniciado Sesión Correctamente co el usuario " + datos.Item("nombre").ToString())
                                    Form2.Show()
                                    Form2.MensajeVentas.Text = Tipo.Text
                                    Form2.NombreVendedor.Text = Tipo.Text
                                    Form2.NombreS.Text = Tipo.Text
                                    Me.Hide()
                                End If
                                If datos.Item("Tipo_Usuario").ToString() = "Proyectos" Then
                                    MessageBox.Show("Se ha Iniciado Sesión Correctamente con el usuario " + datos.Item("nombre").ToString())
                                    Form4.Show()
                                    Form4.MensajeProyectos.Text = Tipo.Text
                                    Form4.NombreProyectista.Text = Tipo.Text
                                    Me.Hide()
                                End If
                                If datos.Item("Tipo_Usuario").ToString() = "Administrador" Then
                                    MessageBox.Show("Se ha Iniciado Sesión Correctamente con el usuario " + datos.Item("nombre").ToString())
                                    Form3.Show()
                                    Form3.MensajeAdmin.Text = Tipo.Text
                                    Me.Hide()
                                End If
                            End If
                            If datos.Item("control_user").ToString() = "1" Then
                                MessageBox.Show("La sesión esta activa con el usuario " + datos.Item("nombre").ToString())
                            End If
                        Else
                            MessageBox.Show("Usuario o Contraseña Incorrectos.", "Error")
                        End If
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                    Usuario.Text = ""
                    Contraseña.Text = ""
                End If
            End If
        End If
    End Sub
    'Esta parte del código permite evitar que el usuario cierre el pograma por el atajo ALT + F4, o por el Administrador de Tareas.'
    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        e.Cancel = True
    End Sub
End Class