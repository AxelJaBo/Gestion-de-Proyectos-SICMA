﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form2))
        Me.MensajeVentas = New System.Windows.Forms.Label()
        Me.CerrarS = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.Button29 = New System.Windows.Forms.Button()
        Me.JBG = New System.Windows.Forms.ComboBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.JBE = New System.Windows.Forms.ComboBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.NBBE = New System.Windows.Forms.ComboBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.PrioridadP = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.FechaJD = New System.Windows.Forms.DateTimePicker()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.FechaPOD = New System.Windows.Forms.DateTimePicker()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.JobRAJ = New System.Windows.Forms.ComboBox()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.NombreRR = New System.Windows.Forms.ComboBox()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.ClienteReporte = New System.Windows.Forms.ComboBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.btnVisitas = New System.Windows.Forms.Button()
        Me.Fecha2 = New System.Windows.Forms.DateTimePicker()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Fecha1 = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.CodigoAct = New System.Windows.Forms.ComboBox()
        Me.JobA = New System.Windows.Forms.ComboBox()
        Me.ContJob = New System.Windows.Forms.TextBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.ClienJob = New System.Windows.Forms.TextBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.DescJob = New System.Windows.Forms.TextBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.NotaA = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.ComentarioA = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.FechaM = New System.Windows.Forms.DateTimePicker()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Estado = New System.Windows.Forms.ComboBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Estatus = New System.Windows.Forms.ComboBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Titulo = New System.Windows.Forms.TextBox()
        Me.DescripcionA = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.NombreP = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.FechaA = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.CodigoVisita = New System.Windows.Forms.ComboBox()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.NombreVendedor = New System.Windows.Forms.TextBox()
        Me.DescVisita = New System.Windows.Forms.TextBox()
        Me.CodVisita = New System.Windows.Forms.Label()
        Me.LV = New System.Windows.Forms.Button()
        Me.EV = New System.Windows.Forms.Button()
        Me.MV = New System.Windows.Forms.Button()
        Me.GV = New System.Windows.Forms.Button()
        Me.BV = New System.Windows.Forms.Button()
        Me.DescripV = New System.Windows.Forms.Label()
        Me.VV = New System.Windows.Forms.Label()
        Me.ContactosVisita = New System.Windows.Forms.ComboBox()
        Me.ClienteVisita = New System.Windows.Forms.ComboBox()
        Me.ConV = New System.Windows.Forms.Label()
        Me.CV = New System.Windows.Forms.Label()
        Me.DateTimePicker5 = New System.Windows.Forms.DateTimePicker()
        Me.FPVisita = New System.Windows.Forms.Label()
        Me.DateTimePicker4 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Visitas = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Contacto = New System.Windows.Forms.ComboBox()
        Me.Empresa = New System.Windows.Forms.ComboBox()
        Me.TelCel = New System.Windows.Forms.TextBox()
        Me.TelOfi = New System.Windows.Forms.TextBox()
        Me.CorreoElectronico = New System.Windows.Forms.TextBox()
        Me.Departamento = New System.Windows.Forms.TextBox()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.LClient = New System.Windows.Forms.Button()
        Me.TCel = New System.Windows.Forms.Label()
        Me.TOfi = New System.Windows.Forms.Label()
        Me.CElec = New System.Windows.Forms.Label()
        Me.Depar = New System.Windows.Forms.Label()
        Me.EC = New System.Windows.Forms.Button()
        Me.MC = New System.Windows.Forms.Button()
        Me.GC = New System.Windows.Forms.Button()
        Me.BC = New System.Windows.Forms.Button()
        Me.NContacto = New System.Windows.Forms.Label()
        Me.NEmpresa = New System.Windows.Forms.Label()
        Me.Clientes = New System.Windows.Forms.Label()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Button47 = New System.Windows.Forms.Button()
        Me.Label134 = New System.Windows.Forms.Label()
        Me.DescEtapa = New System.Windows.Forms.TextBox()
        Me.Job = New System.Windows.Forms.ComboBox()
        Me.NombreS = New System.Windows.Forms.ComboBox()
        Me.Responsable = New System.Windows.Forms.TextBox()
        Me.Button31 = New System.Windows.Forms.Button()
        Me.Etapa = New System.Windows.Forms.ComboBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Button30 = New System.Windows.Forms.Button()
        Me.lider = New System.Windows.Forms.ComboBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Comentarios = New System.Windows.Forms.TextBox()
        Me.MontoCotizado = New System.Windows.Forms.TextBox()
        Me.NumeroCotizacion = New System.Windows.Forms.TextBox()
        Me.Descripcion = New System.Windows.Forms.TextBox()
        Me.FechaPO = New System.Windows.Forms.TextBox()
        Me.PO = New System.Windows.Forms.TextBox()
        Me.DiasTranscurridos = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Contactos = New System.Windows.Forms.ComboBox()
        Me.Cliente = New System.Windows.Forms.ComboBox()
        Me.LSG = New System.Windows.Forms.Button()
        Me.ESG = New System.Windows.Forms.Button()
        Me.MSG = New System.Windows.Forms.Button()
        Me.GSG = New System.Windows.Forms.Button()
        Me.BSG = New System.Windows.Forms.Button()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Moneda = New System.Windows.Forms.ComboBox()
        Me.Categoria = New System.Windows.Forms.ComboBox()
        Me.Prioridad = New System.Windows.Forms.ComboBox()
        Me.Comen = New System.Windows.Forms.Label()
        Me.Mon = New System.Windows.Forms.Label()
        Me.MontoCot = New System.Windows.Forms.Label()
        Me.NumeroCot = New System.Windows.Forms.Label()
        Me.FechaCompromisoE = New System.Windows.Forms.Label()
        Me.Desc = New System.Windows.Forms.Label()
        Me.Cate = New System.Windows.Forms.Label()
        Me.Cont = New System.Windows.Forms.Label()
        Me.Clien = New System.Windows.Forms.Label()
        Me.FRPO = New System.Windows.Forms.Label()
        Me.NPO = New System.Windows.Forms.Label()
        Me.P = New System.Windows.Forms.Label()
        Me.NJob = New System.Windows.Forms.Label()
        Me.DiasT = New System.Windows.Forms.Label()
        Me.FechaSolicitudC = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.DGCP = New System.Windows.Forms.DataGridView()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button27 = New System.Windows.Forms.Button()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.DTPSP = New System.Windows.Forms.DateTimePicker()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.CONSP = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.CLISP = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.LPDP = New System.Windows.Forms.TextBox()
        Me.DSP = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.JOBSP = New System.Windows.Forms.ComboBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TabPage10.SuspendLayout()
        Me.TabPage9.SuspendLayout()
        Me.TabPage8.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.DGCP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.SuspendLayout()
        '
        'MensajeVentas
        '
        Me.MensajeVentas.AutoSize = True
        Me.MensajeVentas.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensajeVentas.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MensajeVentas.Location = New System.Drawing.Point(437, 7)
        Me.MensajeVentas.Name = "MensajeVentas"
        Me.MensajeVentas.Size = New System.Drawing.Size(0, 38)
        Me.MensajeVentas.TabIndex = 10
        '
        'CerrarS
        '
        Me.CerrarS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CerrarS.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CerrarS.ForeColor = System.Drawing.Color.RoyalBlue
        Me.CerrarS.Image = CType(resources.GetObject("CerrarS.Image"), System.Drawing.Image)
        Me.CerrarS.Location = New System.Drawing.Point(912, -1)
        Me.CerrarS.Name = "CerrarS"
        Me.CerrarS.Size = New System.Drawing.Size(60, 60)
        Me.CerrarS.TabIndex = 8
        Me.CerrarS.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Location = New System.Drawing.Point(256, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(181, 38)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Bienvenid@"
        '
        'TabPage10
        '
        Me.TabPage10.BackColor = System.Drawing.Color.Black
        Me.TabPage10.Controls.Add(Me.Button29)
        Me.TabPage10.Controls.Add(Me.JBG)
        Me.TabPage10.Controls.Add(Me.Label55)
        Me.TabPage10.Controls.Add(Me.Label54)
        Me.TabPage10.Controls.Add(Me.Button28)
        Me.TabPage10.Controls.Add(Me.Label53)
        Me.TabPage10.Controls.Add(Me.JBE)
        Me.TabPage10.Controls.Add(Me.Label51)
        Me.TabPage10.Controls.Add(Me.NBBE)
        Me.TabPage10.Controls.Add(Me.Label52)
        Me.TabPage10.Controls.Add(Me.Label16)
        Me.TabPage10.Controls.Add(Me.PrioridadP)
        Me.TabPage10.Controls.Add(Me.Label41)
        Me.TabPage10.Controls.Add(Me.Button22)
        Me.TabPage10.Controls.Add(Me.Label42)
        Me.TabPage10.Controls.Add(Me.FechaJD)
        Me.TabPage10.Controls.Add(Me.Label40)
        Me.TabPage10.Controls.Add(Me.Button21)
        Me.TabPage10.Controls.Add(Me.Label39)
        Me.TabPage10.Controls.Add(Me.FechaPOD)
        Me.TabPage10.Controls.Add(Me.Button20)
        Me.TabPage10.Controls.Add(Me.Label37)
        Me.TabPage10.Controls.Add(Me.Label38)
        Me.TabPage10.Controls.Add(Me.JobRAJ)
        Me.TabPage10.Controls.Add(Me.Button19)
        Me.TabPage10.Controls.Add(Me.Label35)
        Me.TabPage10.Controls.Add(Me.Label36)
        Me.TabPage10.Controls.Add(Me.NombreRR)
        Me.TabPage10.Controls.Add(Me.Button18)
        Me.TabPage10.Controls.Add(Me.Label34)
        Me.TabPage10.Controls.Add(Me.Button7)
        Me.TabPage10.Controls.Add(Me.ClienteReporte)
        Me.TabPage10.Controls.Add(Me.Label19)
        Me.TabPage10.Controls.Add(Me.Button6)
        Me.TabPage10.Controls.Add(Me.Label22)
        Me.TabPage10.Controls.Add(Me.btnVisitas)
        Me.TabPage10.Controls.Add(Me.Fecha2)
        Me.TabPage10.Controls.Add(Me.Label15)
        Me.TabPage10.Controls.Add(Me.Label14)
        Me.TabPage10.Controls.Add(Me.Fecha1)
        Me.TabPage10.Controls.Add(Me.Label10)
        Me.TabPage10.Controls.Add(Me.Label7)
        Me.TabPage10.Location = New System.Drawing.Point(4, 32)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Size = New System.Drawing.Size(977, 478)
        Me.TabPage10.TabIndex = 4
        Me.TabPage10.Text = "Reportes"
        '
        'Button29
        '
        Me.Button29.BackColor = System.Drawing.Color.Black
        Me.Button29.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button29.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button29.Location = New System.Drawing.Point(363, 350)
        Me.Button29.Name = "Button29"
        Me.Button29.Size = New System.Drawing.Size(90, 30)
        Me.Button29.TabIndex = 445
        Me.Button29.Text = "Generar"
        Me.Button29.UseVisualStyleBackColor = False
        '
        'JBG
        '
        Me.JBG.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.JBG.FormattingEnabled = True
        Me.JBG.Location = New System.Drawing.Point(107, 349)
        Me.JBG.Name = "JBG"
        Me.JBG.Size = New System.Drawing.Size(250, 31)
        Me.JBG.TabIndex = 444
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label55.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label55.Location = New System.Drawing.Point(13, 357)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(48, 23)
        Me.Label55.TabIndex = 443
        Me.Label55.Text = "Job:"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Arial Black", 16.0!, System.Drawing.FontStyle.Bold)
        Me.Label54.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label54.Location = New System.Drawing.Point(10, 315)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(200, 31)
        Me.Label54.TabIndex = 442
        Me.Label54.Text = "Bloque General"
        '
        'Button28
        '
        Me.Button28.BackColor = System.Drawing.Color.Black
        Me.Button28.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button28.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button28.Location = New System.Drawing.Point(875, 327)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(90, 30)
        Me.Button28.TabIndex = 441
        Me.Button28.Text = "Generar"
        Me.Button28.UseVisualStyleBackColor = False
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Arial Black", 16.0!, System.Drawing.FontStyle.Bold)
        Me.Label53.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label53.Location = New System.Drawing.Point(524, 292)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(211, 31)
        Me.Label53.TabIndex = 440
        Me.Label53.Text = "Bloque Estación"
        '
        'JBE
        '
        Me.JBE.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.JBE.FormattingEnabled = True
        Me.JBE.Location = New System.Drawing.Point(619, 326)
        Me.JBE.Name = "JBE"
        Me.JBE.Size = New System.Drawing.Size(250, 31)
        Me.JBE.TabIndex = 439
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label51.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label51.Location = New System.Drawing.Point(526, 334)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(48, 23)
        Me.Label51.TabIndex = 438
        Me.Label51.Text = "Job:"
        '
        'NBBE
        '
        Me.NBBE.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.NBBE.FormattingEnabled = True
        Me.NBBE.Location = New System.Drawing.Point(619, 363)
        Me.NBBE.Name = "NBBE"
        Me.NBBE.Size = New System.Drawing.Size(250, 31)
        Me.NBBE.TabIndex = 437
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label52.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label52.Location = New System.Drawing.Point(526, 366)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(84, 69)
        Me.Label52.TabIndex = 436
        Me.Label52.Text = "Nombre " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "del " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Bloque:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label16.Location = New System.Drawing.Point(12, 289)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(84, 23)
        Me.Label16.TabIndex = 137
        Me.Label16.Text = "Nombre:"
        '
        'PrioridadP
        '
        Me.PrioridadP.Enabled = False
        Me.PrioridadP.Location = New System.Drawing.Point(108, 224)
        Me.PrioridadP.Name = "PrioridadP"
        Me.PrioridadP.Size = New System.Drawing.Size(248, 30)
        Me.PrioridadP.TabIndex = 136
        Me.PrioridadP.Text = "1-En proceso"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label41.Location = New System.Drawing.Point(12, 231)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(95, 23)
        Me.Label41.TabIndex = 135
        Me.Label41.Text = "Prioridad:"
        '
        'Button22
        '
        Me.Button22.BackColor = System.Drawing.Color.Black
        Me.Button22.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button22.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button22.Location = New System.Drawing.Point(363, 223)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(90, 30)
        Me.Button22.TabIndex = 134
        Me.Button22.Text = "Generar"
        Me.Button22.UseVisualStyleBackColor = False
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Arial Black", 16.0!, System.Drawing.FontStyle.Bold)
        Me.Label42.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label42.Location = New System.Drawing.Point(10, 190)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(397, 31)
        Me.Label42.TabIndex = 133
        Me.Label42.Text = "Ordenes de Compra en Proceso"
        '
        'FechaJD
        '
        Me.FechaJD.CustomFormat = "yyyy-MM-dd"
        Me.FechaJD.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FechaJD.Location = New System.Drawing.Point(620, 150)
        Me.FechaJD.Name = "FechaJD"
        Me.FechaJD.Size = New System.Drawing.Size(249, 30)
        Me.FechaJD.TabIndex = 132
        Me.FechaJD.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label40.Location = New System.Drawing.Point(526, 158)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(69, 23)
        Me.Label40.TabIndex = 131
        Me.Label40.Text = "Fecha:"
        '
        'Button21
        '
        Me.Button21.BackColor = System.Drawing.Color.Black
        Me.Button21.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button21.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button21.Location = New System.Drawing.Point(875, 152)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(92, 30)
        Me.Button21.TabIndex = 130
        Me.Button21.Text = "Generar"
        Me.Button21.UseVisualStyleBackColor = False
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Arial Black", 14.0!, System.Drawing.FontStyle.Bold)
        Me.Label39.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label39.Location = New System.Drawing.Point(525, 115)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(230, 27)
        Me.Label39.TabIndex = 129
        Me.Label39.Text = "Jobs Generados Hoy"
        '
        'FechaPOD
        '
        Me.FechaPOD.CustomFormat = "yyyy-MM-dd"
        Me.FechaPOD.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FechaPOD.Location = New System.Drawing.Point(620, 85)
        Me.FechaPOD.Name = "FechaPOD"
        Me.FechaPOD.Size = New System.Drawing.Size(249, 30)
        Me.FechaPOD.TabIndex = 127
        Me.FechaPOD.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Button20
        '
        Me.Button20.BackColor = System.Drawing.Color.Black
        Me.Button20.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button20.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button20.Location = New System.Drawing.Point(875, 87)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(92, 30)
        Me.Button20.TabIndex = 126
        Me.Button20.Text = "Generar"
        Me.Button20.UseVisualStyleBackColor = False
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Arial Black", 14.0!, System.Drawing.FontStyle.Bold)
        Me.Label37.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label37.Location = New System.Drawing.Point(525, 50)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(398, 27)
        Me.Label37.TabIndex = 125
        Me.Label37.Text = "Ordenes de Compras Generadas Hoy"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label38.Location = New System.Drawing.Point(526, 91)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(69, 23)
        Me.Label38.TabIndex = 124
        Me.Label38.Text = "Fecha:"
        '
        'JobRAJ
        '
        Me.JobRAJ.FormattingEnabled = True
        Me.JobRAJ.Location = New System.Drawing.Point(108, 153)
        Me.JobRAJ.Name = "JobRAJ"
        Me.JobRAJ.Size = New System.Drawing.Size(249, 31)
        Me.JobRAJ.TabIndex = 123
        '
        'Button19
        '
        Me.Button19.BackColor = System.Drawing.Color.Black
        Me.Button19.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button19.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button19.Location = New System.Drawing.Point(363, 157)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(92, 30)
        Me.Button19.TabIndex = 122
        Me.Button19.Text = "Generar"
        Me.Button19.UseVisualStyleBackColor = False
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Arial Black", 14.0!, System.Drawing.FontStyle.Bold)
        Me.Label35.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label35.Location = New System.Drawing.Point(11, 120)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(345, 27)
        Me.Label35.TabIndex = 121
        Me.Label35.Text = "Actividades Pendientes por Job"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Location = New System.Drawing.Point(12, 161)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(91, 23)
        Me.Label36.TabIndex = 120
        Me.Label36.Text = "# de Job:"
        '
        'NombreRR
        '
        Me.NombreRR.FormattingEnabled = True
        Me.NombreRR.Location = New System.Drawing.Point(108, 86)
        Me.NombreRR.Name = "NombreRR"
        Me.NombreRR.Size = New System.Drawing.Size(249, 31)
        Me.NombreRR.TabIndex = 119
        '
        'Button18
        '
        Me.Button18.BackColor = System.Drawing.Color.Black
        Me.Button18.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button18.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button18.Location = New System.Drawing.Point(363, 90)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(92, 30)
        Me.Button18.TabIndex = 118
        Me.Button18.Text = "Generar"
        Me.Button18.UseVisualStyleBackColor = False
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Arial Black", 14.0!, System.Drawing.FontStyle.Bold)
        Me.Label34.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label34.Location = New System.Drawing.Point(11, 53)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(442, 27)
        Me.Label34.TabIndex = 117
        Me.Label34.Text = "Actividades Pendientes por Responsable"
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.Black
        Me.Button7.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button7.Location = New System.Drawing.Point(857, 6)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(110, 30)
        Me.Button7.TabIndex = 116
        Me.Button7.Text = "Actualizar"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'ClienteReporte
        '
        Me.ClienteReporte.FormattingEnabled = True
        Me.ClienteReporte.Location = New System.Drawing.Point(107, 281)
        Me.ClienteReporte.Name = "ClienteReporte"
        Me.ClienteReporte.Size = New System.Drawing.Size(249, 31)
        Me.ClienteReporte.TabIndex = 115
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label19.Location = New System.Drawing.Point(12, 90)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(84, 23)
        Me.Label19.TabIndex = 114
        Me.Label19.Text = "Nombre:"
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.Black
        Me.Button6.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button6.Location = New System.Drawing.Point(361, 282)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(92, 30)
        Me.Button6.TabIndex = 113
        Me.Button6.Text = "Generar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Arial Black", 16.0!, System.Drawing.FontStyle.Bold)
        Me.Label22.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label22.Location = New System.Drawing.Point(10, 254)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(113, 31)
        Me.Label22.TabIndex = 112
        Me.Label22.Text = "Clientes"
        '
        'btnVisitas
        '
        Me.btnVisitas.BackColor = System.Drawing.Color.Black
        Me.btnVisitas.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnVisitas.ForeColor = System.Drawing.Color.RoyalBlue
        Me.btnVisitas.Location = New System.Drawing.Point(875, 223)
        Me.btnVisitas.Name = "btnVisitas"
        Me.btnVisitas.Size = New System.Drawing.Size(90, 30)
        Me.btnVisitas.TabIndex = 99
        Me.btnVisitas.Text = "Generar"
        Me.btnVisitas.UseVisualStyleBackColor = False
        '
        'Fecha2
        '
        Me.Fecha2.CustomFormat = "yyyy-MM-dd"
        Me.Fecha2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Fecha2.Location = New System.Drawing.Point(620, 259)
        Me.Fecha2.Name = "Fecha2"
        Me.Fecha2.Size = New System.Drawing.Size(249, 30)
        Me.Fecha2.TabIndex = 98
        Me.Fecha2.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label15.Location = New System.Drawing.Point(526, 266)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(62, 23)
        Me.Label15.TabIndex = 97
        Me.Label15.Text = "Hasta"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label14.Location = New System.Drawing.Point(526, 231)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(65, 23)
        Me.Label14.TabIndex = 96
        Me.Label14.Text = "Desde"
        '
        'Fecha1
        '
        Me.Fecha1.CustomFormat = "yyyy-MM-dd"
        Me.Fecha1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Fecha1.Location = New System.Drawing.Point(620, 223)
        Me.Fecha1.Name = "Fecha1"
        Me.Fecha1.Size = New System.Drawing.Size(249, 30)
        Me.Fecha1.TabIndex = 95
        Me.Fecha1.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Arial Black", 16.0!, System.Drawing.FontStyle.Bold)
        Me.Label10.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label10.Location = New System.Drawing.Point(524, 189)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(96, 31)
        Me.Label10.TabIndex = 94
        Me.Label10.Text = "Visitas"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial Black", 20.0!, System.Drawing.FontStyle.Bold)
        Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label7.Location = New System.Drawing.Point(456, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(151, 38)
        Me.Label7.TabIndex = 83
        Me.Label7.Text = "Reportes"
        '
        'TabPage9
        '
        Me.TabPage9.BackColor = System.Drawing.Color.Black
        Me.TabPage9.Controls.Add(Me.CodigoAct)
        Me.TabPage9.Controls.Add(Me.JobA)
        Me.TabPage9.Controls.Add(Me.ContJob)
        Me.TabPage9.Controls.Add(Me.Label58)
        Me.TabPage9.Controls.Add(Me.ClienJob)
        Me.TabPage9.Controls.Add(Me.Label57)
        Me.TabPage9.Controls.Add(Me.DescJob)
        Me.TabPage9.Controls.Add(Me.Label56)
        Me.TabPage9.Controls.Add(Me.NotaA)
        Me.TabPage9.Controls.Add(Me.Label49)
        Me.TabPage9.Controls.Add(Me.ComentarioA)
        Me.TabPage9.Controls.Add(Me.Label50)
        Me.TabPage9.Controls.Add(Me.Button23)
        Me.TabPage9.Controls.Add(Me.Button9)
        Me.TabPage9.Controls.Add(Me.FechaM)
        Me.TabPage9.Controls.Add(Me.Label26)
        Me.TabPage9.Controls.Add(Me.Estado)
        Me.TabPage9.Controls.Add(Me.Label25)
        Me.TabPage9.Controls.Add(Me.Estatus)
        Me.TabPage9.Controls.Add(Me.Label24)
        Me.TabPage9.Controls.Add(Me.Titulo)
        Me.TabPage9.Controls.Add(Me.DescripcionA)
        Me.TabPage9.Controls.Add(Me.Label23)
        Me.TabPage9.Controls.Add(Me.Label6)
        Me.TabPage9.Controls.Add(Me.Label13)
        Me.TabPage9.Controls.Add(Me.Label9)
        Me.TabPage9.Controls.Add(Me.NombreP)
        Me.TabPage9.Controls.Add(Me.Label8)
        Me.TabPage9.Controls.Add(Me.Button1)
        Me.TabPage9.Controls.Add(Me.Button2)
        Me.TabPage9.Controls.Add(Me.Button3)
        Me.TabPage9.Controls.Add(Me.Button4)
        Me.TabPage9.Controls.Add(Me.Button5)
        Me.TabPage9.Controls.Add(Me.FechaA)
        Me.TabPage9.Controls.Add(Me.Label5)
        Me.TabPage9.Controls.Add(Me.Label4)
        Me.TabPage9.Location = New System.Drawing.Point(4, 32)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Size = New System.Drawing.Size(977, 478)
        Me.TabPage9.TabIndex = 3
        Me.TabPage9.Text = "Actividades"
        '
        'CodigoAct
        '
        Me.CodigoAct.FormattingEnabled = True
        Me.CodigoAct.Location = New System.Drawing.Point(133, 37)
        Me.CodigoAct.Name = "CodigoAct"
        Me.CodigoAct.Size = New System.Drawing.Size(300, 31)
        Me.CodigoAct.TabIndex = 125
        '
        'JobA
        '
        Me.JobA.FormattingEnabled = True
        Me.JobA.Location = New System.Drawing.Point(133, 74)
        Me.JobA.Name = "JobA"
        Me.JobA.Size = New System.Drawing.Size(300, 31)
        Me.JobA.TabIndex = 124
        '
        'ContJob
        '
        Me.ContJob.Enabled = False
        Me.ContJob.Location = New System.Drawing.Point(133, 379)
        Me.ContJob.Multiline = True
        Me.ContJob.Name = "ContJob"
        Me.ContJob.Size = New System.Drawing.Size(300, 30)
        Me.ContJob.TabIndex = 115
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label58.Location = New System.Drawing.Point(13, 386)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(96, 23)
        Me.Label58.TabIndex = 116
        Me.Label58.Text = "Contacto:"
        '
        'ClienJob
        '
        Me.ClienJob.Enabled = False
        Me.ClienJob.Location = New System.Drawing.Point(133, 343)
        Me.ClienJob.Multiline = True
        Me.ClienJob.Name = "ClienJob"
        Me.ClienJob.Size = New System.Drawing.Size(300, 30)
        Me.ClienJob.TabIndex = 113
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label57.Location = New System.Drawing.Point(13, 350)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(77, 23)
        Me.Label57.TabIndex = 114
        Me.Label57.Text = "Cliente:"
        '
        'DescJob
        '
        Me.DescJob.Enabled = False
        Me.DescJob.Location = New System.Drawing.Point(133, 257)
        Me.DescJob.Multiline = True
        Me.DescJob.Name = "DescJob"
        Me.DescJob.Size = New System.Drawing.Size(300, 80)
        Me.DescJob.TabIndex = 111
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label56.Location = New System.Drawing.Point(13, 260)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(120, 46)
        Me.Label56.TabIndex = 112
        Me.Label56.Text = "Descripción " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "del Job:"
        '
        'NotaA
        '
        Me.NotaA.Location = New System.Drawing.Point(668, 247)
        Me.NotaA.Multiline = True
        Me.NotaA.Name = "NotaA"
        Me.NotaA.Size = New System.Drawing.Size(300, 80)
        Me.NotaA.TabIndex = 109
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label49.Location = New System.Drawing.Point(548, 250)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(57, 23)
        Me.Label49.TabIndex = 110
        Me.Label49.Text = "Nota:"
        '
        'ComentarioA
        '
        Me.ComentarioA.Location = New System.Drawing.Point(668, 161)
        Me.ComentarioA.Multiline = True
        Me.ComentarioA.Name = "ComentarioA"
        Me.ComentarioA.Size = New System.Drawing.Size(300, 80)
        Me.ComentarioA.TabIndex = 107
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label50.Location = New System.Drawing.Point(548, 164)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(117, 23)
        Me.Label50.TabIndex = 108
        Me.Label50.Text = "Comentario:"
        '
        'Button23
        '
        Me.Button23.BackColor = System.Drawing.Color.Black
        Me.Button23.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button23.Location = New System.Drawing.Point(439, 370)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(127, 60)
        Me.Button23.TabIndex = 104
        Me.Button23.Text = "Actividades Pendientes"
        Me.Button23.UseVisualStyleBackColor = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.Black
        Me.Button9.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button9.Location = New System.Drawing.Point(572, 370)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(60, 60)
        Me.Button9.TabIndex = 103
        Me.Button9.Text = "VER"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'FechaM
        '
        Me.FechaM.CustomFormat = "yyyy-MM-dd"
        Me.FechaM.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FechaM.Location = New System.Drawing.Point(133, 147)
        Me.FechaM.Name = "FechaM"
        Me.FechaM.Size = New System.Drawing.Size(300, 30)
        Me.FechaM.TabIndex = 102
        Me.FechaM.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label26.Location = New System.Drawing.Point(13, 155)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(118, 23)
        Me.Label26.TabIndex = 101
        Me.Label26.Text = "Fecha Meta:"
        '
        'Estado
        '
        Me.Estado.FormattingEnabled = True
        Me.Estado.Items.AddRange(New Object() {"Abierta", "Cerrada"})
        Me.Estado.Location = New System.Drawing.Point(133, 220)
        Me.Estado.Name = "Estado"
        Me.Estado.Size = New System.Drawing.Size(300, 31)
        Me.Estado.TabIndex = 100
        Me.Estado.Text = "Abierta"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label25.Location = New System.Drawing.Point(13, 191)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(83, 23)
        Me.Label25.TabIndex = 99
        Me.Label25.Text = "Estatus:"
        '
        'Estatus
        '
        Me.Estatus.Enabled = False
        Me.Estatus.FormattingEnabled = True
        Me.Estatus.Items.AddRange(New Object() {"Sin Comenzar", "En Proceso", "Terminada"})
        Me.Estatus.Location = New System.Drawing.Point(133, 183)
        Me.Estatus.Name = "Estatus"
        Me.Estatus.Size = New System.Drawing.Size(300, 31)
        Me.Estatus.TabIndex = 98
        Me.Estatus.Text = "Sin Comenzar"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label24.Location = New System.Drawing.Point(13, 228)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(77, 23)
        Me.Label24.TabIndex = 97
        Me.Label24.Text = "Estado:"
        '
        'Titulo
        '
        Me.Titulo.Location = New System.Drawing.Point(668, 39)
        Me.Titulo.Name = "Titulo"
        Me.Titulo.Size = New System.Drawing.Size(300, 30)
        Me.Titulo.TabIndex = 92
        '
        'DescripcionA
        '
        Me.DescripcionA.Location = New System.Drawing.Point(668, 75)
        Me.DescripcionA.Multiline = True
        Me.DescripcionA.Name = "DescripcionA"
        Me.DescripcionA.Size = New System.Drawing.Size(300, 80)
        Me.DescripcionA.TabIndex = 52
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label23.Location = New System.Drawing.Point(13, 46)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(76, 23)
        Me.Label23.TabIndex = 96
        Me.Label23.Text = "Código:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.Location = New System.Drawing.Point(548, 46)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(66, 23)
        Me.Label6.TabIndex = 93
        Me.Label6.Text = "Título:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label13.Location = New System.Drawing.Point(13, 82)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(91, 23)
        Me.Label13.TabIndex = 91
        Me.Label13.Text = "# de Job:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Arial Black", 20.0!, System.Drawing.FontStyle.Bold)
        Me.Label9.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label9.Location = New System.Drawing.Point(456, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(192, 38)
        Me.Label9.TabIndex = 82
        Me.Label9.Text = "Actividades"
        '
        'NombreP
        '
        Me.NombreP.FormattingEnabled = True
        Me.NombreP.Location = New System.Drawing.Point(668, 333)
        Me.NombreP.Name = "NombreP"
        Me.NombreP.Size = New System.Drawing.Size(300, 31)
        Me.NombreP.TabIndex = 81
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label8.Location = New System.Drawing.Point(547, 341)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(84, 23)
        Me.Label8.TabIndex = 80
        Me.Label8.Text = "Nombre:"
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.Location = New System.Drawing.Point(908, 370)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(60, 60)
        Me.Button1.TabIndex = 79
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button2.Image = CType(resources.GetObject("Button2.Image"), System.Drawing.Image)
        Me.Button2.Location = New System.Drawing.Point(842, 370)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(60, 60)
        Me.Button2.TabIndex = 78
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button3.Image = CType(resources.GetObject("Button3.Image"), System.Drawing.Image)
        Me.Button3.Location = New System.Drawing.Point(771, 370)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(60, 60)
        Me.Button3.TabIndex = 77
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button4.Image = CType(resources.GetObject("Button4.Image"), System.Drawing.Image)
        Me.Button4.Location = New System.Drawing.Point(705, 370)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(60, 60)
        Me.Button4.TabIndex = 76
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button5.Image = CType(resources.GetObject("Button5.Image"), System.Drawing.Image)
        Me.Button5.Location = New System.Drawing.Point(638, 370)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(60, 60)
        Me.Button5.TabIndex = 75
        Me.Button5.UseVisualStyleBackColor = True
        '
        'FechaA
        '
        Me.FechaA.CustomFormat = "yyyy-MM-dd"
        Me.FechaA.Enabled = False
        Me.FechaA.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FechaA.Location = New System.Drawing.Point(133, 111)
        Me.FechaA.Name = "FechaA"
        Me.FechaA.Size = New System.Drawing.Size(300, 30)
        Me.FechaA.TabIndex = 62
        Me.FechaA.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.Location = New System.Drawing.Point(13, 118)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(69, 23)
        Me.Label5.TabIndex = 61
        Me.Label5.Text = "Fecha:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Location = New System.Drawing.Point(548, 78)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 23)
        Me.Label4.TabIndex = 53
        Me.Label4.Text = "Descripción:"
        '
        'TabPage8
        '
        Me.TabPage8.BackColor = System.Drawing.Color.Black
        Me.TabPage8.Controls.Add(Me.CodigoVisita)
        Me.TabPage8.Controls.Add(Me.Button11)
        Me.TabPage8.Controls.Add(Me.NombreVendedor)
        Me.TabPage8.Controls.Add(Me.DescVisita)
        Me.TabPage8.Controls.Add(Me.CodVisita)
        Me.TabPage8.Controls.Add(Me.LV)
        Me.TabPage8.Controls.Add(Me.EV)
        Me.TabPage8.Controls.Add(Me.MV)
        Me.TabPage8.Controls.Add(Me.GV)
        Me.TabPage8.Controls.Add(Me.BV)
        Me.TabPage8.Controls.Add(Me.DescripV)
        Me.TabPage8.Controls.Add(Me.VV)
        Me.TabPage8.Controls.Add(Me.ContactosVisita)
        Me.TabPage8.Controls.Add(Me.ClienteVisita)
        Me.TabPage8.Controls.Add(Me.ConV)
        Me.TabPage8.Controls.Add(Me.CV)
        Me.TabPage8.Controls.Add(Me.DateTimePicker5)
        Me.TabPage8.Controls.Add(Me.FPVisita)
        Me.TabPage8.Controls.Add(Me.DateTimePicker4)
        Me.TabPage8.Controls.Add(Me.Label2)
        Me.TabPage8.Controls.Add(Me.Visitas)
        Me.TabPage8.Location = New System.Drawing.Point(4, 32)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Size = New System.Drawing.Size(977, 478)
        Me.TabPage8.TabIndex = 2
        Me.TabPage8.Text = "Visitas"
        '
        'CodigoVisita
        '
        Me.CodigoVisita.FormattingEnabled = True
        Me.CodigoVisita.Location = New System.Drawing.Point(463, 40)
        Me.CodigoVisita.Name = "CodigoVisita"
        Me.CodigoVisita.Size = New System.Drawing.Size(250, 31)
        Me.CodigoVisita.TabIndex = 106
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.Color.Black
        Me.Button11.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button11.Location = New System.Drawing.Point(323, 325)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(60, 60)
        Me.Button11.TabIndex = 105
        Me.Button11.Text = "VER"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'NombreVendedor
        '
        Me.NombreVendedor.Enabled = False
        Me.NombreVendedor.Location = New System.Drawing.Point(463, 150)
        Me.NombreVendedor.Name = "NombreVendedor"
        Me.NombreVendedor.Size = New System.Drawing.Size(250, 30)
        Me.NombreVendedor.TabIndex = 20
        '
        'DescVisita
        '
        Me.DescVisita.Location = New System.Drawing.Point(463, 259)
        Me.DescVisita.Multiline = True
        Me.DescVisita.Name = "DescVisita"
        Me.DescVisita.Size = New System.Drawing.Size(250, 60)
        Me.DescVisita.TabIndex = 5
        '
        'CodVisita
        '
        Me.CodVisita.AutoSize = True
        Me.CodVisita.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CodVisita.ForeColor = System.Drawing.Color.RoyalBlue
        Me.CodVisita.Location = New System.Drawing.Point(227, 48)
        Me.CodVisita.Name = "CodVisita"
        Me.CodVisita.Size = New System.Drawing.Size(76, 23)
        Me.CodVisita.TabIndex = 19
        Me.CodVisita.Text = "Código:"
        '
        'LV
        '
        Me.LV.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.LV.Image = CType(resources.GetObject("LV.Image"), System.Drawing.Image)
        Me.LV.Location = New System.Drawing.Point(653, 325)
        Me.LV.Name = "LV"
        Me.LV.Size = New System.Drawing.Size(60, 60)
        Me.LV.TabIndex = 18
        Me.LV.UseVisualStyleBackColor = True
        '
        'EV
        '
        Me.EV.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.EV.Image = CType(resources.GetObject("EV.Image"), System.Drawing.Image)
        Me.EV.Location = New System.Drawing.Point(587, 325)
        Me.EV.Name = "EV"
        Me.EV.Size = New System.Drawing.Size(60, 60)
        Me.EV.TabIndex = 16
        Me.EV.UseVisualStyleBackColor = True
        '
        'MV
        '
        Me.MV.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MV.Image = CType(resources.GetObject("MV.Image"), System.Drawing.Image)
        Me.MV.Location = New System.Drawing.Point(521, 325)
        Me.MV.Name = "MV"
        Me.MV.Size = New System.Drawing.Size(60, 60)
        Me.MV.TabIndex = 15
        Me.MV.UseVisualStyleBackColor = True
        '
        'GV
        '
        Me.GV.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GV.Image = CType(resources.GetObject("GV.Image"), System.Drawing.Image)
        Me.GV.Location = New System.Drawing.Point(455, 325)
        Me.GV.Name = "GV"
        Me.GV.Size = New System.Drawing.Size(60, 60)
        Me.GV.TabIndex = 14
        Me.GV.UseVisualStyleBackColor = True
        '
        'BV
        '
        Me.BV.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BV.Image = CType(resources.GetObject("BV.Image"), System.Drawing.Image)
        Me.BV.Location = New System.Drawing.Point(389, 325)
        Me.BV.Name = "BV"
        Me.BV.Size = New System.Drawing.Size(60, 60)
        Me.BV.TabIndex = 13
        Me.BV.UseVisualStyleBackColor = True
        '
        'DescripV
        '
        Me.DescripV.AutoSize = True
        Me.DescripV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.DescripV.Location = New System.Drawing.Point(227, 259)
        Me.DescripV.Name = "DescripV"
        Me.DescripV.Size = New System.Drawing.Size(120, 23)
        Me.DescripV.TabIndex = 11
        Me.DescripV.Text = "Descripción:"
        '
        'VV
        '
        Me.VV.AutoSize = True
        Me.VV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.VV.Location = New System.Drawing.Point(227, 157)
        Me.VV.Name = "VV"
        Me.VV.Size = New System.Drawing.Size(100, 23)
        Me.VV.TabIndex = 9
        Me.VV.Text = "Vendedor:"
        '
        'ContactosVisita
        '
        Me.ContactosVisita.FormattingEnabled = True
        Me.ContactosVisita.Location = New System.Drawing.Point(463, 223)
        Me.ContactosVisita.Name = "ContactosVisita"
        Me.ContactosVisita.Size = New System.Drawing.Size(250, 31)
        Me.ContactosVisita.TabIndex = 4
        '
        'ClienteVisita
        '
        Me.ClienteVisita.FormattingEnabled = True
        Me.ClienteVisita.Location = New System.Drawing.Point(463, 186)
        Me.ClienteVisita.Name = "ClienteVisita"
        Me.ClienteVisita.Size = New System.Drawing.Size(250, 31)
        Me.ClienteVisita.TabIndex = 3
        '
        'ConV
        '
        Me.ConV.AutoSize = True
        Me.ConV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.ConV.Location = New System.Drawing.Point(227, 231)
        Me.ConV.Name = "ConV"
        Me.ConV.Size = New System.Drawing.Size(96, 23)
        Me.ConV.TabIndex = 6
        Me.ConV.Text = "Contacto:"
        '
        'CV
        '
        Me.CV.AutoSize = True
        Me.CV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.CV.Location = New System.Drawing.Point(227, 194)
        Me.CV.Name = "CV"
        Me.CV.Size = New System.Drawing.Size(77, 23)
        Me.CV.TabIndex = 5
        Me.CV.Text = "Cliente:"
        '
        'DateTimePicker5
        '
        Me.DateTimePicker5.CustomFormat = "yyyy-MM-dd"
        Me.DateTimePicker5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker5.Location = New System.Drawing.Point(463, 114)
        Me.DateTimePicker5.Name = "DateTimePicker5"
        Me.DateTimePicker5.Size = New System.Drawing.Size(250, 30)
        Me.DateTimePicker5.TabIndex = 1
        '
        'FPVisita
        '
        Me.FPVisita.AutoSize = True
        Me.FPVisita.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FPVisita.ForeColor = System.Drawing.Color.RoyalBlue
        Me.FPVisita.Location = New System.Drawing.Point(227, 121)
        Me.FPVisita.Name = "FPVisita"
        Me.FPVisita.Size = New System.Drawing.Size(229, 23)
        Me.FPVisita.TabIndex = 3
        Me.FPVisita.Text = "Fecha de Próxima Visita:"
        '
        'DateTimePicker4
        '
        Me.DateTimePicker4.CustomFormat = "yyyy-MM-dd"
        Me.DateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker4.Location = New System.Drawing.Point(463, 77)
        Me.DateTimePicker4.Name = "DateTimePicker4"
        Me.DateTimePicker4.Size = New System.Drawing.Size(250, 30)
        Me.DateTimePicker4.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.Location = New System.Drawing.Point(227, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(151, 23)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Fecha de Visita:"
        '
        'Visitas
        '
        Me.Visitas.AutoSize = True
        Me.Visitas.Font = New System.Drawing.Font("Arial Black", 20.0!, System.Drawing.FontStyle.Bold)
        Me.Visitas.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Visitas.Location = New System.Drawing.Point(456, 0)
        Me.Visitas.Name = "Visitas"
        Me.Visitas.Size = New System.Drawing.Size(120, 38)
        Me.Visitas.TabIndex = 0
        Me.Visitas.Text = "Visitas"
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.Black
        Me.TabPage4.Controls.Add(Me.Contacto)
        Me.TabPage4.Controls.Add(Me.Empresa)
        Me.TabPage4.Controls.Add(Me.TelCel)
        Me.TabPage4.Controls.Add(Me.TelOfi)
        Me.TabPage4.Controls.Add(Me.CorreoElectronico)
        Me.TabPage4.Controls.Add(Me.Departamento)
        Me.TabPage4.Controls.Add(Me.Button10)
        Me.TabPage4.Controls.Add(Me.LClient)
        Me.TabPage4.Controls.Add(Me.TCel)
        Me.TabPage4.Controls.Add(Me.TOfi)
        Me.TabPage4.Controls.Add(Me.CElec)
        Me.TabPage4.Controls.Add(Me.Depar)
        Me.TabPage4.Controls.Add(Me.EC)
        Me.TabPage4.Controls.Add(Me.MC)
        Me.TabPage4.Controls.Add(Me.GC)
        Me.TabPage4.Controls.Add(Me.BC)
        Me.TabPage4.Controls.Add(Me.NContacto)
        Me.TabPage4.Controls.Add(Me.NEmpresa)
        Me.TabPage4.Controls.Add(Me.Clientes)
        Me.TabPage4.Location = New System.Drawing.Point(4, 32)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(977, 478)
        Me.TabPage4.TabIndex = 1
        Me.TabPage4.Text = "Clientes"
        '
        'Contacto
        '
        Me.Contacto.FormattingEnabled = True
        Me.Contacto.Location = New System.Drawing.Point(463, 80)
        Me.Contacto.Name = "Contacto"
        Me.Contacto.Size = New System.Drawing.Size(250, 31)
        Me.Contacto.TabIndex = 114
        '
        'Empresa
        '
        Me.Empresa.FormattingEnabled = True
        Me.Empresa.Location = New System.Drawing.Point(463, 44)
        Me.Empresa.Name = "Empresa"
        Me.Empresa.Size = New System.Drawing.Size(250, 31)
        Me.Empresa.TabIndex = 113
        '
        'TelCel
        '
        Me.TelCel.Location = New System.Drawing.Point(463, 153)
        Me.TelCel.Name = "TelCel"
        Me.TelCel.Size = New System.Drawing.Size(250, 30)
        Me.TelCel.TabIndex = 3
        '
        'TelOfi
        '
        Me.TelOfi.Location = New System.Drawing.Point(463, 117)
        Me.TelOfi.Name = "TelOfi"
        Me.TelOfi.Size = New System.Drawing.Size(250, 30)
        Me.TelOfi.TabIndex = 2
        '
        'CorreoElectronico
        '
        Me.CorreoElectronico.Location = New System.Drawing.Point(463, 225)
        Me.CorreoElectronico.Name = "CorreoElectronico"
        Me.CorreoElectronico.Size = New System.Drawing.Size(250, 30)
        Me.CorreoElectronico.TabIndex = 5
        '
        'Departamento
        '
        Me.Departamento.Location = New System.Drawing.Point(463, 189)
        Me.Departamento.Name = "Departamento"
        Me.Departamento.Size = New System.Drawing.Size(250, 30)
        Me.Departamento.TabIndex = 4
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.Black
        Me.Button10.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button10.Location = New System.Drawing.Point(311, 261)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(60, 60)
        Me.Button10.TabIndex = 104
        Me.Button10.Text = "VER"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'LClient
        '
        Me.LClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LClient.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LClient.ForeColor = System.Drawing.Color.RoyalBlue
        Me.LClient.Image = CType(resources.GetObject("LClient.Image"), System.Drawing.Image)
        Me.LClient.Location = New System.Drawing.Point(641, 261)
        Me.LClient.Name = "LClient"
        Me.LClient.Size = New System.Drawing.Size(60, 60)
        Me.LClient.TabIndex = 42
        Me.LClient.UseVisualStyleBackColor = True
        '
        'TCel
        '
        Me.TCel.AutoSize = True
        Me.TCel.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TCel.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TCel.Location = New System.Drawing.Point(242, 160)
        Me.TCel.Name = "TCel"
        Me.TCel.Size = New System.Drawing.Size(160, 23)
        Me.TCel.TabIndex = 36
        Me.TCel.Text = "Teléfono Celular:"
        '
        'TOfi
        '
        Me.TOfi.AutoSize = True
        Me.TOfi.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TOfi.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TOfi.Location = New System.Drawing.Point(241, 124)
        Me.TOfi.Name = "TOfi"
        Me.TOfi.Size = New System.Drawing.Size(186, 23)
        Me.TOfi.TabIndex = 35
        Me.TOfi.Text = "Teléfono de Oficina:"
        '
        'CElec
        '
        Me.CElec.AutoSize = True
        Me.CElec.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CElec.ForeColor = System.Drawing.Color.RoyalBlue
        Me.CElec.Location = New System.Drawing.Point(242, 232)
        Me.CElec.Name = "CElec"
        Me.CElec.Size = New System.Drawing.Size(182, 23)
        Me.CElec.TabIndex = 34
        Me.CElec.Text = "Correo Electrónico:"
        '
        'Depar
        '
        Me.Depar.AutoSize = True
        Me.Depar.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Depar.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Depar.Location = New System.Drawing.Point(242, 196)
        Me.Depar.Name = "Depar"
        Me.Depar.Size = New System.Drawing.Size(142, 23)
        Me.Depar.TabIndex = 33
        Me.Depar.Text = "Departamento:"
        '
        'EC
        '
        Me.EC.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.EC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EC.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EC.ForeColor = System.Drawing.Color.RoyalBlue
        Me.EC.Image = CType(resources.GetObject("EC.Image"), System.Drawing.Image)
        Me.EC.Location = New System.Drawing.Point(575, 261)
        Me.EC.Name = "EC"
        Me.EC.Size = New System.Drawing.Size(60, 60)
        Me.EC.TabIndex = 31
        Me.EC.UseVisualStyleBackColor = True
        '
        'MC
        '
        Me.MC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MC.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MC.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MC.Image = CType(resources.GetObject("MC.Image"), System.Drawing.Image)
        Me.MC.Location = New System.Drawing.Point(509, 261)
        Me.MC.Name = "MC"
        Me.MC.Size = New System.Drawing.Size(60, 60)
        Me.MC.TabIndex = 30
        Me.MC.UseVisualStyleBackColor = True
        '
        'GC
        '
        Me.GC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GC.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GC.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GC.Image = CType(resources.GetObject("GC.Image"), System.Drawing.Image)
        Me.GC.Location = New System.Drawing.Point(443, 261)
        Me.GC.Name = "GC"
        Me.GC.Size = New System.Drawing.Size(60, 60)
        Me.GC.TabIndex = 29
        Me.GC.UseVisualStyleBackColor = True
        '
        'BC
        '
        Me.BC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BC.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BC.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BC.Image = CType(resources.GetObject("BC.Image"), System.Drawing.Image)
        Me.BC.Location = New System.Drawing.Point(377, 261)
        Me.BC.Name = "BC"
        Me.BC.Size = New System.Drawing.Size(60, 60)
        Me.BC.TabIndex = 28
        Me.BC.UseVisualStyleBackColor = True
        '
        'NContacto
        '
        Me.NContacto.AutoSize = True
        Me.NContacto.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NContacto.ForeColor = System.Drawing.Color.RoyalBlue
        Me.NContacto.Location = New System.Drawing.Point(241, 88)
        Me.NContacto.Name = "NContacto"
        Me.NContacto.Size = New System.Drawing.Size(202, 23)
        Me.NContacto.TabIndex = 25
        Me.NContacto.Text = "Nombre del Contacto:"
        '
        'NEmpresa
        '
        Me.NEmpresa.AutoSize = True
        Me.NEmpresa.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NEmpresa.ForeColor = System.Drawing.Color.RoyalBlue
        Me.NEmpresa.Location = New System.Drawing.Point(242, 52)
        Me.NEmpresa.Name = "NEmpresa"
        Me.NEmpresa.Size = New System.Drawing.Size(215, 23)
        Me.NEmpresa.TabIndex = 24
        Me.NEmpresa.Text = "Nombre de la Empresa:"
        '
        'Clientes
        '
        Me.Clientes.AutoSize = True
        Me.Clientes.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clientes.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Clientes.Location = New System.Drawing.Point(456, 3)
        Me.Clientes.Name = "Clientes"
        Me.Clientes.Size = New System.Drawing.Size(139, 38)
        Me.Clientes.TabIndex = 23
        Me.Clientes.Text = "Clientes"
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.Black
        Me.TabPage1.Controls.Add(Me.Button47)
        Me.TabPage1.Controls.Add(Me.Label134)
        Me.TabPage1.Controls.Add(Me.DescEtapa)
        Me.TabPage1.Controls.Add(Me.Job)
        Me.TabPage1.Controls.Add(Me.NombreS)
        Me.TabPage1.Controls.Add(Me.Responsable)
        Me.TabPage1.Controls.Add(Me.Button31)
        Me.TabPage1.Controls.Add(Me.Etapa)
        Me.TabPage1.Controls.Add(Me.Label60)
        Me.TabPage1.Controls.Add(Me.Label59)
        Me.TabPage1.Controls.Add(Me.Button30)
        Me.TabPage1.Controls.Add(Me.lider)
        Me.TabPage1.Controls.Add(Me.Label46)
        Me.TabPage1.Controls.Add(Me.Comentarios)
        Me.TabPage1.Controls.Add(Me.MontoCotizado)
        Me.TabPage1.Controls.Add(Me.NumeroCotizacion)
        Me.TabPage1.Controls.Add(Me.Descripcion)
        Me.TabPage1.Controls.Add(Me.FechaPO)
        Me.TabPage1.Controls.Add(Me.PO)
        Me.TabPage1.Controls.Add(Me.DiasTranscurridos)
        Me.TabPage1.Controls.Add(Me.Label43)
        Me.TabPage1.Controls.Add(Me.Button8)
        Me.TabPage1.Controls.Add(Me.Contactos)
        Me.TabPage1.Controls.Add(Me.Cliente)
        Me.TabPage1.Controls.Add(Me.LSG)
        Me.TabPage1.Controls.Add(Me.ESG)
        Me.TabPage1.Controls.Add(Me.MSG)
        Me.TabPage1.Controls.Add(Me.GSG)
        Me.TabPage1.Controls.Add(Me.BSG)
        Me.TabPage1.Controls.Add(Me.DateTimePicker2)
        Me.TabPage1.Controls.Add(Me.DateTimePicker1)
        Me.TabPage1.Controls.Add(Me.Moneda)
        Me.TabPage1.Controls.Add(Me.Categoria)
        Me.TabPage1.Controls.Add(Me.Prioridad)
        Me.TabPage1.Controls.Add(Me.Comen)
        Me.TabPage1.Controls.Add(Me.Mon)
        Me.TabPage1.Controls.Add(Me.MontoCot)
        Me.TabPage1.Controls.Add(Me.NumeroCot)
        Me.TabPage1.Controls.Add(Me.FechaCompromisoE)
        Me.TabPage1.Controls.Add(Me.Desc)
        Me.TabPage1.Controls.Add(Me.Cate)
        Me.TabPage1.Controls.Add(Me.Cont)
        Me.TabPage1.Controls.Add(Me.Clien)
        Me.TabPage1.Controls.Add(Me.FRPO)
        Me.TabPage1.Controls.Add(Me.NPO)
        Me.TabPage1.Controls.Add(Me.P)
        Me.TabPage1.Controls.Add(Me.NJob)
        Me.TabPage1.Controls.Add(Me.DiasT)
        Me.TabPage1.Controls.Add(Me.FechaSolicitudC)
        Me.TabPage1.Location = New System.Drawing.Point(4, 32)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(977, 478)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Seguimiento"
        '
        'Button47
        '
        Me.Button47.BackColor = System.Drawing.Color.Black
        Me.Button47.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button47.Location = New System.Drawing.Point(476, 184)
        Me.Button47.Name = "Button47"
        Me.Button47.Size = New System.Drawing.Size(30, 30)
        Me.Button47.TabIndex = 124
        Me.Button47.Text = "X"
        Me.Button47.UseVisualStyleBackColor = False
        Me.Button47.Visible = False
        '
        'Label134
        '
        Me.Label134.AutoSize = True
        Me.Label134.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label134.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label134.Location = New System.Drawing.Point(8, 223)
        Me.Label134.Name = "Label134"
        Me.Label134.Size = New System.Drawing.Size(142, 46)
        Me.Label134.TabIndex = 123
        Me.Label134.Text = "Descripción de" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "la Etapa:"
        Me.Label134.Visible = False
        '
        'DescEtapa
        '
        Me.DescEtapa.Enabled = False
        Me.DescEtapa.Location = New System.Drawing.Point(153, 220)
        Me.DescEtapa.Multiline = True
        Me.DescEtapa.Name = "DescEtapa"
        Me.DescEtapa.Size = New System.Drawing.Size(317, 65)
        Me.DescEtapa.TabIndex = 122
        '
        'Job
        '
        Me.Job.FormattingEnabled = True
        Me.Job.Location = New System.Drawing.Point(320, 77)
        Me.Job.Name = "Job"
        Me.Job.Size = New System.Drawing.Size(150, 31)
        Me.Job.TabIndex = 121
        '
        'NombreS
        '
        Me.NombreS.FormattingEnabled = True
        Me.NombreS.Location = New System.Drawing.Point(153, 113)
        Me.NombreS.Name = "NombreS"
        Me.NombreS.Size = New System.Drawing.Size(317, 31)
        Me.NombreS.TabIndex = 120
        '
        'Responsable
        '
        Me.Responsable.Enabled = False
        Me.Responsable.Location = New System.Drawing.Point(153, 291)
        Me.Responsable.Name = "Responsable"
        Me.Responsable.Size = New System.Drawing.Size(317, 30)
        Me.Responsable.TabIndex = 91
        Me.Responsable.Visible = False
        '
        'Button31
        '
        Me.Button31.BackColor = System.Drawing.Color.Black
        Me.Button31.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button31.Location = New System.Drawing.Point(484, 292)
        Me.Button31.Name = "Button31"
        Me.Button31.Size = New System.Drawing.Size(154, 60)
        Me.Button31.TabIndex = 90
        Me.Button31.Text = "ASIGNAR RESPONSABLE"
        Me.Button31.UseVisualStyleBackColor = False
        '
        'Etapa
        '
        Me.Etapa.FormattingEnabled = True
        Me.Etapa.Location = New System.Drawing.Point(153, 184)
        Me.Etapa.Name = "Etapa"
        Me.Etapa.Size = New System.Drawing.Size(317, 31)
        Me.Etapa.TabIndex = 88
        Me.Etapa.Visible = False
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label60.Location = New System.Drawing.Point(8, 299)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(129, 23)
        Me.Label60.TabIndex = 87
        Me.Label60.Text = "Responsable:"
        Me.Label60.Visible = False
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label59.Location = New System.Drawing.Point(8, 192)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(67, 23)
        Me.Label59.TabIndex = 86
        Me.Label59.Text = "Etapa:"
        Me.Label59.Visible = False
        '
        'Button30
        '
        Me.Button30.BackColor = System.Drawing.Color.Black
        Me.Button30.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button30.Location = New System.Drawing.Point(486, 412)
        Me.Button30.Name = "Button30"
        Me.Button30.Size = New System.Drawing.Size(86, 60)
        Me.Button30.TabIndex = 85
        Me.Button30.Text = "ENVIAR"
        Me.Button30.UseVisualStyleBackColor = False
        '
        'lider
        '
        Me.lider.FormattingEnabled = True
        Me.lider.Location = New System.Drawing.Point(646, 347)
        Me.lider.Name = "lider"
        Me.lider.Size = New System.Drawing.Size(322, 31)
        Me.lider.TabIndex = 84
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label46.Location = New System.Drawing.Point(506, 355)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(61, 23)
        Me.Label46.TabIndex = 83
        Me.Label46.Text = "Lider:"
        '
        'Comentarios
        '
        Me.Comentarios.Location = New System.Drawing.Point(646, 266)
        Me.Comentarios.Multiline = True
        Me.Comentarios.Name = "Comentarios"
        Me.Comentarios.Size = New System.Drawing.Size(322, 75)
        Me.Comentarios.TabIndex = 11
        '
        'MontoCotizado
        '
        Me.MontoCotizado.Location = New System.Drawing.Point(818, 194)
        Me.MontoCotizado.Name = "MontoCotizado"
        Me.MontoCotizado.Size = New System.Drawing.Size(150, 30)
        Me.MontoCotizado.TabIndex = 9
        Me.MontoCotizado.Text = "N/A"
        '
        'NumeroCotizacion
        '
        Me.NumeroCotizacion.Location = New System.Drawing.Point(818, 158)
        Me.NumeroCotizacion.Name = "NumeroCotizacion"
        Me.NumeroCotizacion.Size = New System.Drawing.Size(150, 30)
        Me.NumeroCotizacion.TabIndex = 8
        Me.NumeroCotizacion.Text = "N/A"
        '
        'Descripcion
        '
        Me.Descripcion.Location = New System.Drawing.Point(651, 42)
        Me.Descripcion.Multiline = True
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.Size = New System.Drawing.Size(317, 75)
        Me.Descripcion.TabIndex = 6
        '
        'FechaPO
        '
        Me.FechaPO.Location = New System.Drawing.Point(320, 363)
        Me.FechaPO.Name = "FechaPO"
        Me.FechaPO.Size = New System.Drawing.Size(150, 30)
        Me.FechaPO.TabIndex = 2
        Me.FechaPO.Text = "N/A"
        '
        'PO
        '
        Me.PO.Location = New System.Drawing.Point(320, 327)
        Me.PO.Name = "PO"
        Me.PO.Size = New System.Drawing.Size(150, 30)
        Me.PO.TabIndex = 1
        Me.PO.Text = "N/A"
        '
        'DiasTranscurridos
        '
        Me.DiasTranscurridos.Enabled = False
        Me.DiasTranscurridos.Location = New System.Drawing.Point(320, 42)
        Me.DiasTranscurridos.Name = "DiasTranscurridos"
        Me.DiasTranscurridos.Size = New System.Drawing.Size(150, 30)
        Me.DiasTranscurridos.TabIndex = 61
        Me.DiasTranscurridos.Text = "0"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label43.Location = New System.Drawing.Point(8, 120)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(84, 23)
        Me.Label43.TabIndex = 81
        Me.Label43.Text = "Nombre:"
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Black
        Me.Button8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button8.Location = New System.Drawing.Point(578, 412)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(60, 60)
        Me.Button8.TabIndex = 80
        Me.Button8.Text = "VER"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Contactos
        '
        Me.Contactos.FormattingEnabled = True
        Me.Contactos.Location = New System.Drawing.Point(153, 437)
        Me.Contactos.Name = "Contactos"
        Me.Contactos.Size = New System.Drawing.Size(317, 31)
        Me.Contactos.TabIndex = 4
        '
        'Cliente
        '
        Me.Cliente.FormattingEnabled = True
        Me.Cliente.Location = New System.Drawing.Point(153, 400)
        Me.Cliente.Name = "Cliente"
        Me.Cliente.Size = New System.Drawing.Size(317, 31)
        Me.Cliente.TabIndex = 3
        '
        'LSG
        '
        Me.LSG.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LSG.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LSG.ForeColor = System.Drawing.Color.RoyalBlue
        Me.LSG.Image = CType(resources.GetObject("LSG.Image"), System.Drawing.Image)
        Me.LSG.Location = New System.Drawing.Point(908, 412)
        Me.LSG.Name = "LSG"
        Me.LSG.Size = New System.Drawing.Size(60, 60)
        Me.LSG.TabIndex = 74
        Me.LSG.UseVisualStyleBackColor = True
        '
        'ESG
        '
        Me.ESG.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ESG.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ESG.ForeColor = System.Drawing.Color.RoyalBlue
        Me.ESG.Image = CType(resources.GetObject("ESG.Image"), System.Drawing.Image)
        Me.ESG.Location = New System.Drawing.Point(842, 412)
        Me.ESG.Name = "ESG"
        Me.ESG.Size = New System.Drawing.Size(60, 60)
        Me.ESG.TabIndex = 73
        Me.ESG.UseVisualStyleBackColor = True
        '
        'MSG
        '
        Me.MSG.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MSG.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MSG.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MSG.Image = CType(resources.GetObject("MSG.Image"), System.Drawing.Image)
        Me.MSG.Location = New System.Drawing.Point(776, 412)
        Me.MSG.Name = "MSG"
        Me.MSG.Size = New System.Drawing.Size(60, 60)
        Me.MSG.TabIndex = 72
        Me.MSG.UseVisualStyleBackColor = True
        '
        'GSG
        '
        Me.GSG.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GSG.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GSG.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GSG.Image = CType(resources.GetObject("GSG.Image"), System.Drawing.Image)
        Me.GSG.Location = New System.Drawing.Point(710, 412)
        Me.GSG.Name = "GSG"
        Me.GSG.Size = New System.Drawing.Size(60, 60)
        Me.GSG.TabIndex = 71
        Me.GSG.UseVisualStyleBackColor = True
        '
        'BSG
        '
        Me.BSG.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BSG.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BSG.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BSG.Image = CType(resources.GetObject("BSG.Image"), System.Drawing.Image)
        Me.BSG.Location = New System.Drawing.Point(644, 412)
        Me.BSG.Name = "BSG"
        Me.BSG.Size = New System.Drawing.Size(60, 60)
        Me.BSG.TabIndex = 70
        Me.BSG.UseVisualStyleBackColor = True
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.CustomFormat = "yyyy-MM-dd"
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker2.Location = New System.Drawing.Point(818, 123)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(150, 30)
        Me.DateTimePicker2.TabIndex = 7
        Me.DateTimePicker2.Value = New Date(2018, 9, 13, 0, 0, 0, 0)
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CustomFormat = "yyyy-MM-dd"
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker1.Location = New System.Drawing.Point(320, 6)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(150, 30)
        Me.DateTimePicker1.TabIndex = 60
        Me.DateTimePicker1.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Moneda
        '
        Me.Moneda.FormattingEnabled = True
        Me.Moneda.Items.AddRange(New Object() {"Pesos", "Dólares"})
        Me.Moneda.Location = New System.Drawing.Point(818, 229)
        Me.Moneda.Name = "Moneda"
        Me.Moneda.Size = New System.Drawing.Size(150, 31)
        Me.Moneda.TabIndex = 10
        Me.Moneda.Text = "N/A"
        '
        'Categoria
        '
        Me.Categoria.FormattingEnabled = True
        Me.Categoria.Items.AddRange(New Object() {"Proyecto", "Maquinado/Soldadura", "Mantenimiento preventivo", "Mantenimiento correctivo", "Venta de partes ó software", "Marcado Láser", "CAD/Diseño", "Cursos/Asesorías", "Otras ventas/Otros ingresos"})
        Me.Categoria.Location = New System.Drawing.Point(651, 5)
        Me.Categoria.Name = "Categoria"
        Me.Categoria.Size = New System.Drawing.Size(317, 31)
        Me.Categoria.TabIndex = 5
        '
        'Prioridad
        '
        Me.Prioridad.FormattingEnabled = True
        Me.Prioridad.Items.AddRange(New Object() {"1-En proceso", "2-Cotizando", "3-Cotizado/Dar seguimiento", "4-Cotizado/Detenido", "5-Por cotizar/Venta poco probable", "6-Pendientes administrativos", "0-CERRADO"})
        Me.Prioridad.Location = New System.Drawing.Point(153, 148)
        Me.Prioridad.Name = "Prioridad"
        Me.Prioridad.Size = New System.Drawing.Size(317, 31)
        Me.Prioridad.TabIndex = 57
        Me.Prioridad.Text = "2-Cotizando"
        '
        'Comen
        '
        Me.Comen.AutoSize = True
        Me.Comen.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Comen.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Comen.Location = New System.Drawing.Point(506, 266)
        Me.Comen.Name = "Comen"
        Me.Comen.Size = New System.Drawing.Size(117, 23)
        Me.Comen.TabIndex = 56
        Me.Comen.Text = "Comentario:"
        '
        'Mon
        '
        Me.Mon.AutoSize = True
        Me.Mon.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Mon.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Mon.Location = New System.Drawing.Point(506, 237)
        Me.Mon.Name = "Mon"
        Me.Mon.Size = New System.Drawing.Size(85, 23)
        Me.Mon.TabIndex = 55
        Me.Mon.Text = "Moneda:"
        '
        'MontoCot
        '
        Me.MontoCot.AutoSize = True
        Me.MontoCot.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MontoCot.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MontoCot.Location = New System.Drawing.Point(506, 201)
        Me.MontoCot.Name = "MontoCot"
        Me.MontoCot.Size = New System.Drawing.Size(246, 23)
        Me.MontoCot.TabIndex = 54
        Me.MontoCot.Text = "Monto Cotizado (Con I.V.A):"
        '
        'NumeroCot
        '
        Me.NumeroCot.AutoSize = True
        Me.NumeroCot.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumeroCot.ForeColor = System.Drawing.Color.RoyalBlue
        Me.NumeroCot.Location = New System.Drawing.Point(506, 165)
        Me.NumeroCot.Name = "NumeroCot"
        Me.NumeroCot.Size = New System.Drawing.Size(209, 23)
        Me.NumeroCot.TabIndex = 53
        Me.NumeroCot.Text = "Número de Cotización:"
        '
        'FechaCompromisoE
        '
        Me.FechaCompromisoE.AutoSize = True
        Me.FechaCompromisoE.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaCompromisoE.ForeColor = System.Drawing.Color.RoyalBlue
        Me.FechaCompromisoE.Location = New System.Drawing.Point(506, 130)
        Me.FechaCompromisoE.Name = "FechaCompromisoE"
        Me.FechaCompromisoE.Size = New System.Drawing.Size(286, 23)
        Me.FechaCompromisoE.TabIndex = 52
        Me.FechaCompromisoE.Text = "Fecha Compromiso de Entrega:"
        '
        'Desc
        '
        Me.Desc.AutoSize = True
        Me.Desc.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Desc.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Desc.Location = New System.Drawing.Point(506, 42)
        Me.Desc.Name = "Desc"
        Me.Desc.Size = New System.Drawing.Size(120, 23)
        Me.Desc.TabIndex = 51
        Me.Desc.Text = "Descripción:"
        '
        'Cate
        '
        Me.Cate.AutoSize = True
        Me.Cate.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cate.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Cate.Location = New System.Drawing.Point(506, 13)
        Me.Cate.Name = "Cate"
        Me.Cate.Size = New System.Drawing.Size(101, 23)
        Me.Cate.TabIndex = 50
        Me.Cate.Text = "Categoria:"
        '
        'Cont
        '
        Me.Cont.AutoSize = True
        Me.Cont.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cont.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Cont.Location = New System.Drawing.Point(7, 445)
        Me.Cont.Name = "Cont"
        Me.Cont.Size = New System.Drawing.Size(96, 23)
        Me.Cont.TabIndex = 49
        Me.Cont.Text = "Contacto:"
        '
        'Clien
        '
        Me.Clien.AutoSize = True
        Me.Clien.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clien.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Clien.Location = New System.Drawing.Point(8, 408)
        Me.Clien.Name = "Clien"
        Me.Clien.Size = New System.Drawing.Size(77, 23)
        Me.Clien.TabIndex = 48
        Me.Clien.Text = "Cliente:"
        '
        'FRPO
        '
        Me.FRPO.AutoSize = True
        Me.FRPO.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FRPO.ForeColor = System.Drawing.Color.RoyalBlue
        Me.FRPO.Location = New System.Drawing.Point(8, 366)
        Me.FRPO.Name = "FRPO"
        Me.FRPO.Size = New System.Drawing.Size(221, 23)
        Me.FRPO.TabIndex = 47
        Me.FRPO.Text = "Fecha de Recibo de P.O:"
        '
        'NPO
        '
        Me.NPO.AutoSize = True
        Me.NPO.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NPO.ForeColor = System.Drawing.Color.RoyalBlue
        Me.NPO.Location = New System.Drawing.Point(8, 334)
        Me.NPO.Name = "NPO"
        Me.NPO.Size = New System.Drawing.Size(42, 23)
        Me.NPO.TabIndex = 46
        Me.NPO.Text = "P.O:"
        '
        'P
        '
        Me.P.AutoSize = True
        Me.P.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.P.ForeColor = System.Drawing.Color.RoyalBlue
        Me.P.Location = New System.Drawing.Point(8, 156)
        Me.P.Name = "P"
        Me.P.Size = New System.Drawing.Size(95, 23)
        Me.P.TabIndex = 45
        Me.P.Text = "Prioridad:"
        '
        'NJob
        '
        Me.NJob.AutoSize = True
        Me.NJob.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NJob.ForeColor = System.Drawing.Color.RoyalBlue
        Me.NJob.Location = New System.Drawing.Point(8, 85)
        Me.NJob.Name = "NJob"
        Me.NJob.Size = New System.Drawing.Size(91, 23)
        Me.NJob.TabIndex = 44
        Me.NJob.Text = "# de Job:"
        '
        'DiasT
        '
        Me.DiasT.AutoSize = True
        Me.DiasT.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DiasT.ForeColor = System.Drawing.Color.RoyalBlue
        Me.DiasT.Location = New System.Drawing.Point(8, 49)
        Me.DiasT.Name = "DiasT"
        Me.DiasT.Size = New System.Drawing.Size(182, 23)
        Me.DiasT.TabIndex = 43
        Me.DiasT.Text = "Días Transcurridos:"
        '
        'FechaSolicitudC
        '
        Me.FechaSolicitudC.AutoSize = True
        Me.FechaSolicitudC.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaSolicitudC.ForeColor = System.Drawing.Color.RoyalBlue
        Me.FechaSolicitudC.Location = New System.Drawing.Point(8, 13)
        Me.FechaSolicitudC.Name = "FechaSolicitudC"
        Me.FechaSolicitudC.Size = New System.Drawing.Size(304, 23)
        Me.FechaSolicitudC.TabIndex = 42
        Me.FechaSolicitudC.Text = "Fecha de Solicitud de Cotización:"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage8)
        Me.TabControl1.Controls.Add(Me.TabPage9)
        Me.TabControl1.Controls.Add(Me.TabPage10)
        Me.TabControl1.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(0, 65)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(985, 514)
        Me.TabControl1.TabIndex = 9
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.Black
        Me.TabPage2.Controls.Add(Me.DGCP)
        Me.TabPage2.Controls.Add(Me.Button12)
        Me.TabPage2.Controls.Add(Me.Button13)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Location = New System.Drawing.Point(4, 32)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(977, 478)
        Me.TabPage2.TabIndex = 7
        Me.TabPage2.Text = "Cotizaciones"
        '
        'DGCP
        '
        Me.DGCP.AllowUserToAddRows = False
        Me.DGCP.AllowUserToDeleteRows = False
        Me.DGCP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DGCP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGCP.Location = New System.Drawing.Point(8, 74)
        Me.DGCP.Name = "DGCP"
        Me.DGCP.ReadOnly = True
        Me.DGCP.RowHeadersVisible = False
        Me.DGCP.Size = New System.Drawing.Size(960, 396)
        Me.DGCP.TabIndex = 107
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.Black
        Me.Button12.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button12.Location = New System.Drawing.Point(740, 8)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(112, 33)
        Me.Button12.TabIndex = 106
        Me.Button12.Text = "Exportar"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.Color.Black
        Me.Button13.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button13.Location = New System.Drawing.Point(858, 8)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(110, 60)
        Me.Button13.TabIndex = 105
        Me.Button13.Text = "Actualizar"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label11.Location = New System.Drawing.Point(350, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(208, 38)
        Me.Label11.TabIndex = 41
        Me.Label11.Text = "Cotizaciones"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.Black
        Me.TabPage3.Controls.Add(Me.Label3)
        Me.TabPage3.Controls.Add(Me.Button27)
        Me.TabPage3.Controls.Add(Me.Button26)
        Me.TabPage3.Controls.Add(Me.DTPSP)
        Me.TabPage3.Controls.Add(Me.Label48)
        Me.TabPage3.Controls.Add(Me.Label47)
        Me.TabPage3.Controls.Add(Me.CONSP)
        Me.TabPage3.Controls.Add(Me.Label20)
        Me.TabPage3.Controls.Add(Me.CLISP)
        Me.TabPage3.Controls.Add(Me.Label17)
        Me.TabPage3.Controls.Add(Me.LPDP)
        Me.TabPage3.Controls.Add(Me.DSP)
        Me.TabPage3.Controls.Add(Me.Label12)
        Me.TabPage3.Controls.Add(Me.JOBSP)
        Me.TabPage3.Controls.Add(Me.Label45)
        Me.TabPage3.Controls.Add(Me.Button25)
        Me.TabPage3.Controls.Add(Me.Button24)
        Me.TabPage3.Controls.Add(Me.Label44)
        Me.TabPage3.Controls.Add(Me.Label21)
        Me.TabPage3.Location = New System.Drawing.Point(4, 32)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(977, 478)
        Me.TabPage3.TabIndex = 6
        Me.TabPage3.Text = "Seguimiento de Proyectos"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial Black", 15.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.Location = New System.Drawing.Point(181, 248)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(220, 28)
        Me.Label3.TabIndex = 141
        Me.Label3.Text = "BLOQUE GENERAL"
        '
        'Button27
        '
        Me.Button27.BackColor = System.Drawing.Color.Black
        Me.Button27.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button27.Location = New System.Drawing.Point(186, 279)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(172, 61)
        Me.Button27.TabIndex = 140
        Me.Button27.Text = "BLOQUE GENERAL"
        Me.Button27.UseVisualStyleBackColor = False
        '
        'Button26
        '
        Me.Button26.BackColor = System.Drawing.Color.Black
        Me.Button26.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button26.Location = New System.Drawing.Point(617, 279)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(172, 61)
        Me.Button26.TabIndex = 138
        Me.Button26.Text = "BLOQUE ESTACIÓN"
        Me.Button26.UseVisualStyleBackColor = False
        '
        'DTPSP
        '
        Me.DTPSP.CustomFormat = "yyyy-MM-dd"
        Me.DTPSP.Enabled = False
        Me.DTPSP.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTPSP.Location = New System.Drawing.Point(818, 114)
        Me.DTPSP.Name = "DTPSP"
        Me.DTPSP.Size = New System.Drawing.Size(150, 30)
        Me.DTPSP.TabIndex = 135
        Me.DTPSP.Value = New Date(2018, 9, 13, 0, 0, 0, 0)
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label48.Location = New System.Drawing.Point(549, 122)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(176, 23)
        Me.Label48.TabIndex = 136
        Me.Label48.Text = "Fecha  de Entrega:"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label47.Location = New System.Drawing.Point(549, 85)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(96, 23)
        Me.Label47.TabIndex = 134
        Me.Label47.Text = "Contacto:"
        '
        'CONSP
        '
        Me.CONSP.Enabled = False
        Me.CONSP.Location = New System.Drawing.Point(651, 78)
        Me.CONSP.Name = "CONSP"
        Me.CONSP.Size = New System.Drawing.Size(317, 30)
        Me.CONSP.TabIndex = 133
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label20.Location = New System.Drawing.Point(549, 52)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(77, 23)
        Me.Label20.TabIndex = 132
        Me.Label20.Text = "Cliente:"
        '
        'CLISP
        '
        Me.CLISP.AcceptsReturn = True
        Me.CLISP.Enabled = False
        Me.CLISP.Location = New System.Drawing.Point(651, 45)
        Me.CLISP.Name = "CLISP"
        Me.CLISP.Size = New System.Drawing.Size(317, 30)
        Me.CLISP.TabIndex = 131
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label17.Location = New System.Drawing.Point(8, 155)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(172, 23)
        Me.Label17.TabIndex = 130
        Me.Label17.Text = "Líder del Proyecto"
        '
        'LPDP
        '
        Me.LPDP.Enabled = False
        Me.LPDP.Location = New System.Drawing.Point(186, 148)
        Me.LPDP.Name = "LPDP"
        Me.LPDP.Size = New System.Drawing.Size(317, 30)
        Me.LPDP.TabIndex = 129
        '
        'DSP
        '
        Me.DSP.Enabled = False
        Me.DSP.Location = New System.Drawing.Point(186, 78)
        Me.DSP.Multiline = True
        Me.DSP.Name = "DSP"
        Me.DSP.Size = New System.Drawing.Size(317, 66)
        Me.DSP.TabIndex = 127
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label12.Location = New System.Drawing.Point(9, 77)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(120, 23)
        Me.Label12.TabIndex = 128
        Me.Label12.Text = "Descripción:"
        '
        'JOBSP
        '
        Me.JOBSP.FormattingEnabled = True
        Me.JOBSP.Location = New System.Drawing.Point(186, 43)
        Me.JOBSP.Name = "JOBSP"
        Me.JOBSP.Size = New System.Drawing.Size(317, 31)
        Me.JOBSP.TabIndex = 125
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Arial Black", 15.0!, System.Drawing.FontStyle.Bold)
        Me.Label45.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label45.Location = New System.Drawing.Point(613, 248)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(228, 28)
        Me.Label45.TabIndex = 108
        Me.Label45.Text = "BLOQUE ESTACIÓN"
        '
        'Button25
        '
        Me.Button25.BackColor = System.Drawing.Color.Black
        Me.Button25.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button25.Location = New System.Drawing.Point(617, 184)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(172, 61)
        Me.Button25.TabIndex = 106
        Me.Button25.Text = "CREAR BLOQUE ESTACIÓN"
        Me.Button25.UseVisualStyleBackColor = False
        '
        'Button24
        '
        Me.Button24.BackColor = System.Drawing.Color.Black
        Me.Button24.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button24.Location = New System.Drawing.Point(186, 184)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(172, 61)
        Me.Button24.TabIndex = 105
        Me.Button24.Text = "CREAR BLOQUE GENERAL"
        Me.Button24.UseVisualStyleBackColor = False
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label44.Location = New System.Drawing.Point(345, 0)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(402, 38)
        Me.Label44.TabIndex = 90
        Me.Label44.Text = "Seguimiento por Proyecto"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label21.Location = New System.Drawing.Point(9, 51)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(91, 23)
        Me.Label21.TabIndex = 84
        Me.Label21.Text = "# de Job:"
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(984, 579)
        Me.Controls.Add(Me.MensajeVentas)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.CerrarS)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Form2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.TabPage10.ResumeLayout(False)
        Me.TabPage10.PerformLayout()
        Me.TabPage9.ResumeLayout(False)
        Me.TabPage9.PerformLayout()
        Me.TabPage8.ResumeLayout(False)
        Me.TabPage8.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.DGCP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Public WithEvents MensajeVentas As Label
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents Label43 As Label
    Friend WithEvents Button8 As Button
    Friend WithEvents Contactos As ComboBox
    Friend WithEvents Cliente As ComboBox
    Friend WithEvents LSG As Button
    Friend WithEvents ESG As Button
    Friend WithEvents MSG As Button
    Friend WithEvents GSG As Button
    Friend WithEvents BSG As Button
    Friend WithEvents Comentarios As TextBox
    Friend WithEvents MontoCotizado As TextBox
    Friend WithEvents NumeroCotizacion As TextBox
    Friend WithEvents DateTimePicker2 As DateTimePicker
    Friend WithEvents Descripcion As TextBox
    Friend WithEvents PO As TextBox
    Friend WithEvents DiasTranscurridos As TextBox
    Friend WithEvents DateTimePicker1 As DateTimePicker
    Friend WithEvents Moneda As ComboBox
    Friend WithEvents Categoria As ComboBox
    Friend WithEvents Prioridad As ComboBox
    Friend WithEvents Comen As Label
    Friend WithEvents Mon As Label
    Friend WithEvents MontoCot As Label
    Friend WithEvents NumeroCot As Label
    Friend WithEvents FechaCompromisoE As Label
    Friend WithEvents Desc As Label
    Friend WithEvents Cate As Label
    Friend WithEvents Cont As Label
    Friend WithEvents Clien As Label
    Friend WithEvents FRPO As Label
    Friend WithEvents NPO As Label
    Friend WithEvents P As Label
    Friend WithEvents NJob As Label
    Friend WithEvents DiasT As Label
    Friend WithEvents FechaSolicitudC As Label
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents Button10 As Button
    Friend WithEvents LClient As Button
    Friend WithEvents TelCel As TextBox
    Friend WithEvents TelOfi As TextBox
    Friend WithEvents CorreoElectronico As TextBox
    Friend WithEvents Departamento As TextBox
    Friend WithEvents TCel As Label
    Friend WithEvents TOfi As Label
    Friend WithEvents CElec As Label
    Friend WithEvents Depar As Label
    Friend WithEvents EC As Button
    Friend WithEvents MC As Button
    Friend WithEvents GC As Button
    Friend WithEvents BC As Button
    Friend WithEvents NContacto As Label
    Friend WithEvents NEmpresa As Label
    Friend WithEvents Clientes As Label
    Friend WithEvents TabPage8 As TabPage
    Friend WithEvents Button11 As Button
    Public WithEvents NombreVendedor As TextBox
    Friend WithEvents CodVisita As Label
    Friend WithEvents LV As Button
    Friend WithEvents EV As Button
    Friend WithEvents MV As Button
    Friend WithEvents GV As Button
    Friend WithEvents BV As Button
    Friend WithEvents DescVisita As TextBox
    Friend WithEvents DescripV As Label
    Friend WithEvents VV As Label
    Friend WithEvents ContactosVisita As ComboBox
    Friend WithEvents ClienteVisita As ComboBox
    Friend WithEvents ConV As Label
    Friend WithEvents CV As Label
    Friend WithEvents DateTimePicker5 As DateTimePicker
    Friend WithEvents FPVisita As Label
    Friend WithEvents DateTimePicker4 As DateTimePicker
    Friend WithEvents Label2 As Label
    Friend WithEvents Visitas As Label
    Friend WithEvents TabPage9 As TabPage
    Friend WithEvents Button9 As Button
    Friend WithEvents FechaM As DateTimePicker
    Friend WithEvents Label26 As Label
    Friend WithEvents Estado As ComboBox
    Friend WithEvents Label25 As Label
    Friend WithEvents Estatus As ComboBox
    Friend WithEvents Label24 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Titulo As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents NombreP As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents FechaA As DateTimePicker
    Friend WithEvents Label5 As Label
    Friend WithEvents DescripcionA As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents TabPage10 As TabPage
    Friend WithEvents PrioridadP As TextBox
    Friend WithEvents Label41 As Label
    Friend WithEvents Button22 As Button
    Friend WithEvents Label42 As Label
    Friend WithEvents FechaJD As DateTimePicker
    Friend WithEvents Label40 As Label
    Friend WithEvents Button21 As Button
    Friend WithEvents Label39 As Label
    Friend WithEvents FechaPOD As DateTimePicker
    Friend WithEvents Button20 As Button
    Friend WithEvents Label37 As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents JobRAJ As ComboBox
    Friend WithEvents Button19 As Button
    Friend WithEvents Label35 As Label
    Friend WithEvents Label36 As Label
    Friend WithEvents NombreRR As ComboBox
    Friend WithEvents Button18 As Button
    Friend WithEvents Label34 As Label
    Friend WithEvents Button7 As Button
    Friend WithEvents ClienteReporte As ComboBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Button6 As Button
    Friend WithEvents Label22 As Label
    Friend WithEvents btnVisitas As Button
    Friend WithEvents Fecha2 As DateTimePicker
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Fecha1 As DateTimePicker
    Friend WithEvents Label10 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents CerrarS As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Button23 As Button
    Friend WithEvents Label16 As Label
    Friend WithEvents FechaPO As TextBox
    Friend WithEvents lider As ComboBox
    Friend WithEvents Label46 As Label
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents Label45 As Label
    Friend WithEvents Button25 As Button
    Friend WithEvents Button24 As Button
    Friend WithEvents Label44 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents JOBSP As ComboBox
    Public WithEvents LPDP As TextBox
    Friend WithEvents DSP As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents DTPSP As DateTimePicker
    Friend WithEvents Label48 As Label
    Friend WithEvents Label47 As Label
    Public WithEvents CONSP As TextBox
    Friend WithEvents Label20 As Label
    Public WithEvents CLISP As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Button26 As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents Button27 As Button
    Friend WithEvents NotaA As TextBox
    Friend WithEvents Label49 As Label
    Friend WithEvents ComentarioA As TextBox
    Friend WithEvents Label50 As Label
    Friend WithEvents Button29 As Button
    Friend WithEvents JBG As ComboBox
    Friend WithEvents Label55 As Label
    Friend WithEvents Label54 As Label
    Friend WithEvents Button28 As Button
    Friend WithEvents Label53 As Label
    Friend WithEvents JBE As ComboBox
    Friend WithEvents Label51 As Label
    Friend WithEvents NBBE As ComboBox
    Friend WithEvents Label52 As Label
    Friend WithEvents Button30 As Button
    Friend WithEvents ContJob As TextBox
    Friend WithEvents Label58 As Label
    Friend WithEvents ClienJob As TextBox
    Friend WithEvents Label57 As Label
    Friend WithEvents DescJob As TextBox
    Friend WithEvents Label56 As Label
    Friend WithEvents JobA As ComboBox
    Friend WithEvents Button31 As Button
    Friend WithEvents Etapa As ComboBox
    Friend WithEvents Label60 As Label
    Friend WithEvents Label59 As Label
    Public WithEvents Responsable As TextBox
    Friend WithEvents NombreS As ComboBox
    Friend WithEvents Contacto As ComboBox
    Friend WithEvents Empresa As ComboBox
    Friend WithEvents Job As ComboBox
    Friend WithEvents CodigoVisita As ComboBox
    Friend WithEvents CodigoAct As ComboBox
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents Label11 As Label
    Friend WithEvents Button12 As Button
    Friend WithEvents Button13 As Button
    Friend WithEvents DGCP As DataGridView
    Friend WithEvents Label134 As Label
    Friend WithEvents DescEtapa As TextBox
    Friend WithEvents Button47 As Button
End Class
