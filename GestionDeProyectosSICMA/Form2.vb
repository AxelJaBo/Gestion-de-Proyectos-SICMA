﻿Imports System.Data.Odbc
Public Class Form2
    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call conectar()
        CodigoActividad()
        CodigoVisitaX()
        Llenar0()
        Llenar3()
        Llenar5()
        Llenar6()
        Llenar7()
        Llenar11()
        Llenar12()
        Llenar14()
        Llenar16()
        Llenar17()
        Llenar18()
        Llenar22()

        DateTimePicker1.Value = DateTime.Now
        DateTimePicker2.Value = DateTime.Now
        DateTimePicker4.Value = DateTime.Now
        DateTimePicker5.Value = DateTime.Now
        FechaA.Value = DateTime.Now
        Fecha1.Value = DateTime.Now
        Fecha2.Value = DateTime.Now
        FechaPOD.Value = DateTime.Now
        FechaJD.Value = DateTime.Now
        DTPSP.Value = DateTime.Now
        FechaM.Value = DateTime.Now
    End Sub

    'Esta sección de código permite cerrar sesión de usuario en el sistema, y regresar al formulario de login.'
    Private Sub CerrarS_Click(sender As Object, e As EventArgs) Handles CerrarS.Click
        MessageBox.Show("La Sesión se ha Cerrado Correctamente.")
        Try
            Dim controldeusuario As New OdbcCommand("UPDATE gestióndeproyectossicma.usuarios SET control_user='0' where nombre ='" & MensajeVentas.Text.ToArray & "'", cn)
            controldeusuario.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Form1.Show()
        Me.Dispose()
    End Sub

    Public Sub CodigoActividad()
        Dim consultarCodigo As New OdbcCommand("SELECT Cod_act FROM gestióndeproyectossicma.actividades ORDER BY idactividades DESC LIMIT 1", cn)
        Dim datos As OdbcDataReader
        Try
            datos = consultarCodigo.ExecuteReader
            If datos.Read Then
                CodigoAct.Text() = datos.Item("Cod_act").ToString()
                Dim M As Integer
                M = CodigoAct.Text
                CodigoAct.Text = M + 1
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub CodigoVisitaX()
        Dim consultarCodigoV As New OdbcCommand("SELECT Cod_Visita FROM gestióndeproyectossicma.visitas ORDER BY Cod_Visita DESC LIMIT 1", cn)
        Dim datos As OdbcDataReader
        Try
            datos = consultarCodigoV.ExecuteReader
            If datos.Read Then
                CodigoVisita.Text() = datos.Item("Cod_Visita").ToString()
                Dim O As Integer
                O = CodigoVisita.Text
                CodigoVisita.Text = O + 1
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Aquí se consulta el # de job de la tabla seguimiento y se le suma 1 para obtener el job siguiente.'
    Public Sub Llenar0()
        Dim consultarjob As New OdbcCommand("SELECT Job FROM gestióndeproyectossicma.seguimiento ORDER BY idSeguimiento DESC LIMIT 1", cn)
        Dim datos As OdbcDataReader
        Try
            datos = consultarjob.ExecuteReader
            If datos.Read Then
                Job.Text() = datos.Item("Job").ToString()
                Dim N As Integer
                N = Job.Text
                Job.Text = N + 1
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub Llenar1()
        Dim da = New OdbcDataAdapter("SELECT DISTINCT Cliente FROM gestióndeproyectossicma.clientes ORDER BY Cliente ASC", cn)
        Dim dt = New DataTable
        da.Fill(dt)
        Cliente.DataSource = dt
        Cliente.DisplayMember = "Cliente"
    End Sub

    'Se consulta el nombre del contacto de la tabla clientes dependiendo el nombre de la empresa.'
    Public Sub Llenar2()
        Dim da2 = New OdbcDataAdapter("SELECT idClientes, Contacto FROM gestióndeproyectossicma.clientes WHERE Cliente='" & Cliente.Text.ToString & "'", cn)
        Dim dt2 = New DataTable
        da2.Fill(dt2)
        Contactos.DataSource = dt2
        Contactos.DisplayMember = "Contacto"
        Contactos.ValueMember = "idClientes"
    End Sub

    'Se consulta los clientes de la tabla clientes se ordena de forma ascendente.'
    Public Sub Llenar3()
        Dim daV = New OdbcDataAdapter("SELECT DISTINCT Cliente FROM gestióndeproyectossicma.clientes ORDER BY Cliente ASC", cn)
        Dim dtV = New DataTable
        daV.Fill(dtV)
        ClienteVisita.DataSource = dtV
        ClienteVisita.DisplayMember = "Cliente"
    End Sub

    'Se consulta el nombre del contacto de la tabla clientes dependiendo el nombre de la empresa.'
    Public Sub Llenar4()
        Dim daV2 = New OdbcDataAdapter("SELECT idClientes, Contacto FROM gestióndeproyectossicma.clientes WHERE Cliente='" & ClienteVisita.Text.ToString & "'", cn)
        Dim dtV2 = New DataTable
        daV2.Fill(dtV2)
        ContactosVisita.DataSource = dtV2
        ContactosVisita.DisplayMember = "Contacto"
        ContactosVisita.ValueMember = "idClientes"
    End Sub

    'Se obtiene el nombre de la persona del área de proyecto.'
    Public Sub Llenar5()
        Dim daP2 = New OdbcDataAdapter("SELECT idUsuarios, nombre FROM gestióndeproyectossicma.usuarios WHERE Tipo_Usuario= 'Proyectos' OR Tipo_Usuario='Ventas'", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        NombreP.DataSource = dtP2
        NombreP.DisplayMember = "nombre"
        NombreP.ValueMember = "idUsuarios"
    End Sub

    'Se obtiene el Cliente y se ordena por Cliente de manera ascendente.'
    Public Sub Llenar6()
        Dim daR = New OdbcDataAdapter("SELECT DISTINCT Cliente FROM gestióndeproyectossicma.clientes ORDER BY Cliente ASC", cn)
        Dim dtR = New DataTable
        daR.Fill(dtR)
        ClienteReporte.DataSource = dtR
        ClienteReporte.DisplayMember = "Cliente"
    End Sub

    'Se obtiene un número de Job.'
    Public Sub Llenar7()
        Dim daP2 = New OdbcDataAdapter("SELECT Job FROM gestióndeproyectossicma.seguimiento ORDER BY Job ASC", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        JobA.DataSource = dtP2
        JobA.DisplayMember = "Job"
    End Sub

    'Se obtiene el nombre de la persona del área de proyecto.'
    Public Sub Llenar11()
        Dim daP2 = New OdbcDataAdapter("SELECT idUsuarios, nombre FROM gestióndeproyectossicma.usuarios WHERE Tipo_Usuario='Proyectos' OR Tipo_Usuario='Ventas'", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        NombreRR.DataSource = dtP2
        NombreRR.DisplayMember = "nombre"
        NombreRR.ValueMember = "idUsuarios"
    End Sub

    'Se obtienen los Jobs y se obtiene de forma ascendente.'
    Public Sub Llenar12()
        Dim daP2 = New OdbcDataAdapter("SELECT Job FROM gestióndeproyectossicma.actividades ORDER BY Job ASC", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        JobRAJ.DataSource = dtP2
        JobRAJ.DisplayMember = "Job"
    End Sub

    Public Sub Llenar13()
        Dim daP2 = New OdbcDataAdapter("SELECT idUsuarios, nombre FROM gestióndeproyectossicma.usuarios WHERE Tipo_Usuario= 'Proyectos' OR Tipo_Usuario='Ventas'", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        lider.DataSource = dtP2
        lider.DisplayMember = "nombre"
        lider.ValueMember = "idUsuarios"
    End Sub

    Public Sub Llenar14()
        Dim daP2 = New OdbcDataAdapter("SELECT Job FROM gestióndeproyectossicma.seguimiento ORDER BY IdSeguimiento ASC", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        JOBSP.DataSource = dtP2
        JOBSP.DisplayMember = "Job"
    End Sub

    Public Sub Llenar15()
        If JOBSP.Text = "" Then
            MsgBox("El campo Job se encuentra vacío.")
        Else
            Dim consultarjob As New OdbcCommand("SELECT Job, Descripcion, lider, Cliente, Contactos, FechaCompromisoEntrega FROM gestióndeproyectossicma.seguimiento WHERE Job ='" & JOBSP.Text.ToArray & "'", cn)
            Dim datos As OdbcDataReader
            Try
                datos = consultarjob.ExecuteReader
                If datos.Read Then
                    JOBSP.Text() = datos.Item("Job").ToString()
                    DSP.Text() = datos.Item("Descripcion").ToString()
                    LPDP.Text() = datos.Item("lider").ToString()
                    CLISP.Text() = datos.Item("Cliente").ToString()
                    CONSP.Text() = datos.Item("Contactos").ToString()
                    DTPSP.Text() = datos.Item("FechaCompromisoEntrega").ToString()
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Public Sub Llenar16()
        Dim daP2 = New OdbcDataAdapter("SELECT DISTINCT job FROM gestióndeproyectossicma.bloqueestacion ORDER BY job ASC", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        JBE.DataSource = dtP2
        JBE.DisplayMember = "job"
    End Sub
    Public Sub Llenar17()
        Dim da = New OdbcDataAdapter("SELECT DISTINCT nombre FROM gestióndeproyectossicma.bloqueestacion WHERE job ='" & JBE.Text & "' ORDER BY nombre ASC", cn)
        Dim dt = New DataTable
        da.Fill(dt)
        NBBE.DataSource = dt
        NBBE.DisplayMember = "nombre"
    End Sub
    Public Sub Llenar18()
        Dim daP2 = New OdbcDataAdapter("SELECT DISTINCT job FROM gestióndeproyectossicma.bloquegeneral ORDER BY job ASC", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        JBG.DataSource = dtP2
        JBG.DisplayMember = "job"
    End Sub

    Public Sub Llenar19()
        If JobA.Text = "" Then
            MsgBox("El campo Job se encuentra vacío, es necesario que escriba el número de Job para poder realizar la busqueda.")
        Else
            Dim consultarjob As New OdbcCommand("SELECT Cliente, Contactos, Descripcion FROM gestióndeproyectossicma.seguimiento where Job ='" & JobA.Text.ToArray & "'", cn)
            Dim datos As OdbcDataReader
            Try
                datos = consultarjob.ExecuteReader
                If datos.Read Then
                    ClienJob.Text() = datos.Item("Cliente").ToString()
                    ContJob.Text() = datos.Item("Contactos").ToString()
                    DescJob.Text() = datos.Item("Descripcion").ToString()
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Public Sub Llenar20()
        Dim daP2 = New OdbcDataAdapter("SELECT idUsuarios, nombre FROM gestióndeproyectossicma.usuarios WHERE Tipo_Usuario='Ventas'", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        NombreS.DataSource = dtP2
        NombreS.DisplayMember = "nombre"
        NombreS.ValueMember = "idUsuarios"
    End Sub

    Public Sub Llenar21()
        Dim da2 = New OdbcDataAdapter("SELECT idClientes, Contacto FROM gestióndeproyectossicma.clientes WHERE Cliente='" & Empresa.Text.ToString & "'", cn)
        Dim dt2 = New DataTable
        da2.Fill(dt2)
        Contacto.DataSource = dt2
        Contacto.DisplayMember = "Contacto"
        Contacto.ValueMember = "idClientes"
    End Sub

    Public Sub Llenar22()
        Dim da = New OdbcDataAdapter("SELECT DISTINCT Cliente FROM gestióndeproyectossicma.clientes ORDER BY Cliente ASC", cn)
        Dim dt = New DataTable
        da.Fill(dt)
        Empresa.DataSource = dt
        Empresa.DisplayMember = "Cliente"
    End Sub

    Public Sub Llenar23()
        Dim daP2 = New OdbcDataAdapter("SELECT Job FROM gestióndeproyectossicma.seguimiento ORDER BY Job ASC", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        Job.DataSource = dtP2
        Job.DisplayMember = "Job"
    End Sub

    Public Sub Llenar24()
        Dim daP2 = New OdbcDataAdapter("SELECT Cod_Visita FROM gestióndeproyectossicma.visitas ORDER BY id_visitas ASC", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        CodigoVisita.DataSource = dtP2
        CodigoVisita.DisplayMember = "Cod_Visita"
    End Sub

    Public Sub Llenar25()
        Dim daP2 = New OdbcDataAdapter("SELECT idactividades FROM gestióndeproyectossicma.actividades ORDER BY idactividades ASC", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        CodigoAct.DataSource = dtP2
        CodigoAct.DisplayMember = "idactividades"
    End Sub


    Public Sub Llenar33()
        Dim da = New OdbcDataAdapter("SELECT etapa FROM gestióndeproyectossicma.etapascotizacion WHERE job='" & Job.Text.ToString & "'  ORDER BY etapa ASC", cn)
        Dim dt = New DataTable
        da.Fill(dt)
        Etapa.DataSource = dt
        Etapa.DisplayMember = "etapa"
    End Sub

    'Se obtiene un mensaje emergente al momento de cambiar la prioridad  En Proceso.'
    Private Sub Prioridad_Leave(sender As Object, e As EventArgs) Handles Prioridad.Leave
        Dim selectedItem As Object
        selectedItem = Prioridad.SelectedItem
        If (selectedItem = "1-En proceso") Then
            MessageBox.Show("Es importante que escriba el P.O., la Fecha de Recibo de P.O. y la Fecha Compromiso de Entrega.", "Importante", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

    'Solo permite escribir números en el campo de Job.'
    Private Sub Job_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub
    Private Sub Form2_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        e.Cancel = True
    End Sub

    'Se consulta los datos de la tabla de seguimiento dependiendo el número de Job.'
    Private Sub BSG_Click_1(sender As Object, e As EventArgs) Handles BSG.Click
        If Job.Text = "" Then
            MsgBox("El campo Job se encuentra vacío, es necesario que escriba el número de Job para poder realizar la busqueda.")
        Else
            Dim consultarjob As New OdbcCommand("SELECT FechaSolicitudCotizacion, TIMESTAMPDIFF(DAY, FechaSolicitudCotizacion , CURDATE()) AS DIA, Job, Nombre, Prioridad, etapa, descripcionetapa, responsable, PO, FechaReciboPO, Cliente, Contactos, Categoria, Descripcion, FechaCompromisoEntrega, Cotizacion, MontoCotizado, Moneda, Comentarios, lider FROM gestióndeproyectossicma.seguimiento where Job ='" & Job.Text.ToArray & "'", cn)
            Dim datos As OdbcDataReader
            Try
                datos = consultarjob.ExecuteReader
                If datos.Read Then
                    DateTimePicker1.Text() = datos.Item("FechaSolicitudCotizacion").ToString()
                    DiasTranscurridos.Text() = datos.Item("DIA").ToString()
                    Job.Text() = datos.Item("Job").ToString()
                    NombreS.Text() = datos.Item("Nombre").ToString()
                    Prioridad.Text() = datos.Item("Prioridad").ToString()
                    Etapa.Text() = datos.Item("etapa").ToString()
                    DescEtapa.Text() = datos.Item("descripcionetapa").ToString()
                    Responsable.Text() = datos.Item("responsable").ToString()
                    PO.Text() = datos.Item("PO").ToString()
                    FechaPO.Text() = datos.Item("FechaReciboPO").ToString()
                    Cliente.Text() = datos.Item("Cliente").ToString()
                    Contactos.Text() = datos.Item("Contactos").ToString()
                    Categoria.Text() = datos.Item("Categoria").ToString()
                    Descripcion.Text() = datos.Item("Descripcion").ToString()
                    DateTimePicker2.Text() = datos.Item("FechaCompromisoEntrega").ToString()
                    NumeroCotizacion.Text() = datos.Item("Cotizacion").ToString()
                    MontoCotizado.Text() = datos.Item("MontoCotizado").ToString()
                    Moneda.Text() = datos.Item("Moneda").ToString()
                    Comentarios.Text() = datos.Item("Comentarios").ToString()
                    lider.Text() = datos.Item("lider").ToString()

                    If Prioridad.Text = "2-Cotizando" And Categoria.Text = "Proyecto" Then
                        Label59.Visible = True
                        Label60.Visible = True
                        Label134.Visible = True
                        Etapa.Visible = True
                        DescEtapa.Visible = True
                        Responsable.Visible = True
                        Button47.Visible = True
                    Else
                        Label59.Visible = False
                        Label60.Visible = False
                        Label134.Visible = False
                        Etapa.Visible = False
                        DescEtapa.Visible = False
                        Responsable.Visible = False
                        Button47.Visible = False
                        Etapa.Text = ""
                        DescEtapa.Text = ""
                        Responsable.Text = ""
                    End If
                Else
                    MessageBox.Show("Este número de Job no se encuentra Registrado.")
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
        Try
            Dim modificarjob As New OdbcCommand("UPDATE gestióndeproyectossicma.seguimiento SET DiasTranscurridos='" & DiasTranscurridos.Text.ToArray & "' WHERE Job= '" & Job.Text.ToString & "'", cn)
            modificarjob.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Permite limpiar los campos del formulario de seguimiento.'
    Private Sub LSG_Click(sender As Object, e As EventArgs) Handles LSG.Click
        NombreS.Text = MensajeVentas.Text
        DateTimePicker1.Value = DateTime.Now
        DateTimePicker2.Value = DateTime.Now
        Prioridad.Text = "2-Cotizando"
        Cliente.Text = ""
        Contactos.Text = ""
        Categoria.Text = ""
        Moneda.Text = "N/A"
        DiasTranscurridos.Text = "0"
        Llenar0()
        PO.Text = "N/A"
        FechaPO.Text = "N/A"
        Descripcion.Text = ""
        NumeroCotizacion.Text = "N/A"
        MontoCotizado.Text = "N/A"
        Comentarios.Text = ""
        Label59.Visible = False
        Label60.Visible = False
        Label134.Visible = False
        Etapa.Visible = False
        DescEtapa.Visible = False
        Responsable.Visible = False
        Button47.Visible = False
        Etapa.Text = ""
        DescEtapa.Text = ""
        Responsable.Text = ""
    End Sub

    'Se guardan los datos que se ingresaron en el formulario de Seguimiento.'
    Private Sub GSG_Click(sender As Object, e As EventArgs) Handles GSG.Click
        If Job.Text = "" And Cliente.Text = "" And Contactos.Text = "" And Descripcion.Text = "" Then
            MsgBox("Los campos se encuentran vacíos.")
        Else
            If Job.Text = "" Then
                MsgBox("El campo Job se encuentra vacío.")
            Else
                If Cliente.Text = "" Then
                    MsgBox("El campo Cliente se encuentra vacío.")
                Else
                    If Contactos.Text = "" Then
                        MsgBox("El campo Contacto se encuentra vacío.")
                    Else
                        If Categoria.Text = "" Then
                            MsgBox("El campo Categoria se encuentra vacío.")
                        Else
                            If Descripcion.Text = "" Then
                                MsgBox("El campo Descripcion se encuentra vacío.")
                            Else
                                Dim buscarJob As New OdbcCommand("SELECT Job FROM gestióndeproyectossicma.seguimiento WHERE Job ='" & Job.Text.ToArray & "'", cn)
                                Dim datos As OdbcDataReader
                                Try
                                    datos = buscarJob.ExecuteReader
                                    If datos.Read Then
                                        MessageBox.Show("Este Job ya se encuentra Registrado")
                                    Else
                                        Try
                                            MessageBox.Show("Los datos del Job se ha guardado Satisfactoriamente.")
                                            Dim guardarJob As New OdbcCommand("INSERT INTO  gestióndeproyectossicma.seguimiento (FechaSolicitudCotizacion, DiasTranscurridos, Job, Nombre, Prioridad, PO,  FechaReciboPO, Cliente, Contactos, Categoria, Descripcion, FechaCompromisoEntrega, Cotizacion, MontoCotizado, Moneda, Comentarios, lider) VALUES ('" & DateTimePicker1.Text & "','" & DiasTranscurridos.Text & "','" & Job.Text & "', '" & NombreS.Text & "' ,'" & Prioridad.Text & "', '" & PO.Text & "' ,'" & FechaPO.Text & "' ,'" & Cliente.Text & "','" & Contactos.Text & "','" & Categoria.Text & "','" & Descripcion.Text & "','" & DateTimePicker2.Text & "', '" & NumeroCotizacion.Text & "', '" & MontoCotizado.Text & "', '" & Moneda.Text & "', '" & Comentarios.Text & "', '" & lider.Text & "')", cn)
                                            guardarJob.ExecuteNonQuery()
                                        Catch ex As Exception
                                            MsgBox(ex.Message)
                                        End Try
                                    End If
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                                NombreS.Text = MensajeVentas.Text
                                DateTimePicker1.Value = DateTime.Now
                                DateTimePicker2.Value = DateTime.Now
                                Prioridad.Text = "2-Cotizando"
                                Cliente.Text = ""
                                Contactos.Text = ""
                                Categoria.Text = ""
                                Moneda.Text = "N/A"
                                DiasTranscurridos.Text = "0"
                                Llenar0()
                                PO.Text = "N/A"
                                FechaPO.Text = "N/A"
                                Descripcion.Text = ""
                                NumeroCotizacion.Text = "N/A"
                                MontoCotizado.Text = "0"
                                MontoCotizado.Text = FormatCurrency(MontoCotizado.Text, 2)
                                Comentarios.Text = ""
                                If Prioridad.Text = "2-Cotizando" And Categoria.Text = "Proyecto" Then
                                    Label59.Visible = True
                                    Label60.Visible = True
                                    Label134.Visible = True
                                    Etapa.Visible = True
                                    DescEtapa.Visible = True
                                    Responsable.Visible = True
                                    Button47.Visible = True
                                Else
                                    Label59.Visible = False
                                    Label60.Visible = False
                                    Label134.Visible = False
                                    Etapa.Visible = False
                                    DescEtapa.Visible = False
                                    Responsable.Visible = False
                                    Button47.Visible = False
                                    Etapa.Text = ""
                                    DescEtapa.Text = ""
                                    Responsable.Text = ""
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    'Se realiza la Modificación de los datos de seguimiento dependiendo el número de Job.'
    Private Sub MSG_Click(sender As Object, e As EventArgs) Handles MSG.Click
        Dim Msg5 As MsgBoxResult
        Msg5 = MsgBox("¿Esta seguro que desea Actualizar este Job?", vbYesNo, "Actualizar!!")
        If Msg5 = MsgBoxResult.Yes Then
            If Job.Text = "" And Cliente.Text = "" And Contactos.Text = "" Then
                MsgBox("Los campos se encuentran vacíos.")
            Else
                If Job.Text = "" Then
                    MsgBox("El campo Job se encuentra vacío.")
                Else
                    If Cliente.Text = "" Then
                        MsgBox("El campo Cliente se encuentra vacío.")
                    Else
                        If Contactos.Text = "" Then
                            MsgBox("El campo Contactos se encuentra vacío.")
                        Else
                            Dim consultamodificar As New OdbcCommand("SELECT Job FROM gestióndeproyectossicma.seguimiento WHERE Job ='" & Job.Text.ToArray & "'", cn)
                            Dim datos As OdbcDataReader
                            Try
                                datos = consultamodificar.ExecuteReader
                                If datos.Read Then
                                    MessageBox.Show("Los datos de la Job se han Modificado Correctamente.")
                                    Try
                                        Dim modificarjob As New OdbcCommand("UPDATE gestióndeproyectossicma.seguimiento SET FechaSolicitudCotizacion='" & DateTimePicker1.Text.ToArray & "', " & " DiasTranscurridos='" & DiasTranscurridos.Text.ToArray & "', " & " Job='" & Job.Text.ToArray & "',  " & " Nombre='" & NombreS.Text.ToArray & "', " & " Prioridad='" & Prioridad.Text.ToArray & "', " & " etapa='" & Etapa.Text.ToArray & "', " & " descripcionetapa='" & DescEtapa.Text.ToArray & "', " & " responsable='" & Responsable.Text.ToArray & "', " & " PO='" & PO.Text.ToArray & "', " & " FechaReciboPO='" & FechaPO.Text.ToArray & "', " & " Cliente='" & Cliente.Text.ToArray & "', " & " Contactos='" & Contactos.Text.ToArray & "', " & " Categoria='" & Categoria.Text.ToArray & "', " & " Descripcion='" & Descripcion.Text.ToArray & "', " & " FechaCompromisoEntrega='" & DateTimePicker2.Text.ToArray & "', " & " Cotizacion='" & NumeroCotizacion.Text.ToArray & "', " & " MontoCotizado='" & MontoCotizado.Text.ToArray & "', " & " Moneda='" & Moneda.Text.ToArray & "', " & " Comentarios='" & Comentarios.Text.ToArray & "', " & " lider='" & lider.Text.ToArray & "' WHERE Job ='" & Job.Text.ToArray & "'", cn)
                                        modificarjob.ExecuteNonQuery()
                                    Catch ex As Exception
                                        MsgBox(ex.Message)
                                    End Try
                                Else
                                    MessageBox.Show("Esta Job no se encuentra Registrado.")
                                End If
                            Catch ex As Exception
                                MsgBox(ex.Message)
                            End Try
                            NombreS.Text = MensajeVentas.Text
                            DateTimePicker1.Value = DateTime.Now
                            DateTimePicker2.Value = DateTime.Now
                            Prioridad.Text = "2-Cotizando"
                            Cliente.Text = ""
                            Contactos.Text = ""
                            Categoria.Text = ""
                            Moneda.Text = "N/A"
                            DiasTranscurridos.Text = "0"
                            Llenar0()
                            PO.Text = "N/A"
                            FechaPO.Text = "N/A"
                            Descripcion.Text = ""
                            NumeroCotizacion.Text = "N/A"
                            MontoCotizado.Text = "0"
                            MontoCotizado.Text = FormatCurrency(MontoCotizado.Text, 2)
                            Comentarios.Text = ""
                            If Prioridad.Text = "2-Cotizando" And Categoria.Text = "Proyecto" Then
                                Label59.Visible = True
                                Label60.Visible = True
                                Label134.Visible = True
                                Etapa.Visible = True
                                DescEtapa.Visible = True
                                Responsable.Visible = True
                                Button47.Visible = True
                            Else
                                Label59.Visible = False
                                Label60.Visible = False
                                Label134.Visible = False
                                Etapa.Visible = False
                                DescEtapa.Visible = False
                                Responsable.Visible = False
                                Button47.Visible = False
                                Etapa.Text = ""
                                DescEtapa.Text = ""
                                Responsable.Text = ""
                            End If
                        End If
                    End If
                End If
            End If
        Else
            MessageBox.Show("Se canceló la Actualización.")
        End If
    End Sub

    Private Sub Cliente_TextChanged(sender As Object, e As EventArgs) Handles Cliente.TextChanged
        Llenar2()
    End Sub

    'Se elimina los datos de seguimiento dependiendo el número de Job.'
    Private Sub ESG_Click(sender As Object, e As EventArgs) Handles ESG.Click
        Dim Msg As MsgBoxResult
        Msg = MsgBox("¿Esta seguro que desea Eliminar este Job?", vbYesNo, "Eliminar!!")
        If Msg = MsgBoxResult.Yes Then
            If Job.Text = "" Then
                MsgBox("El campo Job se encuentran vacío.")
            Else
                Dim consultarempresa As New OdbcCommand("SELECT * FROM gestióndeproyectossicma.seguimiento WHERE Job ='" & Job.Text.ToArray & "'", cn)
                Dim datos As OdbcDataReader
                Try
                    datos = consultarempresa.ExecuteReader
                    If datos.Read Then
                        MessageBox.Show("Este Job se ha Eliminado Correctamente.")
                        Dim eliminarempresa As New OdbcCommand("DELETE FROM gestióndeproyectossicma.seguimiento WHERE Job='" & Job.Text.ToArray & "'", cn)
                        eliminarempresa.ExecuteNonQuery()
                    Else
                        MessageBox.Show("Este Job no se encuentra Registrado.")
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                NombreS.Text = MensajeVentas.Text
                DateTimePicker1.Value = DateTime.Now
                DateTimePicker2.Value = DateTime.Now
                Prioridad.Text = "2-Cotizando"
                Cliente.Text = ""
                Contactos.Text = ""
                Categoria.Text = ""
                Moneda.Text = "N/A"
                DiasTranscurridos.Text = "0"
                Llenar0()
                PO.Text = "N/A"
                FechaPO.Text = "N/A"
                Descripcion.Text = ""
                NumeroCotizacion.Text = "N/A"
                MontoCotizado.Text = "0"
                MontoCotizado.Text = FormatCurrency(MontoCotizado.Text, 2)
                Comentarios.Text = ""
                If Prioridad.Text = "2-Cotizando" And Categoria.Text = "Proyecto" Then
                    Label59.Visible = True
                    Label60.Visible = True
                    Label134.Visible = True
                    Etapa.Visible = True
                    DescEtapa.Visible = True
                    Responsable.Visible = True
                    Button47.Visible = True
                Else
                    Label59.Visible = False
                    Label60.Visible = False
                    Label134.Visible = False
                    Etapa.Visible = False
                    DescEtapa.Visible = False
                    Responsable.Visible = False
                    Button47.Visible = False
                    Etapa.Text = ""
                    DescEtapa.Text = ""
                    Responsable.Text = ""
                End If
            End If
        Else
            MessageBox.Show("Se canceló la Eliminación.")
        End If
    End Sub

    'Se realiza la busqueda del cliente dependiendo el nombre de la empresa y el nombre del contacto.'
    Private Sub BC_Click(sender As Object, e As EventArgs) Handles BC.Click
        If Empresa.Text = "" And Contacto.Text = "" Then
            MsgBox("Los campos Empresa y Contacto se encuentran vacíos.")
        Else
            If Empresa.Text = "" Then
                MsgBox("El campo Empresa se encuentra vacío, es necesario que escriba el nombre de la empresa para poder realizar la busqueda.")
            Else
                If Contacto.Text = "" Then
                    MsgBox("El campo Contacto se encuentra vacío, es necesario que escriba el nombre del Contacto para poder realizar la busqueda.")
                Else
                    Dim consultarempresa As New OdbcCommand("SELECT Cliente, Contacto, Departamento, CorreoElectronico, TelOfi, TelCel FROM gestióndeproyectossicma.clientes WHERE Cliente ='" & Empresa.Text.ToArray & "'" & "AND Contacto='" & Contacto.Text.ToArray & "'", cn)
                    Dim datos As OdbcDataReader
                    Try
                        datos = consultarempresa.ExecuteReader
                        If datos.Read Then
                            Empresa.Text() = datos.Item("Cliente").ToString()
                            Contacto.Text() = datos.Item("Contacto").ToString()
                            Departamento.Text() = datos.Item("Departamento").ToString()
                            CorreoElectronico.Text() = datos.Item("CorreoElectronico").ToString()
                            TelOfi.Text() = datos.Item("TelOfi").ToString()
                            TelCel.Text() = datos.Item("TelCel").ToString()
                        Else
                            Empresa.Text = ""
                            Contacto.Text = ""
                            Departamento.Text = ""
                            CorreoElectronico.Text = ""
                            TelOfi.Text = ""
                            TelCel.Text = ""
                            MessageBox.Show("Esta Empresa no se encuentra Registrado.")
                        End If
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                End If
            End If
        End If
    End Sub

    'Se guardan los datos del cliente.'
    Private Sub GC_Click(sender As Object, e As EventArgs) Handles GC.Click
        If Empresa.Text = "" And Contacto.Text = "" And Departamento.Text = "" And CorreoElectronico.Text = "" And TelOfi.Text = "" And TelCel.Text = "" Then
            MsgBox("Los campos se encuentran vacíos.")
        Else
            If Empresa.Text = "" Then
                MsgBox("El campo Empresa se encuentra vacío.")
            Else
                If Contacto.Text = "" Then
                    MsgBox("El campo Contacto se encuentra vacío.")
                Else
                    If Departamento.Text = "" Then
                        MsgBox("El campo Departamento se encuentra vacío.")
                    Else
                        If TelOfi.Text = "" Then
                            MsgBox("El campo Departamento se encuentra vacío.")
                        Else
                            If TelCel.Text = "" Then
                                MsgBox("El campo Departamento se encuentra vacío.")
                            Else
                                If CorreoElectronico.Text = "" Then
                                    MsgBox("El campo Departamento se encuentra vacío.")
                                Else
                                    Dim buscarempresa As New OdbcCommand("SELECT Cliente, Contacto FROM gestióndeproyectossicma.clientes WHERE Cliente ='" & Empresa.Text.ToArray & "'" & "AND Contacto= '" & Contacto.Text & "'", cn)
                                    Dim datos As OdbcDataReader
                                    Try
                                        datos = buscarempresa.ExecuteReader
                                        If datos.Read Then
                                            MessageBox.Show("Esta Empresa ya se encuentra Registrado")
                                        Else
                                            Try
                                                MessageBox.Show("Los datos de la Empresa se ha guardado Satisfactoriamente.")
                                                Dim guardarempresa As New OdbcCommand("INSERT INTO gestióndeproyectossicma.clientes (Cliente, Contacto, Departamento, CorreoElectronico, TelOfi, TelCel) VALUES ('" & Empresa.Text & "','" & Contacto.Text & "','" & Departamento.Text & "','" & CorreoElectronico.Text & "','" & TelOfi.Text & "','" & TelCel.Text & "')", cn)
                                                guardarempresa.ExecuteNonQuery()
                                            Catch ex As Exception
                                                MsgBox(ex.Message)
                                            End Try
                                        End If
                                    Catch ex As Exception
                                        MsgBox(ex.Message)
                                    End Try
                                    Empresa.Text = ""
                                    Contacto.Text = ""
                                    Departamento.Text = ""
                                    CorreoElectronico.Text = ""
                                    TelOfi.Text = ""
                                    TelCel.Text = ""
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
        Llenar22()
    End Sub

    'Se modifican los datos del cliente, dependiendo el nombre de la empresa y el de contacto.'
    Private Sub MC_Click(sender As Object, e As EventArgs) Handles MC.Click
        Dim Msg6 As MsgBoxResult
        Msg6 = MsgBox("¿Esta seguro que desea Actualizar este Cliente?", vbYesNo, "Actualizar!!")
        If Msg6 = MsgBoxResult.Yes Then
            If Empresa.Text = "" And Contacto.Text = "" And Departamento.Text = "" And CorreoElectronico.Text = "" And TelOfi.Text = "" And TelCel.Text = "" Then
                MsgBox("Los campos se encuentran vacíos.")
            Else
                If Empresa.Text = "" Then
                    MsgBox("El campo Empresa se encuentra vacío.")
                Else
                    If Contacto.Text = "" Then
                        MsgBox("El campo Contacto se encuentra vacío.")
                    Else
                        If Departamento.Text = "" Then
                            MsgBox("El campo Departamento se encuentra vacío.")
                        Else
                            If CorreoElectronico.Text = "" Then
                                MsgBox("El campo Correo Electrónico se encuentra vacío.")
                            Else
                                If TelOfi.Text = "" Then
                                    MsgBox("El campo Teléfono de Oficina se encuentra vacío.")
                                Else
                                    If TelCel.Text = "" Then
                                        MsgBox("El campo Teléfono Celular se encuentra vacío.")
                                    Else
                                        Dim consultarempresa As New OdbcCommand("SELECT Cliente FROM gestióndeproyectossicma.clientes WHERE Cliente ='" & Empresa.Text.ToArray & "'" & "AND Contacto= '" & Contacto.Text & "'", cn)
                                        Dim datos As OdbcDataReader
                                        Try
                                            datos = consultarempresa.ExecuteReader
                                            If datos.Read Then
                                                MessageBox.Show("Los datos de la empresa se han Modificado Correctamente.")
                                                Try
                                                    Dim modificarempresa As New OdbcCommand("UPDATE gestióndeproyectossicma.clientes SET Cliente='" & Empresa.Text.ToArray & "', " & " Contacto='" & Contacto.Text.ToArray & "', " & " Departamento='" & Departamento.Text.ToArray & "', " & " CorreoElectronico='" & CorreoElectronico.Text.ToArray & "', " & " TelOfi='" & TelOfi.Text.ToArray & "', " & " TelCel='" & TelCel.Text.ToArray & "' WHERE Cliente ='" & Empresa.Text.ToArray & "'" & "AND Contacto= '" & Contacto.Text & "'", cn)
                                                    modificarempresa.ExecuteNonQuery()
                                                Catch ex As Exception
                                                    MsgBox(ex.Message)
                                                End Try
                                            Else
                                                MessageBox.Show("Esta Empresa no se encuentra Registrado.")
                                            End If
                                        Catch ex As Exception
                                            MsgBox(ex.Message)
                                        End Try
                                        Empresa.Text = ""
                                        Contacto.Text = ""
                                        Departamento.Text = ""
                                        CorreoElectronico.Text = ""
                                        TelOfi.Text = ""
                                        TelCel.Text = ""
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            Llenar22()
        Else
            MessageBox.Show("Se canceló la Actualización.")
        End If
    End Sub

    'Se eliminan los datos del cliente dependiendo el nombre de la empresa y el de contacto.'
    Private Sub EC_Click(sender As Object, e As EventArgs) Handles EC.Click
        Dim Msg2 As MsgBoxResult
        Msg2 = MsgBox("¿Esta seguro que desea Eliminar este Cliente?", vbYesNo, "Eliminar!!")
        If Msg2 = MsgBoxResult.Yes Then
            If Empresa.Text = "" And Contacto.Text = "" Then
                MsgBox("Los campos Empresa y Contacto se encuentran vacíos, es necesario que escriba el nombre de la Empresa y nombre del contacto que desea Elminar.")
            Else
                If Empresa.Text = "" Then
                    MsgBox("El campo Empresa se encuentra vacío, es necesario que escriba el nombre de la Empresa que desea Elminar.")
                Else
                    If Contacto.Text = "" Then
                        MsgBox("El campo Contacto se encuentra vacío, es necesario que escriba el nombre del Contacto que desea Elminar.")
                    Else
                        Dim consultarempresa As New OdbcCommand("SELECT Cliente, Contacto FROM gestióndeproyectossicma.clientes WHERE Cliente ='" & Empresa.Text.ToArray & "'" & "AND Contacto='" & Contacto.Text.ToArray & "'", cn)
                        Dim datos As OdbcDataReader
                        Try
                            datos = consultarempresa.ExecuteReader
                            If datos.Read Then
                                MessageBox.Show("Esta Empresa se ha Eliminado Correctamente.")
                                Dim eliminarempresa As New OdbcCommand("DELETE FROM gestióndeproyectossicma.clientes WHERE Cliente='" & Empresa.Text.ToArray & "'" & "AND Contacto='" & Contacto.Text.ToArray & "'", cn)
                                eliminarempresa.ExecuteNonQuery()
                            Else
                                MessageBox.Show("Esta Empresa no se encuentra Registrado.")
                            End If
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                        Empresa.Text = ""
                        Contacto.Text = ""
                        Departamento.Text = ""
                        CorreoElectronico.Text = ""
                        TelOfi.Text = ""
                        TelCel.Text = ""
                    End If
                End If
            End If
            Llenar22()
        Else
            MessageBox.Show("Se canceló la Eliminación.")
        End If
    End Sub

    'Se limpia los campos del formulario de clientes.'
    Private Sub LClient_Click(sender As Object, e As EventArgs) Handles LClient.Click
        Empresa.Text = ""
        Contacto.Text = ""
        Departamento.Text = ""
        CorreoElectronico.Text = ""
        TelOfi.Text = ""
        TelCel.Text = ""
    End Sub

    Private Sub ClienteVisita_TextChanged(sender As Object, e As EventArgs) Handles ClienteVisita.TextChanged
        Llenar4()
    End Sub

    'Se realiza la busqueda de las visitas dependiendo el Código de Visita.'
    Private Sub BV_Click(sender As Object, e As EventArgs) Handles BV.Click
        If CodigoVisita.Text = "" Then
            MsgBox("El campo Código se encuentra vacío.")
        Else
            Dim consultarvisita As New OdbcCommand("SELECT Cod_Visita, fechavisita, fechaproxvisita, vendedor, cliente, contacto, descripcion FROM gestióndeproyectossicma.visitas WHERE Cod_Visita ='" & CodigoVisita.Text.ToArray & "' AND vendedor='" & NombreVendedor.Text.ToArray & "'", cn)
            Dim datosvi As OdbcDataReader
            Try
                datosvi = consultarvisita.ExecuteReader
                If datosvi.Read Then
                    CodigoVisita.Text() = datosvi.Item("Cod_Visita").ToString()
                    DateTimePicker4.Text() = datosvi.Item("fechavisita").ToString()
                    DateTimePicker5.Text() = datosvi.Item("fechaproxvisita").ToString()
                    NombreVendedor.Text() = datosvi.Item("vendedor").ToString()
                    ClienteVisita.Text() = datosvi.Item("cliente").ToString()
                    ContactosVisita.Text() = datosvi.Item("contacto").ToString()
                    DescVisita.Text() = datosvi.Item("descripcion").ToString()
                Else
                    Llenar3()
                    DateTimePicker4.Value = DateTime.Now
                    DateTimePicker5.Value = DateTime.Now
                    CodigoVisitaX()
                    DescVisita.Text = ""
                    MessageBox.Show("Esta Visita no se encuentra Registrada.")
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    'Se limpian los campos del formulario de visitas.'
    Private Sub LV_Click(sender As Object, e As EventArgs) Handles LV.Click
        Llenar3()
        DateTimePicker4.Value = DateTime.Now
        DateTimePicker5.Value = DateTime.Now
        CodigoVisitaX()
        DescVisita.Text = ""
    End Sub

    'Se guardan los datos de una visita.'
    Private Sub GV_Click(sender As Object, e As EventArgs) Handles GV.Click
        If CodigoVisita.Text = "" And DescVisita.Text = "" Then
            MsgBox("Los campos se encuentran vacíos.")
        Else
            If CodigoVisita.Text = "" Then
                MsgBox("El campo Código se encuentra vacío.")
            Else
                If DescVisita.Text = "" Then
                    MsgBox("El campo Descripción se encuentra vacío.")
                Else
                    Dim buscarvisita As New OdbcCommand("SELECT Cod_Visita, vendedor FROM gestióndeproyectossicma.visitas WHERE Cod_Visita ='" & CodigoVisita.Text.ToArray & "'" & "AND vendedor= '" & NombreVendedor.Text & "'", cn)
                    Dim datosvis As OdbcDataReader
                    Try
                        datosvis = buscarvisita.ExecuteReader
                        If datosvis.Read Then
                            MessageBox.Show("Esta Visita ya se encuentra Registrado")
                        Else
                            Try
                                MessageBox.Show("Los datos de la Visita se han guardado Satisfactoriamente.")
                                Dim guardarvisita As New OdbcCommand("INSERT INTO  gestióndeproyectossicma.visitas(Cod_Visita, fechavisita, fechaproxvisita, vendedor, cliente, contacto, descripcion) VALUES ('" & CodigoVisita.Text & "','" & DateTimePicker4.Text & "','" & DateTimePicker5.Text & "','" & NombreVendedor.Text & "','" & ClienteVisita.Text & "','" & ContactosVisita.Text & "','" & DescVisita.Text & "')", cn)
                                guardarvisita.ExecuteNonQuery()
                            Catch ex As Exception
                                MsgBox(ex.Message)
                            End Try
                        End If
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                    Llenar3()
                    DateTimePicker4.Value = DateTime.Now
                    DateTimePicker5.Value = DateTime.Now
                    CodigoVisitaX()
                    DescVisita.Text = ""
                End If
            End If
        End If
    End Sub

    'Se modifican los datos de una visita dependiendo el Código de visita.'
    Private Sub MV_Click(sender As Object, e As EventArgs) Handles MV.Click
        Dim Msg7 As MsgBoxResult
        Msg7 = MsgBox("¿Esta seguro que desea Actualizar esta Visita?", vbYesNo, "Actualizar!!")
        If Msg7 = MsgBoxResult.Yes Then
            If CodigoVisita.Text = "" Then
                MsgBox("El campo Código se encuentra vacío.")
            Else
                Dim consultarVisita As New OdbcCommand("SELECT Cod_Visita, vendedor FROM gestióndeproyectossicma.visitas WHERE Cod_Visita ='" & CodigoVisita.Text.ToArray & "' AND vendedor='" & NombreVendedor.Text.ToArray & "'", cn)
                Dim datosVX As OdbcDataReader
                Try
                    datosVX = consultarVisita.ExecuteReader
                    If datosVX.Read Then
                        MessageBox.Show("Esta Visita se ha Modificado Correctamente.")
                        Try
                            Dim modificarVX As New OdbcCommand("UPDATE gestióndeproyectossicma.visitas SET Cod_Visita='" & CodigoVisita.Text.ToArray & "', " & " fechavisita='" & DateTimePicker4.Text.ToArray & "', " & " fechaproxvisita='" & DateTimePicker5.Text.ToArray & "', " & "vendedor='" & NombreVendedor.Text.ToArray & "', " & " cliente= '" & ClienteVisita.Text.ToArray & "', " & " contacto= '" & ContactosVisita.Text.ToArray & "', " & " descripcion= '" & DescVisita.Text.ToArray & "' where Cod_Visita ='" & CodigoVisita.Text.ToArray & "'", cn)
                            modificarVX.ExecuteNonQuery()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Else
                        MessageBox.Show("Esta Visita no se encuentra Registrada.")
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                Llenar3()
                DateTimePicker4.Value = DateTime.Now
                DateTimePicker5.Value = DateTime.Now
                CodigoVisitaX()
                DescVisita.Text = ""
            End If
        Else
            MessageBox.Show("Se canceló la Actualización.")
        End If
    End Sub

    'Se eliminan los datos de una visita dependiendo el Código de Visita.'
    Private Sub EV_Click(sender As Object, e As EventArgs) Handles EV.Click
        Dim Msg3 As MsgBoxResult
        Msg3 = MsgBox("¿Esta seguro que desea Eliminar esta Visita?", vbYesNo, "Eliminar!!")
        If Msg3 = MsgBoxResult.Yes Then
            If CodigoVisita.Text = "" Then
                MsgBox("El campo Código se encuentra vacío.")
            Else
                Dim consultarvisitaxxxx As New OdbcCommand("SELECT Cod_Visita, vendedor FROM gestióndeproyectossicma.visitas WHERE Cod_Visita='" & CodigoVisita.Text.ToArray & "' AND vendedor='" & NombreVendedor.Text.ToArray & "'", cn)
                Dim datosxxxx As OdbcDataReader
                Try
                    datosxxxx = consultarvisitaxxxx.ExecuteReader
                    If datosxxxx.Read Then
                        MessageBox.Show("Esta Visita se ha Eliminado Correctamente.")
                        Dim eliminarempresa As New OdbcCommand("DELETE FROM gestióndeproyectossicma.visitas WHERE Cod_Visita='" & CodigoVisita.Text.ToArray & "' AND vendedor='" & NombreVendedor.Text.ToArray & "'", cn)
                        eliminarempresa.ExecuteNonQuery()
                    Else
                        MessageBox.Show("Esta Empresa no se encuentra Registrado.")
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                Llenar3()
                DateTimePicker4.Value = DateTime.Now
                DateTimePicker5.Value = DateTime.Now
                CodigoVisitaX()
                DescVisita.Text = ""
            End If
        Else
            MessageBox.Show("Se canceló la Eliminación.")
        End If
    End Sub

    'Se limpian los campos del formulario de Visitas.'
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Llenar5()
        FechaA.Value = DateTime.Now
        FechaM.Value = DateTime.Now
        CodigoActividad()
        Llenar7()
        Titulo.Text = ""
        ComentarioA.Text = ""
        NotaA.Text = ""
        DescripcionA.Text = ""
        Estatus.Text = "Sin Comenzar"
        Estado.Text = "Abierta"
    End Sub

    'Solo permite escribir números en el campo de Job.'
    Private Sub JobA_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub
    'Solo permite escribir números en el campo de Teléfono Celular.'
    Private Sub TelCel_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TelCel.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    'Se realiza la busqueda de los datos de una actividad dependiendo el código de actividad.'
    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If CodigoAct.Text = "" Then
            MsgBox("El campo Código se encuentra vacío.")
        Else
            Dim consultaractividad As New OdbcCommand("SELECT Cod_act, Job, Titulo, Comentario, Nota, Fecha, Nombre, Descripcion, Fecha_Meta, Status, Estado FROM gestióndeproyectossicma.actividades WHERE Cod_act='" & CodigoAct.Text.ToArray & "'", cn)
            Dim datosactividad As OdbcDataReader
            Try
                datosactividad = consultaractividad.ExecuteReader
                If datosactividad.Read Then
                    CodigoAct.Text() = datosactividad.Item("Cod_act").ToString()
                    JobA.Text() = datosactividad.Item("Job").ToString()
                    Titulo.Text() = datosactividad.Item("Titulo").ToString()
                    ComentarioA.Text() = datosactividad.Item("Comentario").ToString()
                    NotaA.Text() = datosactividad.Item("Nota").ToString()
                    FechaM.Text() = datosactividad.Item("Fecha_Meta").ToString()
                    Estatus.Text() = datosactividad.Item("Status").ToString()
                    Estado.Text() = datosactividad.Item("Estado").ToString()
                    FechaA.Text() = datosactividad.Item("Fecha").ToString()
                    NombreP.Text() = datosactividad.Item("Nombre").ToString()
                    DescripcionA.Text() = datosactividad.Item("Descripcion").ToString()
                Else
                    ComentarioA.Text = ""
                    NotaA.Text = ""
                    Llenar5()
                    FechaA.Value = DateTime.Now
                    FechaM.Value = DateTime.Now
                    CodigoActividad()
                    Llenar7()
                    Titulo.Text = ""
                    DescripcionA.Text = ""
                    Estatus.Text = "Sin Comenzar"
                    Estado.Text = "Abierta"
                    MessageBox.Show("Esta Actividad no se encuentra Registrada.")
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    'Se guardan los datos de la Actividad.'
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If CodigoAct.Text = "" And JobA.Text = "" And Titulo.Text = "" And NombreP.Text = "" And DescripcionA.Text = "" Then
            MsgBox("Los campos se encuentran vacíos.")
        Else
            If CodigoAct.Text = "" Then
                MsgBox("El campo Código se encuentra vacío.")
            Else
                If JobA.Text = "" Then
                    MsgBox("El campo Job se encuentra vacío.")
                Else
                    If Titulo.Text = "" Then
                        MsgBox("El campo Título se encuentra vacío.")
                    Else
                        If NombreP.Text = "" Then
                            MsgBox("El campo Nombre se encuentra vacío.")
                        Else
                            If DescripcionA.Text = "" Then
                                MsgBox("El campo Descripción se encuentra vacío.")
                            Else
                                Dim buscaractividad2 As New OdbcCommand("SELECT Job FROM gestióndeproyectossicma.seguimiento WHERE Job ='" & JobA.Text.ToArray & "'", cn)
                                Dim datosactividad2 As OdbcDataReader
                                Try
                                    datosactividad2 = buscaractividad2.ExecuteReader
                                    If datosactividad2.Read Then
                                        Try
                                            MessageBox.Show("Los datos de la Actividad se han guardado Satisfactoriamente.")
                                            Dim guardaractividad As New OdbcCommand("INSERT INTO  gestióndeproyectossicma.actividades (Cod_act, Job, Titulo, Comentario, Nota, Fecha, Nombre, Descripcion, Fecha_Meta, Status, Estado) VALUES ('" & CodigoAct.Text & "','" & JobA.Text & "','" & Titulo.Text & "', '" & ComentarioA.Text & "', '" & NotaA.Text & "','" & FechaA.Text & "','" & NombreP.Text & "','" & DescripcionA.Text & "','" & FechaM.Text & "','" & Estatus.Text & "','" & Estado.Text & "')", cn)
                                            guardaractividad.ExecuteNonQuery()
                                        Catch ex As Exception
                                            MsgBox(ex.Message)
                                        End Try
                                    Else
                                        MessageBox.Show("Es necesario registrar el Job, antes de registrar una Actividad")
                                    End If
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                                FechaA.Value = DateTime.Now
                                FechaM.Value = DateTime.Now
                                CodigoActividad()
                                Titulo.Text = ""
                                ComentarioA.Text = ""
                                NotaA.Text = ""
                                DescripcionA.Text = ""
                                Estatus.Text = "Sin Comenzar"
                                Estado.Text = "Abierta"
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    'Se modifican los datos de las actividades dependiendo del código de actividad.'
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim Msg8 As MsgBoxResult
        Msg8 = MsgBox("¿Esta seguro que desea Actualizar esta Actividad?", vbYesNo, "Actualizar!!")
        If Msg8 = MsgBoxResult.Yes Then
            If CodigoAct.Text = "" Then
                MsgBox("Los campos se encuentran vacíos.")
            Else
                Dim consultaractividad3 As New OdbcCommand("SELECT Cod_act, Job, Nombre FROM gestióndeproyectossicma.actividades WHERE Cod_act ='" & CodigoAct.Text.ToArray & "'", cn)
                Dim datosactividad3 As OdbcDataReader
                Try
                    datosactividad3 = consultaractividad3.ExecuteReader
                    If datosactividad3.Read Then
                        MessageBox.Show("Esta Actividad se ha Modificado Correctamente.")
                        Try
                            Dim modificarVX As New OdbcCommand("UPDATE gestióndeproyectossicma.actividades SET Cod_act='" & CodigoAct.Text.ToArray & "', Job='" & JobA.Text.ToArray & "', " & " Titulo='" & Titulo.Text.ToArray & "', Comentario='" & ComentarioA.Text.ToArray & "', Nota='" & NotaA.Text.ToArray & "', " & " Fecha= '" & FechaA.Text.ToArray & "', " & " Nombre= '" & NombreP.Text.ToArray & "', " & " Descripcion= '" & DescripcionA.Text.ToArray & "' WHERE Cod_act ='" & CodigoAct.Text.ToArray & "' AND Job ='" & JobA.Text.ToArray & "'", cn)
                            modificarVX.ExecuteNonQuery()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Else
                        MessageBox.Show("Esta Actividad no se encuentra Registrada.")
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                FechaA.Value = DateTime.Now
                FechaM.Value = DateTime.Now
                CodigoActividad()
                Titulo.Text = ""
                ComentarioA.Text = ""
                NotaA.Text = ""
                DescripcionA.Text = ""
                Estatus.Text = "Sin Comenzar"
                Estado.Text = "Abierta"
            End If
        Else
            MessageBox.Show("Se canceló la Actualización.")
        End If
    End Sub

    'Se elimina la actividad dependiendo el código de actividad.'
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim Msg4 As MsgBoxResult
        Msg4 = MsgBox("¿Esta seguro que desea Eliminar esta Actividad?", vbYesNo, "Eliminar!!")
        If Msg4 = MsgBoxResult.Yes Then
            If CodigoAct.Text = "" And JobA.Text = "" Then
                MsgBox("Los campos se encuentran vacíos.")
            Else
                If CodigoAct.Text = "" Then
                    MsgBox("El campo Código se encuentra vacío.")
                Else
                    If JobA.Text = "" Then
                        MsgBox("El campo Job se encuentra vacío.")
                    Else
                        Dim consultaractividad4 As New OdbcCommand("SELECT Cod_act, Job, Nombre FROM gestióndeproyectossicma.actividades WHERE Cod_act= '" & CodigoAct.Text.ToArray & "' AND Job='" & JobA.Text.ToArray & "' AND Nombre='" & NombreP.Text.ToArray & "'", cn)
                        Dim datosactividad4 As OdbcDataReader
                        Try
                            datosactividad4 = consultaractividad4.ExecuteReader
                            If datosactividad4.Read Then
                                MessageBox.Show("Esta Actividad se ha Eliminado Correctamente.")
                                Dim eliminaractividad As New OdbcCommand("DELETE FROM gestióndeproyectossicma.actividades WHERE Cod_act= '" & CodigoAct.Text.ToArray & "' AND Job='" & JobA.Text.ToArray & "' AND Nombre='" & NombreP.Text.ToArray & "'", cn)
                                eliminaractividad.ExecuteNonQuery()
                            Else
                                MessageBox.Show("Esta Actividad no se encuentra Registrado.")
                            End If
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                        Llenar5()
                        FechaA.Value = DateTime.Now
                        FechaM.Value = DateTime.Now
                        CodigoActividad()
                        Llenar7()
                        Titulo.Text = ""
                        ComentarioA.Text = ""
                        NotaA.Text = ""
                        DescripcionA.Text = ""
                        Estatus.Text = "Sin Comenzar"
                        Estado.Text = "Abierta"
                    End If
                End If
            End If
        Else
            MessageBox.Show("Se canceló la Eliminación.")
        End If
    End Sub

    'Aquí se genera el reporte de visitas dependiendo una fecha de inicio y una fecha final.'
    Private Sub btnVisitas_Click(sender As Object, e As EventArgs) Handles btnVisitas.Click
        Dim buscarFV As New OdbcCommand("SELECT Cod_Visita, fechavisita, fechaproxvisita, vendedor, cliente, contacto, descripcion FROM gestióndeproyectossicma.visitas WHERE fechavisita >='" & Fecha1.Text.ToArray & "'" & "AND fechavisita <= '" & Fecha2.Text & "'", cn)
        Dim datosFV As OdbcDataReader
        Try
            datosFV = buscarFV.ExecuteReader
            If datosFV.Read Then
                Dim FechaInicial As Date = CDate(Fecha1.Text)
                Dim FechaFinal As Date = CDate(Fecha2.Text)
                Dim ReporteVisitas As New ReporteVisitas
                ReporteVisitas.FechaI = FechaInicial
                ReporteVisitas.FechaF = FechaFinal
                ReporteVisitas.ShowDialog()
            Else
                MessageBox.Show("Es necesario poner una fecha correcta, para poder Generar el Reporte")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Se genera un reporte de clientes dependiendo el nombre de la empresa.'
    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        If ClienteReporte.Text = "" Then
            MsgBox("El campo Cliente se encuentra vacío.")
        Else
            Dim buscarCR As New OdbcCommand("SELECT Cliente FROM gestióndeproyectossicma.clientes WHERE Cliente ='" & ClienteReporte.Text.ToArray & "'", cn)
            Dim datosCR As OdbcDataReader
            Try
                datosCR = buscarCR.ExecuteReader
                If datosCR.Read Then
                    Dim Clients As String = ClienteReporte.Text
                    Dim ReporteClientes As New ReporteClientes
                    ReporteClientes.NoClients = Clients
                    ReporteClientes.ShowDialog()
                Else
                    ClienteReporte.Text = ""
                    MessageBox.Show("Es necesario registrar el Cliente, antes de Generar el Reporte")
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Llenar6()
        Llenar11()
        Llenar12()
    End Sub
    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        VerSeguimiento.Show()
        VerSeguimiento.MensajeSeguimiento.Text = NombreS.Text
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        VerActividades.Show()
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        VerClientes.Show()
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        VerVisitas.Show()
        VerVisitas.MensajeVentas2.Text = NombreVendedor.Text
    End Sub

    'Se genera el reporte de actividades pendiendtes dependiendo el responsable.'
    Private Sub Button18_Click(sender As Object, e As EventArgs) Handles Button18.Click
        If NombreRR.Text = "" Then
            MsgBox("El campo Nombre se encuentra vacío.")
        Else
            Dim buscarAPR As New OdbcCommand("SELECT Nombre FROM gestióndeproyectossicma.actividades WHERE Nombre ='" & NombreRR.Text.ToArray & "'", cn)
            Dim datosAPR As OdbcDataReader
            Try
                datosAPR = buscarAPR.ExecuteReader
                If datosAPR.Read Then
                    Dim Nom As String = NombreRR.Text
                    Dim ReporteAR As New ReporteActividadesPendientesResponsable
                    ReporteAR.NRA = Nom
                    ReporteAR.ShowDialog()
                Else
                    NombreRR.Text = ""
                    MessageBox.Show("Es necesario registrar el Nombre, antes de Generar el Reporte")
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    'Se genera el reporte de actividades pendientes dependiendo el número de Job.'
    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        If JobRAJ.Text = "" Then
            MsgBox("El campo Job se encuentra vacío.")
        Else
            Dim buscarAPJ As New OdbcCommand("SELECT Job FROM gestióndeproyectossicma.actividades WHERE Job ='" & JobRAJ.Text.ToArray & "'", cn)
            Dim datosAPJ As OdbcDataReader
            Try
                datosAPJ = buscarAPJ.ExecuteReader
                If datosAPJ.Read Then
                    Dim Jobx As Integer = JobRAJ.Text
                    Dim ReporteAJ As New ReporteActividadesPendientesJob
                    ReporteAJ.NRAJ = Jobx
                    ReporteAJ.ShowDialog()
                Else
                    MessageBox.Show("Es necesario registrar el Job, antes de Generar el Reporte")
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    'Se genera el reporte de las PO generadas en el día.'
    Private Sub Button20_Click(sender As Object, e As EventArgs) Handles Button20.Click
        Dim buscarPOT As New OdbcCommand("SELECT PO, FechaSolicitudCotizacion FROM gestióndeproyectossicma.seguimiento WHERE PO!='' AND FechaSolicitudCotizacion='" & FechaPOD.Text.ToArray & "'", cn)
        Dim datosPOT As OdbcDataReader
        Try
            datosPOT = buscarPOT.ExecuteReader
            If datosPOT.Read Then
                Dim FPOT As Date = FechaPOD.Text
                Dim RPOT As New ReportePOT
                RPOT.NRPOT = FPOT
                RPOT.ShowDialog()
            Else
                MessageBox.Show("No existe ninguna Orden de Compra Registrada Hoy!")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Se genera el reporte de los Jobs generadas en el día.'
    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click
        Dim buscarJT As New OdbcCommand("SELECT Job, FechaSolicitudCotizacion FROM gestióndeproyectossicma.seguimiento WHERE FechaSolicitudCotizacion='" & FechaJD.Text.ToArray & "'", cn)
        Dim datosJT As OdbcDataReader
        Try
            datosJT = buscarJT.ExecuteReader
            If datosJT.Read Then
                Dim FJT As Date = FechaJD.Text
                Dim RJT As New ReporteJT
                RJT.NRJT = FJT
                RJT.ShowDialog()
            Else
                MessageBox.Show("No existe ningún Job Registrado Hoy!")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Se genera el reporte de las PO que estan en proceso.'
    Private Sub Button22_Click(sender As Object, e As EventArgs) Handles Button22.Click
        Dim buscarPOP As New OdbcCommand("SELECT PO, Prioridad FROM gestióndeproyectossicma.seguimiento WHERE PO!='' AND Prioridad='" & PrioridadP.Text.ToArray & "'", cn)
        Dim datosPOP As OdbcDataReader
        Try
            datosPOP = buscarPOP.ExecuteReader
            If datosPOP.Read Then
                Dim POP As String = PrioridadP.Text
                Dim RPOP As New ReportePOP
                RPOP.NRPOP = POP
                RPOP.ShowDialog()
            Else
                MessageBox.Show("No existe ninguna Orden de Compra en Proceso!")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub MontoCotizado_Leave(sender As Object, e As EventArgs) Handles MontoCotizado.Leave
        MontoCotizado.Text = FormatCurrency(MontoCotizado.Text, 2)
    End Sub

    Private Sub Cliente_Click(sender As Object, e As EventArgs) Handles Cliente.Click
        Llenar1()
    End Sub

    Private Sub Button23_Click(sender As Object, e As EventArgs)
        Dim consultarjob As New OdbcCommand("SELECT TIMESTAMPDIFF(DAY, FechaSolicitudCotizacion , CURDATE()) AS DIA FROM gestióndeproyectossicma.seguimiento where Job ='" & Job.Text.ToArray & "'", cn)
        Dim datos As OdbcDataReader
        Try
            datos = consultarjob.ExecuteReader
            If datos.Read Then
                DiasTranscurridos.Text() = datos.Item("DIA").ToString()
            Else
                MessageBox.Show("Este número de Job no se encuentra Registrado.")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button23_Click_1(sender As Object, e As EventArgs) Handles Button23.Click
        VerActividadesPendientes.Show()
        VerActividadesPendientes.MensajeAP.Text = MensajeVentas.Text
    End Sub

    Private Sub Button24_Click(sender As Object, e As EventArgs) Handles Button24.Click
        BloqueGeneral.Show()
        BloqueGeneral.JCBG.Text = JOBSP.Text
    End Sub

    Private Sub Button25_Click(sender As Object, e As EventArgs) Handles Button25.Click
        BloqueEstacion.Show()
        BloqueEstacion.JCBE.Text = JOBSP.Text
    End Sub

    Private Sub Button27_Click(sender As Object, e As EventArgs) Handles Button27.Click
        VerBloqueGeneral.Show()
    End Sub

    Private Sub Button26_Click(sender As Object, e As EventArgs) Handles Button26.Click
        VerBloqueEstacion.Show()
    End Sub

    Private Sub JOBSP_TextChanged(sender As Object, e As EventArgs) Handles JOBSP.TextChanged
        Llenar15()
    End Sub

    Private Sub Button28_Click(sender As Object, e As EventArgs) Handles Button28.Click
        Dim buscarFV As New OdbcCommand("SELECT job FROM gestióndeproyectossicma.bloqueestacion WHERE job='" & JBE.Text.ToArray & "'", cn)
        Dim datosFV As OdbcDataReader
        Try
            datosFV = buscarFV.ExecuteReader
            If datosFV.Read Then
                Dim jobBEX As String = CStr(JBE.Text)
                Dim nombreBEX As String = CStr(NBBE.Text)
                Dim ReporteBloqueEstacion As New ReporteBloqueEstacion
                ReporteBloqueEstacion.jobBE = jobBEX
                ReporteBloqueEstacion.nombreBE = nombreBEX
                ReporteBloqueEstacion.ShowDialog()
            Else
                MessageBox.Show("Es necesario llenar el campo de job y el nombre, para poder Generar el Reporte")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub JBE_TextChanged(sender As Object, e As EventArgs) Handles JBE.TextChanged
        Llenar17()
    End Sub

    Private Sub Button29_Click(sender As Object, e As EventArgs) Handles Button29.Click
        Dim buscarFV As New OdbcCommand("SELECT job FROM gestióndeproyectossicma.bloquegeneral WHERE job='" & JBG.Text.ToArray & "'", cn)
        Dim datosFV As OdbcDataReader
        Try
            datosFV = buscarFV.ExecuteReader
            If datosFV.Read Then
                Dim jobBGX As String = CStr(JBG.Text)
                Dim ReporteBloqueGeneral As New ReporteBloqueGeneral
                ReporteBloqueGeneral.jobBG = jobBGX
                ReporteBloqueGeneral.ShowDialog()
            Else
                MessageBox.Show("Es necesario llenar el campo de job y el nombre, para poder Generar el Reporte")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button30_Click(sender As Object, e As EventArgs) Handles Button30.Click
        JobA.Text = Job.Text
        DescJob.Text = Descripcion.Text
        ClienJob.Text = Cliente.Text
        ContJob.Text = Contactos.Text
    End Sub

    Private Sub JobA_TextChanged(sender As Object, e As EventArgs) Handles JobA.TextChanged
        Llenar19()
    End Sub

    Private Sub Button31_Click(sender As Object, e As EventArgs) Handles Button31.Click
        ResponsableCotizacion.Show()
    End Sub

    Private Sub Prioridad_TextChanged(sender As Object, e As EventArgs) Handles Prioridad.TextChanged
        If Prioridad.Text = "2-Cotizando" And Categoria.Text = "Proyecto" Then
            Label59.Visible = True
            Label60.Visible = True
            Label134.Visible = True
            Etapa.Visible = True
            DescEtapa.Visible = True
            Responsable.Visible = True
        Else
            Label59.Visible = False
            Label60.Visible = False
            Label134.Visible = False
            Etapa.Visible = False
            DescEtapa.Visible = False
            Responsable.Visible = False
            Etapa.Text = ""
            DescEtapa.Text = ""
            Responsable.Text = ""
        End If
    End Sub

    Private Sub NombreS_Click(sender As Object, e As EventArgs) Handles NombreS.Click
        Llenar20()
    End Sub

    Private Sub Empresa_TextChanged(sender As Object, e As EventArgs) Handles Empresa.TextChanged
        Llenar21()
    End Sub

    Private Sub Job_Click(sender As Object, e As EventArgs) Handles Job.Click
        Llenar23()
    End Sub

    Private Sub CodigoVisita_Click(sender As Object, e As EventArgs) Handles CodigoVisita.Click
        Llenar24()
    End Sub

    Private Sub CodigoAct_Click(sender As Object, e As EventArgs) Handles CodigoAct.Click
        Llenar25()
    End Sub

    Private Sub lider_Click(sender As Object, e As EventArgs) Handles lider.Click
        Llenar13()
    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        Try
            Dim da = New OdbcDataAdapter("SELECT Job AS 'Job', etapa AS 'Etapa', descripcionetapa AS 'Descrión de la Etapa', responsable AS 'Responsable', Descripcion AS 'Descripción', Cliente AS 'Cliente', Contactos AS 'Contacto' FROM gestióndeproyectossicma.seguimiento WHERE responsable='" & MensajeVentas.Text & "' AND Prioridad='2-Cotizando' AND Categoria='Proyecto'", cn)
            Dim dt = New DataTable
            da.Fill(dt)
            DGCP.DataSource = dt
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        Dim SAVE As New SaveFileDialog
        Dim ruta As String
        Dim xlApp As Object = CreateObject("Excel.Application")
        Dim pth As String = ""
        'crearemos una nueva hoja de calculo
        Dim xlwb As Object = xlApp.WorkBooks.add
        Dim xlws As Object = xlwb.WorkSheets(1)
        Try
            'exportaremos los caracteres de las columnas
            For c As Integer = 0 To DGCP.Columns.Count - 1
                xlws.cells(1, c + 1).value = DGCP.Columns(c).HeaderText
            Next
            'exportaremos las cabeceras de las calumnas
            For r As Integer = 0 To DGCP.RowCount - 1
                For c As Integer = 0 To DGCP.Columns.Count - 1
                    xlws.cells(r + 2, c + 1).value = Convert.ToString(DGCP.Item(c, r).Value)
                Next
            Next
            'guardamos la hoja de excel en la ruta especifica
            Dim SaveFileDialog1 As SaveFileDialog = New SaveFileDialog
            SaveFileDialog1.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            SaveFileDialog1.Filter = "Archivo Excel| *.xlsx"
            SaveFileDialog1.FilterIndex = 2
            If SaveFileDialog1.ShowDialog = DialogResult.OK Then
                ruta = SaveFileDialog1.FileName
                xlwb.saveas(ruta)
                xlws = Nothing
                xlwb = Nothing
                xlApp.quit()
                MsgBox("Exportado Correctamente", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class