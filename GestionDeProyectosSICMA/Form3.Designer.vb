﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form3
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form3))
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Usuario = New System.Windows.Forms.TextBox()
        Me.MU = New System.Windows.Forms.Button()
        Me.CSA = New System.Windows.Forms.Button()
        Me.EU = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GU = New System.Windows.Forms.Button()
        Me.BU = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Telefono = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Correo = New System.Windows.Forms.TextBox()
        Me.ControlUsuario = New System.Windows.Forms.TextBox()
        Me.TipoUsuario = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Contraseña = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Usuarios = New System.Windows.Forms.Label()
        Me.LU = New System.Windows.Forms.Button()
        Me.MensajeAdmin = New System.Windows.Forms.Label()
        Me.Nombre = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.Location = New System.Drawing.Point(12, 92)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 23)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Nombre:"
        '
        'Usuario
        '
        Me.Usuario.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Usuario.Location = New System.Drawing.Point(179, 121)
        Me.Usuario.Name = "Usuario"
        Me.Usuario.Size = New System.Drawing.Size(291, 30)
        Me.Usuario.TabIndex = 1
        '
        'MU
        '
        Me.MU.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MU.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MU.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MU.Image = CType(resources.GetObject("MU.Image"), System.Drawing.Image)
        Me.MU.Location = New System.Drawing.Point(217, 312)
        Me.MU.Name = "MU"
        Me.MU.Size = New System.Drawing.Size(60, 60)
        Me.MU.TabIndex = 8
        Me.MU.UseVisualStyleBackColor = True
        '
        'CSA
        '
        Me.CSA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CSA.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CSA.ForeColor = System.Drawing.Color.RoyalBlue
        Me.CSA.Image = CType(resources.GetObject("CSA.Image"), System.Drawing.Image)
        Me.CSA.Location = New System.Drawing.Point(410, 2)
        Me.CSA.Name = "CSA"
        Me.CSA.Size = New System.Drawing.Size(60, 60)
        Me.CSA.TabIndex = 1
        Me.CSA.UseVisualStyleBackColor = True
        '
        'EU
        '
        Me.EU.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EU.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EU.ForeColor = System.Drawing.Color.RoyalBlue
        Me.EU.Image = CType(resources.GetObject("EU.Image"), System.Drawing.Image)
        Me.EU.Location = New System.Drawing.Point(283, 312)
        Me.EU.Name = "EU"
        Me.EU.Size = New System.Drawing.Size(60, 60)
        Me.EU.TabIndex = 9
        Me.EU.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.Location = New System.Drawing.Point(12, 164)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(117, 23)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Contraseña:"
        '
        'GU
        '
        Me.GU.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GU.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GU.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GU.Image = CType(resources.GetObject("GU.Image"), System.Drawing.Image)
        Me.GU.Location = New System.Drawing.Point(151, 312)
        Me.GU.Name = "GU"
        Me.GU.Size = New System.Drawing.Size(60, 60)
        Me.GU.TabIndex = 7
        Me.GU.UseVisualStyleBackColor = True
        '
        'BU
        '
        Me.BU.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BU.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BU.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BU.Image = CType(resources.GetObject("BU.Image"), System.Drawing.Image)
        Me.BU.Location = New System.Drawing.Point(85, 312)
        Me.BU.Name = "BU"
        Me.BU.Size = New System.Drawing.Size(60, 60)
        Me.BU.TabIndex = 6
        Me.BU.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Black", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Location = New System.Drawing.Point(9, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(181, 38)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Bienvenid@"
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Black
        Me.Button8.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button8.Location = New System.Drawing.Point(16, 312)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(60, 60)
        Me.Button8.TabIndex = 81
        Me.Button8.Text = "VER"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label14.Location = New System.Drawing.Point(12, 200)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(92, 23)
        Me.Label14.TabIndex = 39
        Me.Label14.Text = "Teléfono:"
        '
        'Telefono
        '
        Me.Telefono.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Telefono.Location = New System.Drawing.Point(179, 193)
        Me.Telefono.Name = "Telefono"
        Me.Telefono.Size = New System.Drawing.Size(291, 30)
        Me.Telefono.TabIndex = 38
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label13.Location = New System.Drawing.Point(12, 236)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(64, 23)
        Me.Label13.TabIndex = 37
        Me.Label13.Text = "Email:"
        '
        'Correo
        '
        Me.Correo.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Correo.Location = New System.Drawing.Point(179, 229)
        Me.Correo.Name = "Correo"
        Me.Correo.Size = New System.Drawing.Size(291, 30)
        Me.Correo.TabIndex = 36
        '
        'ControlUsuario
        '
        Me.ControlUsuario.Location = New System.Drawing.Point(443, 352)
        Me.ControlUsuario.Name = "ControlUsuario"
        Me.ControlUsuario.Size = New System.Drawing.Size(20, 20)
        Me.ControlUsuario.TabIndex = 35
        Me.ControlUsuario.Text = "0"
        Me.ControlUsuario.Visible = False
        '
        'TipoUsuario
        '
        Me.TipoUsuario.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.TipoUsuario.FormattingEnabled = True
        Me.TipoUsuario.Items.AddRange(New Object() {"Administrador", "Ventas", "Proyectos"})
        Me.TipoUsuario.Location = New System.Drawing.Point(178, 265)
        Me.TipoUsuario.Name = "TipoUsuario"
        Me.TipoUsuario.Size = New System.Drawing.Size(292, 31)
        Me.TipoUsuario.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label7.Location = New System.Drawing.Point(12, 273)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(154, 23)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Tipo de Usuario:"
        '
        'Contraseña
        '
        Me.Contraseña.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Contraseña.Location = New System.Drawing.Point(179, 157)
        Me.Contraseña.Name = "Contraseña"
        Me.Contraseña.Size = New System.Drawing.Size(291, 30)
        Me.Contraseña.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.Location = New System.Drawing.Point(12, 128)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(83, 23)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Usuario:"
        '
        'Usuarios
        '
        Me.Usuarios.AutoSize = True
        Me.Usuarios.Font = New System.Drawing.Font("Arial Black", 20.0!, System.Drawing.FontStyle.Bold)
        Me.Usuarios.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Usuarios.Location = New System.Drawing.Point(209, 47)
        Me.Usuarios.Name = "Usuarios"
        Me.Usuarios.Size = New System.Drawing.Size(148, 38)
        Me.Usuarios.TabIndex = 14
        Me.Usuarios.Text = "Usuarios"
        '
        'LU
        '
        Me.LU.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LU.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LU.ForeColor = System.Drawing.Color.RoyalBlue
        Me.LU.Image = CType(resources.GetObject("LU.Image"), System.Drawing.Image)
        Me.LU.Location = New System.Drawing.Point(349, 312)
        Me.LU.Name = "LU"
        Me.LU.Size = New System.Drawing.Size(60, 60)
        Me.LU.TabIndex = 13
        Me.LU.UseVisualStyleBackColor = True
        '
        'MensajeAdmin
        '
        Me.MensajeAdmin.AutoSize = True
        Me.MensajeAdmin.Font = New System.Drawing.Font("Arial Black", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensajeAdmin.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MensajeAdmin.Location = New System.Drawing.Point(184, 9)
        Me.MensajeAdmin.Name = "MensajeAdmin"
        Me.MensajeAdmin.Size = New System.Drawing.Size(0, 38)
        Me.MensajeAdmin.TabIndex = 16
        '
        'Nombre
        '
        Me.Nombre.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Nombre.FormattingEnabled = True
        Me.Nombre.Items.AddRange(New Object() {"Administrador", "Ventas", "Proyectos"})
        Me.Nombre.Location = New System.Drawing.Point(178, 84)
        Me.Nombre.Name = "Nombre"
        Me.Nombre.Size = New System.Drawing.Size(292, 31)
        Me.Nombre.TabIndex = 82
        '
        'Form3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(484, 384)
        Me.ControlBox = False
        Me.Controls.Add(Me.Nombre)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.MensajeAdmin)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Telefono)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.CSA)
        Me.Controls.Add(Me.Correo)
        Me.Controls.Add(Me.Usuarios)
        Me.Controls.Add(Me.ControlUsuario)
        Me.Controls.Add(Me.MU)
        Me.Controls.Add(Me.TipoUsuario)
        Me.Controls.Add(Me.Usuario)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.EU)
        Me.Controls.Add(Me.Contraseña)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.GU)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.LU)
        Me.Controls.Add(Me.BU)
        Me.Controls.Add(Me.Label2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form3"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As Label
    Friend WithEvents Usuario As TextBox
    Friend WithEvents MU As Button
    Friend WithEvents CSA As Button
    Friend WithEvents EU As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents GU As Button
    Friend WithEvents BU As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents LU As Button
    Friend WithEvents Usuarios As Label
    Friend WithEvents TipoUsuario As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Contraseña As TextBox
    Friend WithEvents Label6 As Label
    Public WithEvents MensajeAdmin As Label
    Friend WithEvents ControlUsuario As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Telefono As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Correo As TextBox
    Friend WithEvents Button8 As Button
    Friend WithEvents Nombre As ComboBox
End Class
