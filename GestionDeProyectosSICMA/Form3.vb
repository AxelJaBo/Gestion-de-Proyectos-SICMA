﻿Imports System.Data.Odbc
Imports System.Data
Public Class Form3
    Private Sub Form3_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call conectar()
        Llenar()
    End Sub

    Public Sub Llenar()
        Dim daP2 = New OdbcDataAdapter("SELECT idUsuarios, nombre FROM gestióndeproyectossicma.usuarios", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        Nombre.DataSource = dtP2
        Nombre.DisplayMember = "nombre"
        Nombre.ValueMember = "idUsuarios"
    End Sub

    Function busquedaxletra(ByVal busqueda As String) As DataTable
        Dim dt As New DataTable
        Dim da As New OdbcDataAdapter("SELECT nombre AS 'Nombre', usuario AS 'Usuario', contraseña AS 'Contraseña', Tipo_Usuario AS 'Tipo de Usuario', control_user AS 'Control' FROM gestióndeproyectossicma.usuarios WHERE nombre LIKE '%" & busqueda & "%'", cn)
        da.Fill(dt)
        Return dt
    End Function

    'Cerrar sesión.'
    Private Sub CSA_Click(sender As Object, e As EventArgs) Handles CSA.Click
        MessageBox.Show("La Sesión se ha Cerrado Correctamente.")
        Try
            Dim controldeusuario As New OdbcCommand("UPDATE gestióndeproyectossicma.usuarios SET control_user='0' where nombre ='" & MensajeAdmin.Text.ToArray & "'", cn)
            controldeusuario.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Form1.Show()
        Me.Dispose()
    End Sub

    'Guardar usuario en la baase de datos.'
    Private Sub GU_Click(sender As Object, e As EventArgs) Handles GU.Click
        If Nombre.Text = "" And Usuario.Text = "" And Contraseña.Text = "" And TipoUsuario.Text = "" Then
            MsgBox("Los campos se encuentran vacíos.")
        Else
            If Nombre.Text = "" Then
                MsgBox("El campo Nombre se encuentra vacío.")
            Else
                If Usuario.Text = "" Then
                    MsgBox("El campo Usuario se encuentra vacío.")
                Else
                    If Contraseña.Text = "" Then
                        MsgBox("El campo Contraseña se encuentra vacío.")
                    Else
                        If TipoUsuario.Text = "" Then
                            MsgBox("El campo Tipo de Usuario se encuentra vacío.")
                        Else
                            If Telefono.Text = "" Then
                                MsgBox("El campo Teléfono se encuentra vacío.")
                            Else
                                If Correo.Text = "" Then
                                    MsgBox("El campo Correo Electrónico se encuentra vacío.")
                                Else
                                    Dim buscarusuario As New OdbcCommand("SELECT nombre FROM gestióndeproyectossicma.usuarios where nombre ='" & Nombre.Text.ToArray & "'", cn)
                                    Dim datos As OdbcDataReader
                                    Try
                                        datos = buscarusuario.ExecuteReader
                                        If datos.Read Then
                                            MessageBox.Show("Este usuario ya se encuentra Registrado")
                                        Else
                                            Try
                                                MessageBox.Show("Usuario guardado Satisfactoriamente.")
                                                Dim guardarusuario As New OdbcCommand("INSERT INTO  gestióndeproyectossicma.usuarios(nombre, usuario, contraseña, Tipo_Usuario, control_user, CorreoE, TelCel) VALUES ('" & Nombre.Text.ToArray & "','" & Usuario.Text.ToArray & "','" & Contraseña.Text.ToArray & "','" & TipoUsuario.Text.ToArray & "','" & ControlUsuario.Text.ToArray & "','" & Correo.Text.ToArray & "','" & Telefono.Text.ToArray & "')", cn)
                                                guardarusuario.ExecuteNonQuery()
                                            Catch ex As Exception
                                                MsgBox(ex.Message)
                                            End Try
                                        End If
                                    Catch ex As Exception
                                        MsgBox(ex.Message)
                                    End Try
                                    Nombre.Text = ""
                                    Contraseña.Text = ""
                                    Usuario.Text = ""
                                    TipoUsuario.Text = ""
                                    ControlUsuario.Text = "0"
                                    Telefono.Text = ""
                                    Correo.Text = ""
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    'Eliminar usuario dependiendo el nombre de usuario.'
    Private Sub EU_Click(sender As Object, e As EventArgs) Handles EU.Click
        Dim Msg2 As MsgBoxResult
        Msg2 = MsgBox("¿Esta seguro que desea Eliminar este Usuario?", vbYesNo, "Eliminar!!")
        If Msg2 = MsgBoxResult.Yes Then
            If Nombre.Text = "" Then
                MsgBox("El campo Nombre se encuentra vacío, es necesario que escriba el nombre del usuario que desea Elminar.")
            Else
                Dim consultarusuario As New OdbcCommand("SELECT nombre FROM gestióndeproyectossicma.usuarios WHERE nombre ='" & Nombre.Text.ToArray & "'", cn)
                Dim datos As OdbcDataReader
                Try
                    datos = consultarusuario.ExecuteReader
                    If datos.Read Then
                        MessageBox.Show("Este usuario se ha Eliminado Correctamente.")
                        Dim eliminarusuario As New OdbcCommand("DELETE FROM gestióndeproyectossicma.usuarios WHERE nombre='" & Nombre.Text.ToArray & "'", cn)
                        eliminarusuario.ExecuteNonQuery()
                    Else
                        MessageBox.Show("Este usuario no se encuentra Registrado.")
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                Nombre.Text = ""
                Contraseña.Text = ""
                Usuario.Text = ""
                TipoUsuario.Text = ""
                ControlUsuario.Text = "0"
                Telefono.Text = ""
                Correo.Text = ""
            End If
        Else
            MessageBox.Show("Se canceló la Eliminación.")
        End If
    End Sub

    'Modificar usuario dependiendo del nombre de usuario.'
    Private Sub MU_Click(sender As Object, e As EventArgs) Handles MU.Click
        Dim Msg As MsgBoxResult
        Msg = MsgBox("¿Esta seguro que desea Actualizar este Usuario?", vbYesNo, "Actualizar!!")
        If Msg = MsgBoxResult.Yes Then
            If Nombre.Text = "" And Usuario.Text = "" And Contraseña.Text = "" And TipoUsuario.Text = "" Then
                MsgBox("Los campos se encuentran vacíos.")
            Else
                If Nombre.Text = "" Then
                    MsgBox("El campo Nombre se encuentran vacío.")
                Else
                    If Usuario.Text = "" Then
                        MsgBox("El campo Usuario se encuentran vacío.")
                    Else
                        If Contraseña.Text = "" Then
                            MsgBox("El campo Contraseña se encuentran vacío.")
                        Else
                            If TipoUsuario.Text = "" Then
                                MsgBox("El campo Tipo de Usuario se encuentran vacío.")
                            Else
                                If Telefono.Text = "" Then
                                    MsgBox("El campo Teléfono se encuentra vacío.")
                                Else
                                    If Correo.Text = "" Then
                                        MsgBox("El campo Correo Electrónico se encuentra vacío.")
                                    Else
                                        Dim consultarusuario As New OdbcCommand("SELECT nombre FROM gestióndeproyectossicma.usuarios WHERE nombre ='" & Nombre.Text.ToArray & "'", cn)
                                        Dim datos As OdbcDataReader
                                        Try
                                            datos = consultarusuario.ExecuteReader
                                            If datos.Read Then
                                                MessageBox.Show("Este usuario se ha Modificado Correctamente.")
                                                Try
                                                    Dim modificarusuario As New OdbcCommand("UPDATE gestióndeproyectossicma.usuarios SET nombre='" & Nombre.Text.ToArray & "', usuario='" & Usuario.Text.ToArray & "', " & " contraseña='" & Contraseña.Text.ToArray & "', Tipo_Usuario='" & TipoUsuario.Text.ToArray & "', CorreoE='" & Correo.Text.ToArray & "', TelCel='" & Telefono.Text.ToArray & "' WHERE nombre ='" & Nombre.Text.ToArray & "'", cn)
                                                    modificarusuario.ExecuteNonQuery()
                                                Catch ex As Exception
                                                    MsgBox(ex.Message)
                                                End Try
                                            Else
                                                MessageBox.Show("Este usuario no se encuentra Registrado.")
                                            End If
                                        Catch ex As Exception
                                            MsgBox(ex.Message)
                                        End Try
                                        Nombre.Text = ""
                                        Contraseña.Text = ""
                                        Usuario.Text = ""
                                        TipoUsuario.Text = ""
                                        ControlUsuario.Text = "0"
                                        Telefono.Text = ""
                                        Correo.Text = ""
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Else
            MessageBox.Show("Se canceló la Actualización.")
        End If
    End Sub

    'Buscar usuario dependiendo  el nombre de usuario.'
    Private Sub BU_Click_1(sender As Object, e As EventArgs) Handles BU.Click
        If Nombre.Text = "" Then
            MsgBox("El campo Usuario se encuentra vacío, es necesario que escriba el nombre del usuario para poder realizar la busqueda.")
        Else
            Dim consultarusuario As New OdbcCommand("SELECT nombre, usuario, contraseña, Tipo_Usuario, CorreoE, TelCel FROM gestióndeproyectossicma.usuarios where nombre ='" & Nombre.Text.ToArray & "'", cn)
            Dim datos As OdbcDataReader
            Try
                datos = consultarusuario.ExecuteReader
                If datos.Read Then
                    Nombre.Text() = datos.Item("nombre").ToString()
                    Usuario.Text() = datos.Item("usuario").ToString()
                    Contraseña.Text() = datos.Item("contraseña").ToString()
                    TipoUsuario.Text() = datos.Item("Tipo_Usuario").ToString()
                    Correo.Text() = datos.Item("CorreoE").ToString()
                    Telefono.Text() = datos.Item("TelCel").ToString()
                Else
                    Nombre.Text = ""
                    Contraseña.Text = ""
                    Usuario.Text = ""
                    TipoUsuario.Text = ""
                    ControlUsuario.Text = "0"
                    Telefono.Text = ""
                    Correo.Text = ""
                    MessageBox.Show("Este usuario no se encuentra Registrado.")
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub Form3_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        e.Cancel = True
    End Sub

    'Limpiar campos del formulario de usuarios.'
    Private Sub LU_Click(sender As Object, e As EventArgs) Handles LU.Click
        Nombre.Text = ""
        Contraseña.Text = ""
        Usuario.Text = ""
        TipoUsuario.Text = ""
        ControlUsuario.Text = "0"
        Telefono.Text = ""
        Correo.Text = ""
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        VerUsuarios.Show()
    End Sub
End Class