﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form4
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form4))
        Me.CerrarS = New System.Windows.Forms.Button()
        Me.MensajeProyectos = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.MU = New System.Windows.Forms.Button()
        Me.Status = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.codigo = New System.Windows.Forms.ComboBox()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.BusquedaLetraA = New System.Windows.Forms.TextBox()
        Me.DGA = New System.Windows.Forms.DataGridView()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.NombreProyectista = New System.Windows.Forms.TextBox()
        Me.CodigoVisitaP = New System.Windows.Forms.TextBox()
        Me.CodVisita = New System.Windows.Forms.Label()
        Me.LV = New System.Windows.Forms.Button()
        Me.EV = New System.Windows.Forms.Button()
        Me.MV = New System.Windows.Forms.Button()
        Me.GV = New System.Windows.Forms.Button()
        Me.BV = New System.Windows.Forms.Button()
        Me.DescVisitaP = New System.Windows.Forms.TextBox()
        Me.DescripV = New System.Windows.Forms.Label()
        Me.VV = New System.Windows.Forms.Label()
        Me.ContactosVisitaP = New System.Windows.Forms.ComboBox()
        Me.ClienteVisitaP = New System.Windows.Forms.ComboBox()
        Me.ConV = New System.Windows.Forms.Label()
        Me.CV = New System.Windows.Forms.Label()
        Me.DateTimePicker5 = New System.Windows.Forms.DateTimePicker()
        Me.FPVisita = New System.Windows.Forms.Label()
        Me.DateTimePicker4 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Visitas = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.DGCP = New System.Windows.Forms.DataGridView()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.DGA, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.DGCP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CerrarS
        '
        Me.CerrarS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CerrarS.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CerrarS.ForeColor = System.Drawing.Color.RoyalBlue
        Me.CerrarS.Image = CType(resources.GetObject("CerrarS.Image"), System.Drawing.Image)
        Me.CerrarS.Location = New System.Drawing.Point(1112, 2)
        Me.CerrarS.Name = "CerrarS"
        Me.CerrarS.Size = New System.Drawing.Size(60, 60)
        Me.CerrarS.TabIndex = 5
        Me.CerrarS.UseVisualStyleBackColor = True
        '
        'MensajeProyectos
        '
        Me.MensajeProyectos.AutoSize = True
        Me.MensajeProyectos.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensajeProyectos.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MensajeProyectos.Location = New System.Drawing.Point(695, 9)
        Me.MensajeProyectos.Name = "MensajeProyectos"
        Me.MensajeProyectos.Size = New System.Drawing.Size(0, 38)
        Me.MensajeProyectos.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Location = New System.Drawing.Point(508, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(181, 38)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Bienvenid@"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(2, 75)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1186, 611)
        Me.TabControl1.TabIndex = 9
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.Black
        Me.TabPage2.Controls.Add(Me.Button2)
        Me.TabPage2.Controls.Add(Me.Button1)
        Me.TabPage2.Controls.Add(Me.Button5)
        Me.TabPage2.Controls.Add(Me.MU)
        Me.TabPage2.Controls.Add(Me.Status)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.codigo)
        Me.TabPage2.Controls.Add(Me.Button8)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.BusquedaLetraA)
        Me.TabPage2.Controls.Add(Me.DGA)
        Me.TabPage2.Location = New System.Drawing.Point(4, 32)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1178, 575)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Actividades"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Black
        Me.Button2.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button2.Location = New System.Drawing.Point(590, 5)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(209, 33)
        Me.Button2.TabIndex = 121
        Me.Button2.Text = "Notas y Comentarios"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Black
        Me.Button1.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button1.Location = New System.Drawing.Point(805, 6)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(112, 33)
        Me.Button1.TabIndex = 107
        Me.Button1.Text = "Exportar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button5.Image = CType(resources.GetObject("Button5.Image"), System.Drawing.Image)
        Me.Button5.Location = New System.Drawing.Point(924, 6)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(60, 60)
        Me.Button5.TabIndex = 106
        Me.Button5.UseVisualStyleBackColor = True
        '
        'MU
        '
        Me.MU.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MU.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MU.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MU.Image = CType(resources.GetObject("MU.Image"), System.Drawing.Image)
        Me.MU.Location = New System.Drawing.Point(990, 6)
        Me.MU.Name = "MU"
        Me.MU.Size = New System.Drawing.Size(60, 60)
        Me.MU.TabIndex = 105
        Me.MU.UseVisualStyleBackColor = True
        '
        'Status
        '
        Me.Status.FormattingEnabled = True
        Me.Status.Items.AddRange(New Object() {"En Proceso", "Terminada"})
        Me.Status.Location = New System.Drawing.Point(768, 43)
        Me.Status.Name = "Status"
        Me.Status.Size = New System.Drawing.Size(149, 31)
        Me.Status.TabIndex = 104
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.Location = New System.Drawing.Point(689, 51)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(73, 23)
        Me.Label6.TabIndex = 103
        Me.Label6.Text = "Status:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.Location = New System.Drawing.Point(480, 51)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 23)
        Me.Label5.TabIndex = 102
        Me.Label5.Text = "Código:"
        '
        'codigo
        '
        Me.codigo.FormattingEnabled = True
        Me.codigo.Location = New System.Drawing.Point(562, 44)
        Me.codigo.Name = "codigo"
        Me.codigo.Size = New System.Drawing.Size(121, 31)
        Me.codigo.TabIndex = 101
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Black
        Me.Button8.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button8.Location = New System.Drawing.Point(1056, 6)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(110, 60)
        Me.Button8.TabIndex = 100
        Me.Button8.Text = "Actualizar"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial Black", 20.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Location = New System.Drawing.Point(364, -2)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(192, 38)
        Me.Label4.TabIndex = 26
        Me.Label4.Text = "Actividades"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.Location = New System.Drawing.Point(11, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(177, 23)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Busqueda General:"
        '
        'BusquedaLetraA
        '
        Me.BusquedaLetraA.Location = New System.Drawing.Point(194, 45)
        Me.BusquedaLetraA.Name = "BusquedaLetraA"
        Me.BusquedaLetraA.Size = New System.Drawing.Size(200, 30)
        Me.BusquedaLetraA.TabIndex = 24
        '
        'DGA
        '
        Me.DGA.AllowUserToAddRows = False
        Me.DGA.AllowUserToDeleteRows = False
        Me.DGA.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DGA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGA.Location = New System.Drawing.Point(13, 81)
        Me.DGA.Name = "DGA"
        Me.DGA.ReadOnly = True
        Me.DGA.RowHeadersVisible = False
        Me.DGA.Size = New System.Drawing.Size(1153, 484)
        Me.DGA.TabIndex = 23
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.Black
        Me.TabPage1.Controls.Add(Me.Button9)
        Me.TabPage1.Controls.Add(Me.NombreProyectista)
        Me.TabPage1.Controls.Add(Me.CodigoVisitaP)
        Me.TabPage1.Controls.Add(Me.CodVisita)
        Me.TabPage1.Controls.Add(Me.LV)
        Me.TabPage1.Controls.Add(Me.EV)
        Me.TabPage1.Controls.Add(Me.MV)
        Me.TabPage1.Controls.Add(Me.GV)
        Me.TabPage1.Controls.Add(Me.BV)
        Me.TabPage1.Controls.Add(Me.DescVisitaP)
        Me.TabPage1.Controls.Add(Me.DescripV)
        Me.TabPage1.Controls.Add(Me.VV)
        Me.TabPage1.Controls.Add(Me.ContactosVisitaP)
        Me.TabPage1.Controls.Add(Me.ClienteVisitaP)
        Me.TabPage1.Controls.Add(Me.ConV)
        Me.TabPage1.Controls.Add(Me.CV)
        Me.TabPage1.Controls.Add(Me.DateTimePicker5)
        Me.TabPage1.Controls.Add(Me.FPVisita)
        Me.TabPage1.Controls.Add(Me.DateTimePicker4)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Visitas)
        Me.TabPage1.Location = New System.Drawing.Point(4, 32)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1178, 575)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Visitas"
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.Black
        Me.Button9.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button9.Location = New System.Drawing.Point(365, 323)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(60, 60)
        Me.Button9.TabIndex = 104
        Me.Button9.Text = "VER"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'NombreProyectista
        '
        Me.NombreProyectista.Enabled = False
        Me.NombreProyectista.Location = New System.Drawing.Point(477, 148)
        Me.NombreProyectista.Name = "NombreProyectista"
        Me.NombreProyectista.Size = New System.Drawing.Size(300, 30)
        Me.NombreProyectista.TabIndex = 43
        '
        'CodigoVisitaP
        '
        Me.CodigoVisitaP.Location = New System.Drawing.Point(477, 39)
        Me.CodigoVisitaP.Name = "CodigoVisitaP"
        Me.CodigoVisitaP.Size = New System.Drawing.Size(300, 30)
        Me.CodigoVisitaP.TabIndex = 24
        '
        'CodVisita
        '
        Me.CodVisita.AutoSize = True
        Me.CodVisita.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CodVisita.ForeColor = System.Drawing.Color.RoyalBlue
        Me.CodVisita.Location = New System.Drawing.Point(241, 46)
        Me.CodVisita.Name = "CodVisita"
        Me.CodVisita.Size = New System.Drawing.Size(76, 23)
        Me.CodVisita.TabIndex = 42
        Me.CodVisita.Text = "Código:"
        '
        'LV
        '
        Me.LV.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.LV.Image = CType(resources.GetObject("LV.Image"), System.Drawing.Image)
        Me.LV.Location = New System.Drawing.Point(695, 323)
        Me.LV.Name = "LV"
        Me.LV.Size = New System.Drawing.Size(60, 60)
        Me.LV.TabIndex = 41
        Me.LV.UseVisualStyleBackColor = True
        '
        'EV
        '
        Me.EV.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.EV.Image = CType(resources.GetObject("EV.Image"), System.Drawing.Image)
        Me.EV.Location = New System.Drawing.Point(629, 323)
        Me.EV.Name = "EV"
        Me.EV.Size = New System.Drawing.Size(60, 60)
        Me.EV.TabIndex = 39
        Me.EV.UseVisualStyleBackColor = True
        '
        'MV
        '
        Me.MV.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MV.Image = CType(resources.GetObject("MV.Image"), System.Drawing.Image)
        Me.MV.Location = New System.Drawing.Point(563, 323)
        Me.MV.Name = "MV"
        Me.MV.Size = New System.Drawing.Size(60, 60)
        Me.MV.TabIndex = 38
        Me.MV.UseVisualStyleBackColor = True
        '
        'GV
        '
        Me.GV.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GV.Image = CType(resources.GetObject("GV.Image"), System.Drawing.Image)
        Me.GV.Location = New System.Drawing.Point(497, 323)
        Me.GV.Name = "GV"
        Me.GV.Size = New System.Drawing.Size(60, 60)
        Me.GV.TabIndex = 37
        Me.GV.UseVisualStyleBackColor = True
        '
        'BV
        '
        Me.BV.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BV.Image = CType(resources.GetObject("BV.Image"), System.Drawing.Image)
        Me.BV.Location = New System.Drawing.Point(431, 323)
        Me.BV.Name = "BV"
        Me.BV.Size = New System.Drawing.Size(60, 60)
        Me.BV.TabIndex = 36
        Me.BV.UseVisualStyleBackColor = True
        '
        'DescVisitaP
        '
        Me.DescVisitaP.Location = New System.Drawing.Point(477, 257)
        Me.DescVisitaP.Multiline = True
        Me.DescVisitaP.Name = "DescVisitaP"
        Me.DescVisitaP.Size = New System.Drawing.Size(300, 60)
        Me.DescVisitaP.TabIndex = 32
        '
        'DescripV
        '
        Me.DescripV.AutoSize = True
        Me.DescripV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.DescripV.Location = New System.Drawing.Point(241, 294)
        Me.DescripV.Name = "DescripV"
        Me.DescripV.Size = New System.Drawing.Size(120, 23)
        Me.DescripV.TabIndex = 35
        Me.DescripV.Text = "Descripción:"
        '
        'VV
        '
        Me.VV.AutoSize = True
        Me.VV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.VV.Location = New System.Drawing.Point(241, 155)
        Me.VV.Name = "VV"
        Me.VV.Size = New System.Drawing.Size(84, 23)
        Me.VV.TabIndex = 34
        Me.VV.Text = "Nombre:"
        '
        'ContactosVisitaP
        '
        Me.ContactosVisitaP.FormattingEnabled = True
        Me.ContactosVisitaP.Location = New System.Drawing.Point(477, 221)
        Me.ContactosVisitaP.Name = "ContactosVisitaP"
        Me.ContactosVisitaP.Size = New System.Drawing.Size(300, 31)
        Me.ContactosVisitaP.TabIndex = 30
        '
        'ClienteVisitaP
        '
        Me.ClienteVisitaP.FormattingEnabled = True
        Me.ClienteVisitaP.Location = New System.Drawing.Point(477, 184)
        Me.ClienteVisitaP.Name = "ClienteVisitaP"
        Me.ClienteVisitaP.Size = New System.Drawing.Size(300, 31)
        Me.ClienteVisitaP.TabIndex = 29
        '
        'ConV
        '
        Me.ConV.AutoSize = True
        Me.ConV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.ConV.Location = New System.Drawing.Point(241, 229)
        Me.ConV.Name = "ConV"
        Me.ConV.Size = New System.Drawing.Size(96, 23)
        Me.ConV.TabIndex = 33
        Me.ConV.Text = "Contacto:"
        '
        'CV
        '
        Me.CV.AutoSize = True
        Me.CV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CV.ForeColor = System.Drawing.Color.RoyalBlue
        Me.CV.Location = New System.Drawing.Point(241, 192)
        Me.CV.Name = "CV"
        Me.CV.Size = New System.Drawing.Size(77, 23)
        Me.CV.TabIndex = 31
        Me.CV.Text = "Cliente:"
        '
        'DateTimePicker5
        '
        Me.DateTimePicker5.CustomFormat = "yyyy-MM-dd"
        Me.DateTimePicker5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker5.Location = New System.Drawing.Point(477, 112)
        Me.DateTimePicker5.Name = "DateTimePicker5"
        Me.DateTimePicker5.Size = New System.Drawing.Size(300, 30)
        Me.DateTimePicker5.TabIndex = 25
        '
        'FPVisita
        '
        Me.FPVisita.AutoSize = True
        Me.FPVisita.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FPVisita.ForeColor = System.Drawing.Color.RoyalBlue
        Me.FPVisita.Location = New System.Drawing.Point(241, 119)
        Me.FPVisita.Name = "FPVisita"
        Me.FPVisita.Size = New System.Drawing.Size(229, 23)
        Me.FPVisita.TabIndex = 28
        Me.FPVisita.Text = "Fecha de Próxima Visita:"
        '
        'DateTimePicker4
        '
        Me.DateTimePicker4.CustomFormat = "yyyy-MM-dd"
        Me.DateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker4.Location = New System.Drawing.Point(477, 75)
        Me.DateTimePicker4.Name = "DateTimePicker4"
        Me.DateTimePicker4.Size = New System.Drawing.Size(300, 30)
        Me.DateTimePicker4.TabIndex = 27
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.Location = New System.Drawing.Point(241, 82)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(151, 23)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Fecha de Visita:"
        '
        'Visitas
        '
        Me.Visitas.AutoSize = True
        Me.Visitas.Font = New System.Drawing.Font("Arial Black", 20.0!, System.Drawing.FontStyle.Bold)
        Me.Visitas.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Visitas.Location = New System.Drawing.Point(502, -2)
        Me.Visitas.Name = "Visitas"
        Me.Visitas.Size = New System.Drawing.Size(120, 38)
        Me.Visitas.TabIndex = 23
        Me.Visitas.Text = "Visitas"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.Black
        Me.TabPage3.Controls.Add(Me.DGCP)
        Me.TabPage3.Controls.Add(Me.Button12)
        Me.TabPage3.Controls.Add(Me.Button13)
        Me.TabPage3.Controls.Add(Me.Label11)
        Me.TabPage3.Location = New System.Drawing.Point(4, 32)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1178, 575)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Cotizaciones"
        '
        'DGCP
        '
        Me.DGCP.AllowUserToAddRows = False
        Me.DGCP.AllowUserToDeleteRows = False
        Me.DGCP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DGCP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGCP.Location = New System.Drawing.Point(6, 79)
        Me.DGCP.Name = "DGCP"
        Me.DGCP.ReadOnly = True
        Me.DGCP.RowHeadersVisible = False
        Me.DGCP.Size = New System.Drawing.Size(1160, 486)
        Me.DGCP.TabIndex = 111
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.Black
        Me.Button12.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button12.Location = New System.Drawing.Point(938, 5)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(112, 33)
        Me.Button12.TabIndex = 110
        Me.Button12.Text = "Exportar"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.Color.Black
        Me.Button13.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button13.Location = New System.Drawing.Point(1056, 5)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(110, 60)
        Me.Button13.TabIndex = 109
        Me.Button13.Text = "Actualizar"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label11.Location = New System.Drawing.Point(410, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(208, 38)
        Me.Label11.TabIndex = 108
        Me.Label11.Text = "Cotizaciones"
        '
        'Form4
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1184, 684)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.MensajeProyectos)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CerrarS)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Form4"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.DGA, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.DGCP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CerrarS As Button
    Public WithEvents MensajeProyectos As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Public WithEvents NombreProyectista As TextBox
    Friend WithEvents CodigoVisitaP As TextBox
    Friend WithEvents CodVisita As Label
    Friend WithEvents LV As Button
    Friend WithEvents EV As Button
    Friend WithEvents MV As Button
    Friend WithEvents GV As Button
    Friend WithEvents BV As Button
    Friend WithEvents DescVisitaP As TextBox
    Friend WithEvents DescripV As Label
    Friend WithEvents VV As Label
    Friend WithEvents ContactosVisitaP As ComboBox
    Friend WithEvents ClienteVisitaP As ComboBox
    Friend WithEvents ConV As Label
    Friend WithEvents CV As Label
    Friend WithEvents DateTimePicker5 As DateTimePicker
    Friend WithEvents FPVisita As Label
    Friend WithEvents DateTimePicker4 As DateTimePicker
    Friend WithEvents Label2 As Label
    Friend WithEvents Visitas As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents BusquedaLetraA As TextBox
    Friend WithEvents DGA As DataGridView
    Friend WithEvents Button8 As Button
    Friend WithEvents codigo As ComboBox
    Friend WithEvents Status As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents MU As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents Button9 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents DGCP As DataGridView
    Friend WithEvents Button12 As Button
    Friend WithEvents Button13 As Button
    Friend WithEvents Label11 As Label
End Class
