﻿Imports System.Data.Odbc
Public Class Form4
    Dim Hoy As Date = DateTime.Now.ToString("yyyy/MM/dd")
    Dim Antes As Date = DateTime.Now.AddDays(-5).ToString("yyyy/MM/dd")
    Private Sub Form4_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Llenar2()
        Llenar3()
        DateTimePicker4.Value = DateTime.Now
        DateTimePicker5.Value = DateTime.Now
        Try
            Dim daac = New OdbcDataAdapter("SELECT actividades.Cod_act AS 'Código', actividades.Job AS 'Job', actividades.Fecha AS 'Fecha de Actividad', actividades.Fecha_Meta AS 'Fecha Meta', actividades.Nombre AS 'Nombre', seguimiento.Cliente AS 'Empresa', seguimiento.Contactos AS 'Contacto', actividades.Titulo AS 'Título', actividades.Descripcion AS 'Descripción', actividades.Comentario AS 'Comentario', actividades.Nota AS 'Nota', actividades.Status AS 'Status'
FROM gestióndeproyectossicma.actividades INNER JOIN gestióndeproyectossicma.seguimiento ON actividades.Job = seguimiento.Job WHERE (actividades.Nombre='" & MensajeProyectos.Text.ToString & "' AND actividades.Status='En Proceso') OR (actividades.Nombre='" & MensajeProyectos.Text.ToString & "' AND actividades.Status='Sin Comenzar')", cn)
            Dim dtac = New DataTable
            daac.Fill(dtac)
            DGA.DataSource = dtac
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Cerrar Sesión.'
    Private Sub CerrarS_Click(sender As Object, e As EventArgs) Handles CerrarS.Click
        MessageBox.Show("La Sesión se ha Cerrado Correctamente.")
        Try
            Dim controldeusuario As New OdbcCommand("UPDATE gestióndeproyectossicma.usuarios SET control_user='0' where nombre ='" & MensajeProyectos.Text.ToArray & "'", cn)
            controldeusuario.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Form1.Show()
        Me.Dispose()
    End Sub

    Private Sub Form4_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        e.Cancel = True
    End Sub


    Public Sub Llenar2()
        Dim dax = New OdbcDataAdapter("SELECT Cod_act FROM gestióndeproyectossicma.actividades WHERE (Nombre='" & MensajeProyectos.Text.ToString & "' AND Status='En Proceso') OR (Nombre='" & MensajeProyectos.Text.ToString & "' AND Status='Sin Comenzar') ORDER BY idactividades ASC", cn)
        Dim dtx = New DataTable
        dax.Fill(dtx)
        codigo.DataSource = dtx
        codigo.DisplayMember = "Cod_act"
    End Sub

    Public Sub Llenar3()
        Dim daV = New OdbcDataAdapter("SELECT DISTINCT Cliente FROM gestióndeproyectossicma.clientes ORDER BY Cliente ASC", cn)
        Dim dtV = New DataTable
        daV.Fill(dtV)
        ClienteVisitaP.DataSource = dtV
        ClienteVisitaP.DisplayMember = "Cliente"
    End Sub

    Public Sub Llenar4()
        Dim daV2 = New OdbcDataAdapter("SELECT idClientes, Contacto FROM gestióndeproyectossicma.clientes WHERE Cliente='" & ClienteVisitaP.Text.ToString & "'", cn)
        Dim dtV2 = New DataTable
        daV2.Fill(dtV2)
        ContactosVisitaP.DataSource = dtV2
        ContactosVisitaP.DisplayMember = "Contacto"
        ContactosVisitaP.ValueMember = "idClientes"
    End Sub

    'Se busca las visitas de proyectos.'
    Private Sub BV_Click(sender As Object, e As EventArgs) Handles BV.Click
        If CodigoVisitaP.Text = "" Then
            MsgBox("El campo Código se encuentra vacío.")
        Else
            Dim consultarvisitap As New OdbcCommand("SELECT Cod_Visita, fechavisita, fechaproxvisita, vendedor, cliente, contacto, descripcion FROM gestióndeproyectossicma.visitas WHERE Cod_Visita ='" & CodigoVisitaP.Text.ToArray & "' AND vendedor='" & NombreProyectista.Text.ToArray & "'", cn)
            Dim datosvip As OdbcDataReader
            Try
                datosvip = consultarvisitap.ExecuteReader
                If datosvip.Read Then
                    CodigoVisitaP.Text() = datosvip.Item("Cod_Visita").ToString()
                    DateTimePicker4.Text() = datosvip.Item("fechavisita").ToString()
                    DateTimePicker5.Text() = datosvip.Item("fechaproxvisita").ToString()
                    NombreProyectista.Text() = datosvip.Item("vendedor").ToString()
                    ClienteVisitaP.Text() = datosvip.Item("cliente").ToString()
                    ContactosVisitaP.Text() = datosvip.Item("contacto").ToString()
                    DescVisitaP.Text() = datosvip.Item("descripcion").ToString()
                Else
                    MessageBox.Show("Esta Visita no se encuentra Registrada.")
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    'Se guardan los datos de la visita.'
    Private Sub GV_Click(sender As Object, e As EventArgs) Handles GV.Click
        If CodigoVisitaP.Text = "" And DescVisitaP.Text = "" Then
            MsgBox("Los campos se encuentran vacíos.")
        Else
            If CodigoVisitaP.Text = "" Then
                MsgBox("El campo Código se encuentra vacío.")
            Else
                If DescVisitaP.Text = "" Then
                    MsgBox("El campo Descripción se encuentra vacío.")
                Else
                    Dim buscarvisitaP As New OdbcCommand("SELECT Cod_Visita, vendedor FROM gestióndeproyectossicma.visitas WHERE Cod_Visita ='" & CodigoVisitaP.Text.ToArray & "'" & "AND vendedor= '" & NombreProyectista.Text & "'", cn)
                    Dim datosvisP As OdbcDataReader
                    Try
                        datosvisP = buscarvisitaP.ExecuteReader
                        If datosvisP.Read Then
                            MessageBox.Show("Esta Visita ya se encuentra Registrado")
                        Else
                            Try
                                MessageBox.Show("Los datos de la Visita se han guardado Satisfactoriamente.")
                                Dim guardarvisitaP As New OdbcCommand("INSERT INTO  gestióndeproyectossicma.visitas(Cod_Visita, fechavisita, fechaproxvisita, vendedor, cliente, contacto, descripcion) VALUES ('" & CodigoVisitaP.Text & "','" & DateTimePicker4.Text & "','" & DateTimePicker5.Text & "','" & NombreProyectista.Text & "','" & ClienteVisitaP.Text & "','" & ContactosVisitaP.Text & "','" & DescVisitaP.Text & "')", cn)
                                guardarvisitaP.ExecuteNonQuery()
                            Catch ex As Exception
                                MsgBox(ex.Message)
                            End Try
                        End If
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                    Llenar3()
                    DateTimePicker4.Value = DateTime.Now
                    DateTimePicker5.Value = DateTime.Now
                    CodigoVisitaP.Text = ""
                    DescVisitaP.Text = ""
                End If
            End If
        End If
    End Sub

    Private Sub ClienteVisitaP_TextChanged(sender As Object, e As EventArgs) Handles ClienteVisitaP.TextChanged
        Llenar4()
    End Sub

    'Se modifican los datos de las visitas.'
    Private Sub MV_Click(sender As Object, e As EventArgs) Handles MV.Click
        Dim Msg2 As MsgBoxResult
        Msg2 = MsgBox("¿Esta seguro que desea Actualizar esta Visita?", vbYesNo, "Actualizar!!")
        If Msg2 = MsgBoxResult.Yes Then
            If CodigoVisitaP.Text = "" Then
                MsgBox("El campo Código se encuentra vacío.")
            Else
                Dim consultarVisitaP As New OdbcCommand("SELECT Cod_Visita, vendedor FROM gestióndeproyectossicma.visitas WHERE Cod_Visita ='" & CodigoVisitaP.Text.ToArray & "' AND vendedor='" & NombreProyectista.Text.ToArray & "'", cn)
                Dim datosVXP As OdbcDataReader
                Try
                    datosVXP = consultarVisitaP.ExecuteReader
                    If datosVXP.Read Then
                        MessageBox.Show("Esta Visita se ha Modificado Correctamente.")
                        Try
                            Dim modificarVXP As New OdbcCommand("UPDATE gestióndeproyectossicma.visitas SET Cod_Visita='" & CodigoVisitaP.Text.ToArray & "', " & " fechavisita='" & DateTimePicker4.Text.ToArray & "', " & " fechaproxvisita='" & DateTimePicker5.Text.ToArray & "', " & "vendedor='" & NombreProyectista.Text.ToArray & "', " & " cliente= '" & ClienteVisitaP.Text.ToArray & "', " & " contacto= '" & ContactosVisitaP.Text.ToArray & "', " & " descripcion= '" & DescVisitaP.Text.ToArray & "' where Cod_Visita ='" & CodigoVisitaP.Text.ToArray & "'", cn)
                            modificarVXP.ExecuteNonQuery()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Else
                        MessageBox.Show("Esta Visita no se encuentra Registrada.")
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                Llenar3()
                DateTimePicker4.Value = DateTime.Now
                DateTimePicker5.Value = DateTime.Now
                CodigoVisitaP.Text = ""
                DescVisitaP.Text = ""
            End If
        Else
            MessageBox.Show("Se canceló la Actualización.")
        End If
    End Sub

    'Se Elimina la visita.'
    Private Sub EV_Click(sender As Object, e As EventArgs) Handles EV.Click
        Dim Msg As MsgBoxResult
        Msg = MsgBox("¿Esta seguro que desea Eliminar esta Visita?", vbYesNo, "Eliminar!!")
        If Msg = MsgBoxResult.Yes Then
            If CodigoVisitaP.Text = "" Then
                MsgBox("El campo Código se encuentra vacío.")
            Else
                Dim consultarvisitaxxxx As New OdbcCommand("SELECT Cod_Visita, vendedor FROM gestióndeproyectossicma.visitas WHERE Cod_Visita='" & CodigoVisitaP.Text.ToArray & "' AND vendedor='" & NombreProyectista.Text.ToArray & "'", cn)
                Dim datosxxxx As OdbcDataReader
                Try
                    datosxxxx = consultarvisitaxxxx.ExecuteReader
                    If datosxxxx.Read Then
                        MessageBox.Show("Esta Visita se ha Eliminado Correctamente.")
                        Dim eliminarempresa As New OdbcCommand("DELETE FROM gestióndeproyectossicma.visitas WHERE Cod_Visita='" & CodigoVisitaP.Text.ToArray & "' AND vendedor='" & NombreProyectista.Text.ToArray & "'", cn)
                        eliminarempresa.ExecuteNonQuery()
                    Else
                        MessageBox.Show("Esta Empresa no se encuentra Registrado.")
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                Llenar3()
                DateTimePicker4.Value = DateTime.Now
                DateTimePicker5.Value = DateTime.Now
                CodigoVisitaP.Text = ""
                DescVisitaP.Text = ""
            End If
        Else
            MessageBox.Show("Se canceló la Eliminación.")
        End If
    End Sub

    'Se limpian los campos del formulario de visitas.'
    Private Sub LV_Click(sender As Object, e As EventArgs) Handles LV.Click
        Llenar3()
        DateTimePicker4.Value = DateTime.Now
        DateTimePicker5.Value = DateTime.Now
        CodigoVisitaP.Text = ""
        DescVisitaP.Text = ""
    End Sub

    Function busquedaxletraA(ByVal busquedaA As String) As DataTable
        Dim dt As New DataTable
        Dim da As New OdbcDataAdapter("SELECT actividades.Cod_act AS 'Código', actividades.Job AS 'Job', actividades.Fecha AS 'Fecha de Actividad', actividades.Fecha_Meta AS 'Fecha Meta', actividades.Nombre AS 'Nombre', seguimiento.Cliente AS 'Empresa', seguimiento.Contactos AS 'Contacto', actividades.Titulo AS 'Título', actividades.Descripcion AS 'Descripción', actividades.Comentario AS 'Comentario', actividades.Nota AS 'Nota', actividades.Status AS 'Status'
FROM gestióndeproyectossicma.actividades INNER JOIN gestióndeproyectossicma.seguimiento ON actividades.Job = seguimiento.Job WHERE actividades.Cod_act LIKE '%" & busquedaA & "%' " & "AND ((actividades.Nombre='" & MensajeProyectos.Text.ToArray & "' AND actividades.Status='Sin Comenzar') OR (actividades.Nombre='" & MensajeProyectos.Text.ToArray & "' AND actividades.Status='En Proceso'))", cn)
        da.Fill(dt)
        Return dt
    End Function

    Private Sub BusquedaLetraA_TextChanged(sender As Object, e As EventArgs) Handles BusquedaLetraA.TextChanged
        If busquedaxletraA(BusquedaLetraA.Text).Rows.Count > 0 Then
            DGA.DataSource = busquedaxletraA(BusquedaLetraA.Text)
        End If
    End Sub

    'Actualiza los datos de la tabla.'
    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Llenar2()
        Try
            Dim daac = New OdbcDataAdapter("SELECT actividades.Cod_act AS 'Código', actividades.Job AS 'Job', actividades.Fecha AS 'Fecha de Actividad', actividades.Fecha_Meta AS 'Fecha Meta', actividades.Nombre AS 'Nombre', seguimiento.Cliente AS 'Empresa', seguimiento.Contactos AS 'Contacto', actividades.Titulo AS 'Título', actividades.Descripcion AS 'Descripción', actividades.Comentario AS 'Comentario', actividades.Nota AS 'Nota', actividades.Status AS 'Status'
FROM gestióndeproyectossicma.actividades INNER JOIN gestióndeproyectossicma.seguimiento ON actividades.Job = seguimiento.Job WHERE (actividades.Nombre='" & MensajeProyectos.Text.ToString & "' AND actividades.Status='En Proceso') OR (actividades.Nombre='" & MensajeProyectos.Text.ToString & "' AND actividades.Status='Sin Comenzar')", cn)
            Dim dtac = New DataTable
            daac.Fill(dtac)
            DGA.DataSource = dtac
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub MU_Click(sender As Object, e As EventArgs) Handles MU.Click
        Dim Msg3 As MsgBoxResult
        Msg3 = MsgBox("¿Esta seguro que desea Actualizar esta Actividad?", vbYesNo, "Actualizar!!")
        If Msg3 = MsgBoxResult.Yes Then
            If codigo.Text = "" And Status.Text = "" Then
                MsgBox("Los campos se encuentran vacíos.")
            Else
                If codigo.Text = "" Then
                    MsgBox("El campo Código se encuentra vacío.")
                Else
                    If Status.Text = "" Then
                        MsgBox("El campo Status se encuentra vacío.")
                    Else
                        Dim consultaractividad3 As New OdbcCommand("SELECT Cod_act FROM gestióndeproyectossicma.actividades WHERE Cod_act ='" & codigo.Text.ToArray & "'", cn)
                        Dim datosactividad3 As OdbcDataReader
                        Try
                            datosactividad3 = consultaractividad3.ExecuteReader
                            If datosactividad3.Read Then
                                MessageBox.Show("Esta Actividad se ha Modificado Correctamente.")
                                Try
                                    Dim modificarVX As New OdbcCommand("UPDATE gestióndeproyectossicma.actividades SET Status='" & Status.Text.ToArray & "' WHERE Cod_act ='" & codigo.Text.ToArray & "'", cn)
                                    modificarVX.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            Else
                                MessageBox.Show("Esta Actividad no se encuentra Registrada.")
                            End If
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                        Llenar2()
                        Status.Text = "En Proceso"
                    End If
                End If
            End If
            Try
                Dim daac = New OdbcDataAdapter("SELECT actividades.Cod_act AS 'Código', actividades.Job AS 'Job', actividades.Fecha AS 'Fecha de Actividad', actividades.Fecha_Meta AS 'Fecha Meta', actividades.Nombre AS 'Nombre', seguimiento.Cliente AS 'Empresa', seguimiento.Contactos AS 'Contacto', actividades.Titulo AS 'Título', actividades.Descripcion AS 'Descripción', actividades.Comentario AS 'Comentario', actividades.Nota AS 'Nota', actividades.Status AS 'Status'
FROM gestióndeproyectossicma.actividades INNER JOIN gestióndeproyectossicma.seguimiento ON actividades.Job = seguimiento.Job WHERE (actividades.Nombre='" & MensajeProyectos.Text.ToString & "' AND actividades.Status='En Proceso') OR (actividades.Nombre='" & MensajeProyectos.Text.ToString & "' AND actividades.Status='Sin Comenzar')", cn)
                Dim dtac = New DataTable
                daac.Fill(dtac)
                DGA.DataSource = dtac
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("Se canceló la Actualización.")
        End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If codigo.Text = "" Then
            MsgBox("El campo Código se encuentra vacío.")
        Else
            Dim consultaractividad As New OdbcCommand("SELECT Status FROM gestióndeproyectossicma.actividades WHERE Cod_act='" & codigo.Text.ToArray & "'", cn)
            Dim datosactividad As OdbcDataReader
            Try
                datosactividad = consultaractividad.ExecuteReader
                If datosactividad.Read Then
                    Status.Text() = datosactividad.Item("Status").ToString()
                Else
                    MessageBox.Show("Esta Actividad no se encuentra Registrada.")
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        VerVisitasP.Show()
        VerVisitasP.MensajeVisitas2.Text = MensajeProyectos.Text
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim SAVE As New SaveFileDialog
        Dim ruta As String
        Dim xlApp As Object = CreateObject("Excel.Application")
        Dim pth As String = ""
        'crearemos una nueva hoja de calculo
        Dim xlwb As Object = xlApp.WorkBooks.add
        Dim xlws As Object = xlwb.WorkSheets(1)
        Try
            'exportaremos los caracteres de las columnas
            For c As Integer = 0 To DGA.Columns.Count - 1
                xlws.cells(1, c + 1).value = DGA.Columns(c).HeaderText
            Next
            'exportaremos las cabeceras de las calumnas
            For r As Integer = 0 To DGA.RowCount - 1
                For c As Integer = 0 To DGA.Columns.Count - 1
                    xlws.cells(r + 2, c + 1).value = Convert.ToString(DGA.Item(c, r).Value)
                Next
            Next
            'guardamos la hoja de excel en la ruta especifica
            Dim SaveFileDialog1 As SaveFileDialog = New SaveFileDialog
            SaveFileDialog1.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            SaveFileDialog1.Filter = "Archivo Excel| *.xlsx"
            SaveFileDialog1.FilterIndex = 2
            If SaveFileDialog1.ShowDialog = DialogResult.OK Then
                ruta = SaveFileDialog1.FileName
                xlwb.saveas(ruta)
                xlws = Nothing
                xlwb = Nothing
                xlApp.quit()
                MsgBox("Exportado Correctamente", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        CNAP.Show()
        CNAP.MAP.Text = MensajeProyectos.Text
    End Sub

    Private Sub DGA_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles DGA.CellFormatting
        If DGA.Columns(e.ColumnIndex).Name.Equals("Fecha Meta") Then
            If CDate(e.Value) = Hoy Then
                e.CellStyle.BackColor = Color.Red
            End If
            If CDate(e.Value) >= Antes And CDate(e.Value) < Hoy Then
                e.CellStyle.BackColor = Color.Yellow
            End If
        End If
    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        Try
            Dim da = New OdbcDataAdapter("SELECT Job AS 'Job', etapa AS 'Etapa', descripcionetapa AS 'Descrión de la Etapa', responsable AS 'Responsable', Descripcion AS 'Descripción', Cliente AS 'Cliente', Contactos AS 'Contacto' FROM gestióndeproyectossicma.seguimiento WHERE responsable='" & MensajeProyectos.Text & "' AND Prioridad='2-Cotizando' AND Categoria='Proyecto'", cn)
            Dim dt = New DataTable
            da.Fill(dt)
            DGCP.DataSource = dt
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        Dim SAVE As New SaveFileDialog
        Dim ruta As String
        Dim xlApp As Object = CreateObject("Excel.Application")
        Dim pth As String = ""
        'crearemos una nueva hoja de calculo
        Dim xlwb As Object = xlApp.WorkBooks.add
        Dim xlws As Object = xlwb.WorkSheets(1)
        Try
            'exportaremos los caracteres de las columnas
            For c As Integer = 0 To DGCP.Columns.Count - 1
                xlws.cells(1, c + 1).value = DGCP.Columns(c).HeaderText
            Next
            'exportaremos las cabeceras de las calumnas
            For r As Integer = 0 To DGCP.RowCount - 1
                For c As Integer = 0 To DGCP.Columns.Count - 1
                    xlws.cells(r + 2, c + 1).value = Convert.ToString(DGCP.Item(c, r).Value)
                Next
            Next
            'guardamos la hoja de excel en la ruta especifica
            Dim SaveFileDialog1 As SaveFileDialog = New SaveFileDialog
            SaveFileDialog1.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            SaveFileDialog1.Filter = "Archivo Excel| *.xlsx"
            SaveFileDialog1.FilterIndex = 2
            If SaveFileDialog1.ShowDialog = DialogResult.OK Then
                ruta = SaveFileDialog1.FileName
                xlwb.saveas(ruta)
                xlws = Nothing
                xlwb = Nothing
                xlApp.quit()
                MsgBox("Exportado Correctamente", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class