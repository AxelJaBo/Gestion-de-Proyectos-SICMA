﻿Imports System.Data.Odbc
Public Class ReporteBloqueEstacion
    Public jobBE As String
    Public nombreBE As String
    Private Sub ReporteBloqueEstacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim RBE As New ReportedeBloqueEstacion
        RBE.SetParameterValue("job", jobBE)
        RBE.SetParameterValue("nombre", nombreBE)
        CrystalReportViewer1.ReportSource = RBE
    End Sub
End Class