﻿Imports System.Data.Odbc
Public Class ReporteVisitas
    Public FechaI As Date
    Public FechaF As Date
    Private Sub ReporteVisitas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim RV As New ReportedeVisitas
        RV.SetParameterValue("desde", FechaI)
        RV.SetParameterValue("hasta", FechaF)
        CrystalReportViewer1.ReportSource = RV
    End Sub
End Class