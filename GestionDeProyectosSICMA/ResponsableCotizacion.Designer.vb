﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ResponsableCotizacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ResponsableCotizacion))
        Me.Responsable = New System.Windows.Forms.ComboBox()
        Me.Etapa = New System.Windows.Forms.ComboBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Job = New System.Windows.Forms.ComboBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.SeguimientoG = New System.Windows.Forms.Label()
        Me.ContJob = New System.Windows.Forms.TextBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.ClienJob = New System.Windows.Forms.TextBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.DescJob = New System.Windows.Forms.TextBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.GRC = New System.Windows.Forms.Button()
        Me.MRC = New System.Windows.Forms.Button()
        Me.BRC = New System.Windows.Forms.Button()
        Me.DescEtapa = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Responsable
        '
        Me.Responsable.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Responsable.FormattingEnabled = True
        Me.Responsable.Location = New System.Drawing.Point(155, 206)
        Me.Responsable.Name = "Responsable"
        Me.Responsable.Size = New System.Drawing.Size(317, 31)
        Me.Responsable.TabIndex = 93
        '
        'Etapa
        '
        Me.Etapa.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Etapa.FormattingEnabled = True
        Me.Etapa.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14"})
        Me.Etapa.Location = New System.Drawing.Point(155, 83)
        Me.Etapa.Name = "Etapa"
        Me.Etapa.Size = New System.Drawing.Size(317, 31)
        Me.Etapa.TabIndex = 92
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label60.Location = New System.Drawing.Point(10, 214)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(129, 23)
        Me.Label60.TabIndex = 91
        Me.Label60.Text = "Responsable:"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label59.Location = New System.Drawing.Point(12, 92)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(67, 23)
        Me.Label59.TabIndex = 90
        Me.Label59.Text = "Etapa:"
        '
        'Job
        '
        Me.Job.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Job.FormattingEnabled = True
        Me.Job.Location = New System.Drawing.Point(155, 47)
        Me.Job.Name = "Job"
        Me.Job.Size = New System.Drawing.Size(317, 31)
        Me.Job.TabIndex = 125
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Location = New System.Drawing.Point(12, 55)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(91, 23)
        Me.Label36.TabIndex = 124
        Me.Label36.Text = "# de Job:"
        '
        'SeguimientoG
        '
        Me.SeguimientoG.AutoSize = True
        Me.SeguimientoG.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SeguimientoG.ForeColor = System.Drawing.Color.RoyalBlue
        Me.SeguimientoG.Location = New System.Drawing.Point(38, 9)
        Me.SeguimientoG.Name = "SeguimientoG"
        Me.SeguimientoG.Size = New System.Drawing.Size(416, 38)
        Me.SeguimientoG.TabIndex = 126
        Me.SeguimientoG.Text = "Responsable de Cotización"
        '
        'ContJob
        '
        Me.ContJob.Enabled = False
        Me.ContJob.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.ContJob.Location = New System.Drawing.Point(156, 365)
        Me.ContJob.Multiline = True
        Me.ContJob.Name = "ContJob"
        Me.ContJob.Size = New System.Drawing.Size(317, 30)
        Me.ContJob.TabIndex = 131
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label58.Location = New System.Drawing.Point(11, 372)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(96, 23)
        Me.Label58.TabIndex = 132
        Me.Label58.Text = "Contacto:"
        '
        'ClienJob
        '
        Me.ClienJob.Enabled = False
        Me.ClienJob.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.ClienJob.Location = New System.Drawing.Point(156, 329)
        Me.ClienJob.Multiline = True
        Me.ClienJob.Name = "ClienJob"
        Me.ClienJob.Size = New System.Drawing.Size(317, 30)
        Me.ClienJob.TabIndex = 129
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label57.Location = New System.Drawing.Point(12, 336)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(77, 23)
        Me.Label57.TabIndex = 130
        Me.Label57.Text = "Cliente:"
        '
        'DescJob
        '
        Me.DescJob.Enabled = False
        Me.DescJob.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.DescJob.Location = New System.Drawing.Point(156, 243)
        Me.DescJob.Multiline = True
        Me.DescJob.Name = "DescJob"
        Me.DescJob.Size = New System.Drawing.Size(317, 80)
        Me.DescJob.TabIndex = 127
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label56.Location = New System.Drawing.Point(11, 243)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(120, 46)
        Me.Label56.TabIndex = 128
        Me.Label56.Text = "Descripción " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "del Job:"
        '
        'GRC
        '
        Me.GRC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GRC.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GRC.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GRC.Image = CType(resources.GetObject("GRC.Image"), System.Drawing.Image)
        Me.GRC.Location = New System.Drawing.Point(347, 401)
        Me.GRC.Name = "GRC"
        Me.GRC.Size = New System.Drawing.Size(60, 60)
        Me.GRC.TabIndex = 133
        Me.GRC.UseVisualStyleBackColor = True
        '
        'MRC
        '
        Me.MRC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MRC.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MRC.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MRC.Image = CType(resources.GetObject("MRC.Image"), System.Drawing.Image)
        Me.MRC.Location = New System.Drawing.Point(413, 401)
        Me.MRC.Name = "MRC"
        Me.MRC.Size = New System.Drawing.Size(60, 60)
        Me.MRC.TabIndex = 136
        Me.MRC.UseVisualStyleBackColor = True
        '
        'BRC
        '
        Me.BRC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BRC.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BRC.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BRC.Image = CType(resources.GetObject("BRC.Image"), System.Drawing.Image)
        Me.BRC.Location = New System.Drawing.Point(281, 401)
        Me.BRC.Name = "BRC"
        Me.BRC.Size = New System.Drawing.Size(60, 60)
        Me.BRC.TabIndex = 135
        Me.BRC.UseVisualStyleBackColor = True
        '
        'DescEtapa
        '
        Me.DescEtapa.Enabled = False
        Me.DescEtapa.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.DescEtapa.Location = New System.Drawing.Point(155, 120)
        Me.DescEtapa.Multiline = True
        Me.DescEtapa.Name = "DescEtapa"
        Me.DescEtapa.Size = New System.Drawing.Size(317, 80)
        Me.DescEtapa.TabIndex = 137
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Location = New System.Drawing.Point(11, 121)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 46)
        Me.Label1.TabIndex = 138
        Me.Label1.Text = "Descripción " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "de la Etapa:"
        '
        'ResponsableCotizacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(484, 481)
        Me.Controls.Add(Me.DescEtapa)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MRC)
        Me.Controls.Add(Me.BRC)
        Me.Controls.Add(Me.GRC)
        Me.Controls.Add(Me.ContJob)
        Me.Controls.Add(Me.Label58)
        Me.Controls.Add(Me.ClienJob)
        Me.Controls.Add(Me.Label57)
        Me.Controls.Add(Me.DescJob)
        Me.Controls.Add(Me.Label56)
        Me.Controls.Add(Me.SeguimientoG)
        Me.Controls.Add(Me.Job)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.Responsable)
        Me.Controls.Add(Me.Etapa)
        Me.Controls.Add(Me.Label60)
        Me.Controls.Add(Me.Label59)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ResponsableCotizacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Responsable As ComboBox
    Friend WithEvents Etapa As ComboBox
    Friend WithEvents Label60 As Label
    Friend WithEvents Label59 As Label
    Friend WithEvents Job As ComboBox
    Friend WithEvents Label36 As Label
    Friend WithEvents SeguimientoG As Label
    Friend WithEvents ContJob As TextBox
    Friend WithEvents Label58 As Label
    Friend WithEvents ClienJob As TextBox
    Friend WithEvents Label57 As Label
    Friend WithEvents DescJob As TextBox
    Friend WithEvents Label56 As Label
    Friend WithEvents GRC As Button
    Friend WithEvents MRC As Button
    Friend WithEvents BRC As Button
    Friend WithEvents DescEtapa As TextBox
    Friend WithEvents Label1 As Label
End Class
