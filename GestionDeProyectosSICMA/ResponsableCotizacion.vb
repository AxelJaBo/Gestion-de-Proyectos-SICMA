﻿Imports System.Data.Odbc

Public Class ResponsableCotizacion
    Private Sub ResponsableCotizacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Llenar()
        Llenar1()
    End Sub
    Public Sub Llenar()
        Dim daP2 = New OdbcDataAdapter("SELECT Job FROM gestióndeproyectossicma.seguimiento WHERE Prioridad='2-Cotizando' AND Categoria='Proyecto' ORDER BY Job ASC ", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        Job.DataSource = dtP2
        Job.DisplayMember = "Job"
    End Sub
    Public Sub Llenar1()
        Dim daP2 = New OdbcDataAdapter("SELECT idUsuarios, nombre FROM gestióndeproyectossicma.usuarios WHERE Tipo_Usuario='Proyectos' OR Tipo_Usuario='Ventas'", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        Responsable.DataSource = dtP2
        Responsable.DisplayMember = "nombre"
        Responsable.ValueMember = "idUsuarios"
    End Sub

    Public Sub Llenar2()
        If Job.Text = "" Then
            MsgBox("El campo Job se encuentra vacio, es necesario que escriba el número de Job para poder realizar la busqueda.")
        Else
            Dim consultarjob As New OdbcCommand("SELECT Cliente, Contactos, Descripcion FROM gestióndeproyectossicma.seguimiento where Job ='" & Job.Text.ToArray & "'", cn)
            Dim datos As OdbcDataReader
            Try
                datos = consultarjob.ExecuteReader
                If datos.Read Then
                    ClienJob.Text() = datos.Item("Cliente").ToString()
                    ContJob.Text() = datos.Item("Contactos").ToString()
                    DescJob.Text() = datos.Item("Descripcion").ToString()
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub Job_TextChanged(sender As Object, e As EventArgs) Handles Job.TextChanged
        Llenar2()
    End Sub

    Private Sub GRC_Click(sender As Object, e As EventArgs) Handles GRC.Click
        If Job.Text = "" And Etapa.Text = "" Then
            MsgBox("Los campos se encuentran vacios.")
        Else
            If Job.Text = "" Then
                MsgBox("El campo Job se encuentra vacio.")
            Else
                If Etapa.Text = "" Then
                    MsgBox("El campo Etapa se encuentra vacio.")
                Else
                    Dim buscarRC As New OdbcCommand("SELECT job, etapa FROM gestióndeproyectossicma.etapascotizacion WHERE job ='" & Job.Text.ToArray & "'" & "AND etapa= '" & Etapa.Text & "'", cn)
                    Dim datosvis As OdbcDataReader
                    Try
                        datosvis = buscarRC.ExecuteReader
                        If datosvis.Read Then
                            MessageBox.Show("Este Responsable de Cotización ya se encuentra Registrado")
                        Else
                            Try
                                MessageBox.Show("Los datos del Responsable de la Cotización se han guardado Satisfactoriamente.")
                                Dim guardarRC As New OdbcCommand("INSERT INTO  gestióndeproyectossicma.etapascotizacion (job, etapa, responsable, descripcionetapa) VALUES ('" & Job.Text & "','" & Etapa.Text & "','" & Responsable.Text & "','" & DescEtapa.Text & "')", cn)
                                guardarRC.ExecuteNonQuery()
                            Catch ex As Exception
                                MsgBox(ex.Message)
                            End Try
                        End If
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                End If
            End If
        End If
    End Sub

    Private Sub BRC_Click(sender As Object, e As EventArgs) Handles BRC.Click
        If Job.Text = "" And Etapa.Text = "" Then
            MsgBox("Los campos se encuentran vacios.")
        Else
            If Job.Text = "" Then
                MsgBox("El campo Job se encuentra vacio.")
            Else
                If Etapa.Text = "" Then
                    MsgBox("El campo Etapa se encuentra vacio.")
                Else
                    Dim consultar As New OdbcCommand("SELECT job, etapa, responsable, descripcionetapa FROM gestióndeproyectossicma.etapascotizacion WHERE job='" & Job.Text.ToArray & "' AND etapa='" & Etapa.Text.ToArray & "'", cn)
                    Dim datos As OdbcDataReader
                    Try
                        datos = consultar.ExecuteReader
                        If datos.Read Then
                            Job.Text() = datos.Item("job").ToString()
                            Etapa.Text() = datos.Item("etapa").ToString()
                            Responsable.Text() = datos.Item("responsable").ToString()
                            DescEtapa.Text() = datos.Item("descripcionetapa").ToString()
                        Else
                            MessageBox.Show("Este Responsable de Cotización no se encuentra Registrada.")
                        End If
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                End If
            End If
        End If
    End Sub

    Private Sub MRC_Click(sender As Object, e As EventArgs) Handles MRC.Click
        Dim Msg As MsgBoxResult
        Msg = MsgBox("¿Esta seguro que desea Actualizar este Responsable?", vbYesNo, "Actualizar!!")
        If Msg = MsgBoxResult.Yes Then
            If Job.Text = "" And Etapa.Text = "" Then
                MsgBox("Los campos se encuentran vacios.")
            Else
                If Job.Text = "" Then
                    MsgBox("El campo Job se encuentra vacio.")
                Else
                    If Etapa.Text = "" Then
                        MsgBox("El campo Etapa se encuentra vacio.")
                    Else
                        Dim consultarRE As New OdbcCommand("SELECT job, etapa, responsable FROM gestióndeproyectossicma.etapascotizacion WHERE job='" & Job.Text.ToArray & "' AND etapa='" & Etapa.Text.ToArray & "'", cn)
                        Dim datosVX As OdbcDataReader
                        Try
                            datosVX = consultarRE.ExecuteReader
                            If datosVX.Read Then
                                MessageBox.Show("Este Responable de Cotización se ha Modificado Correctamente.")
                                Try
                                    Dim modificarVX As New OdbcCommand("UPDATE gestióndeproyectossicma.etapascotizacion SET responsable='" & Responsable.Text.ToArray & "', " & " descripcionetapa='" & DescEtapa.Text.ToArray & "' WHERE job='" & Job.Text.ToArray & "' AND etapa='" & Etapa.Text.ToArray & "'", cn)
                                    modificarVX.ExecuteNonQuery()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            Else
                                MessageBox.Show("Este Responsable de Cotización no se encuentra Registrada.")
                            End If
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    End If
                End If
            End If
        Else
            MessageBox.Show("Se canceló la Actualización.")
        End If
    End Sub

    Private Sub Etapa_TextChanged(sender As Object, e As EventArgs) Handles Etapa.TextChanged
        If Etapa.Text = "1" Then
            DescEtapa.Text = "RESPONDER AL CLIENTE"
        End If
        If Etapa.Text = "2" Then
            DescEtapa.Text = "JUNTA DE SEGUIMIENTO"
        End If
        If Etapa.Text = "3" Then
            DescEtapa.Text = "SKETCH PRELIMINAR"
        End If
        If Etapa.Text = "4" Then
            DescEtapa.Text = "BOM PRELIMINAR"
        End If
        If Etapa.Text = "5" Then
            DescEtapa.Text = "COTIZAR BOM PRELIMINAR"
        End If
        If Etapa.Text = "6" Then
            DescEtapa.Text = "LABOR COST"
        End If
        If Etapa.Text = "7" Then
            DescEtapa.Text = "QUOTE EXTERNAL JOB"
        End If
        If Etapa.Text = "8" Then
            DescEtapa.Text = "PRECIO DE VENTA"
        End If
        If Etapa.Text = "9" Then
            DescEtapa.Text = "CARACTERISTICAS TECNICAS"
        End If
        If Etapa.Text = "10" Then
            DescEtapa.Text = "TERMINOS Y CONDICIONES"
        End If
        If Etapa.Text = "11" Then
            DescEtapa.Text = "GENERAR COTIZACION FORMAL"
        End If
        If Etapa.Text = "12" Then
            DescEtapa.Text = "REVISION FINAL"
        End If
        If Etapa.Text = "13" Then
            DescEtapa.Text = "ENVIAR COTIZACION AL CLIENTE"
        End If
        If Etapa.Text = "14" Then
            DescEtapa.Text = "CONFIRMAR DE RECIBIDO Y PRIMERAS IMPRESIONES"
        End If
    End Sub
End Class