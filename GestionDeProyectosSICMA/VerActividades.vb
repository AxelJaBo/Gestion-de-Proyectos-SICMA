﻿Imports System.Data.Odbc

Public Class VerActividades
    Dim Hoy As Date = DateTime.Now.ToString("yyyy/MM/dd")
    Dim Antes As Date = DateTime.Now.AddDays(-5).ToString("yyyy/MM/dd")
    Private Sub VerActividades_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call conectar()
        Try
            Dim davis2 = New OdbcDataAdapter("SELECT actividades.Cod_act AS 'Código', actividades.Job AS 'Job', seguimiento.Descripcion AS 'Descripción de Job', actividades.Fecha AS 'Fecha de Actividad', actividades.Fecha_Meta AS 'Fecha Meta', actividades.Status AS 'Estatus', actividades.Estado AS 'Estado', actividades.Nombre AS 'Nombre', seguimiento.Cliente AS 'Empresa', actividades.Titulo AS 'Título', actividades.Descripcion AS 'Descripción', actividades.Comentario AS 'Comentario', actividades.Nota AS 'Nota'
FROM gestióndeproyectossicma.actividades INNER JOIN gestióndeproyectossicma.seguimiento ON actividades.Job = seguimiento.Job", cn)
            Dim dtvis2 = New DataTable
            davis2.Fill(dtvis2)
            DGA.DataSource = dtvis2
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Function busquedaxletraA(ByVal busquedaA As String) As DataTable
        Dim dt As New DataTable
        Dim da As New OdbcDataAdapter("SELECT actividades.Cod_act AS 'Código', actividades.Job AS 'Job', seguimiento.Descripcion AS 'Descripción de Job', actividades.Fecha AS 'Fecha de Actividad', actividades.Fecha_Meta AS 'Fecha Meta', actividades.Status AS 'Estatus', actividades.Estado AS 'Estado', actividades.Nombre AS 'Nombre', seguimiento.Cliente AS 'Empresa', actividades.Titulo AS 'Título', actividades.Descripcion AS 'Descripción', actividades.Comentario AS 'Comentario', actividades.Nota AS 'Nota'
FROM gestióndeproyectossicma.actividades INNER JOIN gestióndeproyectossicma.seguimiento ON actividades.Job = seguimiento.Job WHERE Cod_act LIKE '%" & busquedaA & "%'", cn)
        da.Fill(dt)
        Return dt
    End Function

    Private Sub BusquedaLetraA_TextChanged(sender As Object, e As EventArgs) Handles BusquedaLetraA.TextChanged
        If busquedaxletraA(BusquedaLetraA.Text).Rows.Count > 0 Then
            DGA.DataSource = busquedaxletraA(BusquedaLetraA.Text)
        End If
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Try
            Dim davis2 = New OdbcDataAdapter("SELECT actividades.Cod_act AS 'Código', actividades.Job AS 'Job', seguimiento.Descripcion AS 'Descripción de Job', actividades.Fecha AS 'Fecha de Actividad', actividades.Fecha_Meta AS 'Fecha Meta', actividades.Status AS 'Estatus', actividades.Estado AS 'Estado', actividades.Nombre AS 'Nombre', seguimiento.Cliente AS 'Empresa', actividades.Titulo AS 'Título', actividades.Descripcion AS 'Descripción', actividades.Comentario AS 'Comentario', actividades.Nota AS 'Nota'
FROM gestióndeproyectossicma.actividades INNER JOIN gestióndeproyectossicma.seguimiento ON actividades.Job = seguimiento.Job", cn)
            Dim dtvis2 = New DataTable
            davis2.Fill(dtvis2)
            DGA.DataSource = dtvis2
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim SAVE As New SaveFileDialog
        Dim ruta As String
        Dim xlApp As Object = CreateObject("Excel.Application")
        Dim pth As String = ""
        'crearemos una nueva hoja de calculo
        Dim xlwb As Object = xlApp.WorkBooks.add
        Dim xlws As Object = xlwb.WorkSheets(1)
        Try
            'exportaremos los caracteres de las columnas
            For c As Integer = 0 To DGA.Columns.Count - 1
                xlws.cells(1, c + 1).value = DGA.Columns(c).HeaderText
            Next
            'exportaremos las cabeceras de las calumnas
            For r As Integer = 0 To DGA.RowCount - 1
                For c As Integer = 0 To DGA.Columns.Count - 1
                    xlws.cells(r + 2, c + 1).value = Convert.ToString(DGA.Item(c, r).Value)
                Next
            Next
            'guardamos la hoja de excel en la ruta especifica
            Dim SaveFileDialog1 As SaveFileDialog = New SaveFileDialog
            SaveFileDialog1.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            SaveFileDialog1.Filter = "Archivo Excel| *.xlsx"
            SaveFileDialog1.FilterIndex = 2
            If SaveFileDialog1.ShowDialog = DialogResult.OK Then
                ruta = SaveFileDialog1.FileName
                xlwb.saveas(ruta)
                xlws = Nothing
                xlwb = Nothing
                xlApp.quit()
                MsgBox("Exportado Correctamente", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub DGA_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles DGA.CellFormatting
        If DGA.Columns(e.ColumnIndex).Name.Equals("Fecha Meta") Then
            If CDate(e.Value) = Hoy Then
                e.CellStyle.BackColor = Color.Red
            End If
            If CDate(e.Value) >= Antes And CDate(e.Value) < Hoy Then
                e.CellStyle.BackColor = Color.Yellow
            End If
        End If
    End Sub
End Class