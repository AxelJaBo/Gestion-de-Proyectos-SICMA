﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VerActividadesPendientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(VerActividadesPendientes))
        Me.Button5 = New System.Windows.Forms.Button()
        Me.MU = New System.Windows.Forms.Button()
        Me.Status = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.codigo = New System.Windows.Forms.ComboBox()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.BusquedaLetraA = New System.Windows.Forms.TextBox()
        Me.DGA = New System.Windows.Forms.DataGridView()
        Me.MensajeAP = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        CType(Me.DGA, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button5.Image = CType(resources.GetObject("Button5.Image"), System.Drawing.Image)
        Me.Button5.Location = New System.Drawing.Point(929, 6)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(60, 60)
        Me.Button5.TabIndex = 117
        Me.Button5.UseVisualStyleBackColor = True
        '
        'MU
        '
        Me.MU.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MU.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MU.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MU.Image = CType(resources.GetObject("MU.Image"), System.Drawing.Image)
        Me.MU.Location = New System.Drawing.Point(996, 6)
        Me.MU.Name = "MU"
        Me.MU.Size = New System.Drawing.Size(60, 60)
        Me.MU.TabIndex = 116
        Me.MU.UseVisualStyleBackColor = True
        '
        'Status
        '
        Me.Status.FormattingEnabled = True
        Me.Status.Items.AddRange(New Object() {"En Proceso", "Terminada"})
        Me.Status.Location = New System.Drawing.Point(774, 45)
        Me.Status.Name = "Status"
        Me.Status.Size = New System.Drawing.Size(149, 21)
        Me.Status.TabIndex = 115
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.Location = New System.Drawing.Point(695, 45)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(73, 23)
        Me.Label6.TabIndex = 114
        Me.Label6.Text = "Status:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.Location = New System.Drawing.Point(486, 44)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 23)
        Me.Label5.TabIndex = 113
        Me.Label5.Text = "Código:"
        '
        'codigo
        '
        Me.codigo.FormattingEnabled = True
        Me.codigo.Location = New System.Drawing.Point(568, 45)
        Me.codigo.Name = "codigo"
        Me.codigo.Size = New System.Drawing.Size(121, 21)
        Me.codigo.TabIndex = 112
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Black
        Me.Button8.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button8.Location = New System.Drawing.Point(1062, 6)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(110, 60)
        Me.Button8.TabIndex = 111
        Me.Button8.Text = "Actualizar"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial Black", 20.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Location = New System.Drawing.Point(370, -1)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(192, 38)
        Me.Label4.TabIndex = 110
        Me.Label4.Text = "Actividades"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.Location = New System.Drawing.Point(15, 45)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(177, 23)
        Me.Label3.TabIndex = 109
        Me.Label3.Text = "Busqueda General:"
        '
        'BusquedaLetraA
        '
        Me.BusquedaLetraA.Location = New System.Drawing.Point(200, 45)
        Me.BusquedaLetraA.Name = "BusquedaLetraA"
        Me.BusquedaLetraA.Size = New System.Drawing.Size(200, 20)
        Me.BusquedaLetraA.TabIndex = 108
        '
        'DGA
        '
        Me.DGA.AllowUserToAddRows = False
        Me.DGA.AllowUserToDeleteRows = False
        Me.DGA.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGA.Location = New System.Drawing.Point(19, 72)
        Me.DGA.Name = "DGA"
        Me.DGA.ReadOnly = True
        Me.DGA.RowHeadersVisible = False
        Me.DGA.Size = New System.Drawing.Size(1153, 577)
        Me.DGA.TabIndex = 107
        '
        'MensajeAP
        '
        Me.MensajeAP.AutoSize = True
        Me.MensajeAP.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensajeAP.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MensajeAP.Location = New System.Drawing.Point(12, -1)
        Me.MensajeAP.Name = "MensajeAP"
        Me.MensajeAP.Size = New System.Drawing.Size(0, 38)
        Me.MensajeAP.TabIndex = 118
        Me.MensajeAP.Visible = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Black
        Me.Button1.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button1.Location = New System.Drawing.Point(811, 6)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(112, 33)
        Me.Button1.TabIndex = 119
        Me.Button1.Text = "Exportar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Black
        Me.Button2.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button2.Location = New System.Drawing.Point(596, 6)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(209, 33)
        Me.Button2.TabIndex = 120
        Me.Button2.Text = "Notas y Comentarios"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'VerActividadesPendientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1184, 661)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.MensajeAP)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.MU)
        Me.Controls.Add(Me.Status)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.codigo)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.BusquedaLetraA)
        Me.Controls.Add(Me.DGA)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "VerActividadesPendientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.DGA, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button5 As Button
    Friend WithEvents MU As Button
    Friend WithEvents Status As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents codigo As ComboBox
    Friend WithEvents Button8 As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents BusquedaLetraA As TextBox
    Friend WithEvents DGA As DataGridView
    Public WithEvents MensajeAP As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
End Class
