﻿Imports System.Data.Odbc

Public Class VerActividadesPendientes
    Dim Hoy As Date = DateTime.Now.ToString("yyyy/MM/dd")
    Dim Antes As Date = DateTime.Now.AddDays(-5).ToString("yyyy/MM/dd")
    Private Sub VerActividadesPendientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Llenar2()
    End Sub

    Public Sub Llenar2()
        Dim dax = New OdbcDataAdapter("SELECT Cod_act FROM gestióndeproyectossicma.actividades WHERE (Nombre='" & MensajeAP.Text.ToString & "' AND Status='En Proceso') OR (Nombre='" & MensajeAP.Text.ToString & "' AND Status='Sin Comenzar') ORDER BY idactividades ASC", cn)
        Dim dtx = New DataTable
        dax.Fill(dtx)
        codigo.DataSource = dtx
        codigo.DisplayMember = "Cod_act"
    End Sub
    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If codigo.Text = "" Then
            MsgBox("El campo Código se encuentra vacio.")
        Else
            Dim consultaractividad As New OdbcCommand("SELECT Status FROM gestióndeproyectossicma.actividades WHERE Cod_act='" & codigo.Text.ToArray & "'", cn)
            Dim datosactividad As OdbcDataReader
            Try
                datosactividad = consultaractividad.ExecuteReader
                If datosactividad.Read Then
                    Status.Text() = datosactividad.Item("Status").ToString()
                Else
                    MessageBox.Show("Esta Actividad no se encuentra Registrada.")
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub MU_Click(sender As Object, e As EventArgs) Handles MU.Click
        If codigo.Text = "" And Status.Text = "" Then
            MsgBox("Los campos se encuentran vacios.")
        Else
            If codigo.Text = "" Then
                MsgBox("El campo Código se encuentra vacio.")
            Else
                If Status.Text = "" Then
                    MsgBox("El campo Status se encuentra vacio.")
                Else
                    Dim consultaractividad3 As New OdbcCommand("SELECT Cod_act FROM gestióndeproyectossicma.actividades WHERE Cod_act ='" & codigo.Text.ToArray & "'", cn)
                    Dim datosactividad3 As OdbcDataReader
                    Try
                        datosactividad3 = consultaractividad3.ExecuteReader
                        If datosactividad3.Read Then
                            MessageBox.Show("Esta Actividad se ha Modificado Correctamente.")
                            Try
                                Dim modificarVX As New OdbcCommand("UPDATE gestióndeproyectossicma.actividades SET Status='" & Status.Text.ToArray & "' WHERE Cod_act ='" & codigo.Text.ToArray & "'", cn)
                                modificarVX.ExecuteNonQuery()
                            Catch ex As Exception
                                MsgBox(ex.Message)
                            End Try
                        Else
                            MessageBox.Show("Esta Actividad no se encuentra Registrada.")
                        End If
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                    Llenar2()
                    Status.Text = "En Proceso"
                End If
            End If
        End If
        Try
            Dim daac = New OdbcDataAdapter("SELECT actividades.Cod_act AS 'Código', actividades.Job AS 'Job', actividades.Fecha AS 'Fecha de Actividad', actividades.Fecha_Meta AS 'Fecha Meta', actividades.Nombre AS 'Nombre', seguimiento.Cliente AS 'Empresa', seguimiento.Contactos AS 'Contacto', actividades.Titulo AS 'Título', actividades.Descripcion AS 'Descripción', actividades.Comentario AS 'Comentario', actividades.Nota AS 'Nota', actividades.Status AS 'Status'
FROM gestióndeproyectossicma.actividades INNER JOIN gestióndeproyectossicma.seguimiento ON actividades.Job = seguimiento.Job WHERE (actividades.Nombre='" & MensajeAP.Text.ToString & "' AND actividades.Status='En Proceso') OR (actividades.Nombre='" & MensajeAP.Text.ToString & "' AND actividades.Status='Sin Comenzar')", cn)
            Dim dtac = New DataTable
            daac.Fill(dtac)
            DGA.DataSource = dtac
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Llenar2()
        Try
            Dim daac = New OdbcDataAdapter("SELECT actividades.Cod_act AS 'Código', actividades.Job AS 'Job', actividades.Fecha AS 'Fecha de Actividad', actividades.Fecha_Meta AS 'Fecha Meta', actividades.Nombre AS 'Nombre', seguimiento.Cliente AS 'Empresa', seguimiento.Contactos AS 'Contacto', actividades.Titulo AS 'Título', actividades.Descripcion AS 'Descripción', actividades.Comentario AS 'Comentario', actividades.Nota AS 'Nota', actividades.Status AS 'Status'
FROM gestióndeproyectossicma.actividades INNER JOIN gestióndeproyectossicma.seguimiento ON actividades.Job = seguimiento.Job WHERE (actividades.Nombre='" & MensajeAP.Text.ToString & "' AND actividades.Status='En Proceso') OR (actividades.Nombre='" & MensajeAP.Text.ToString & "' AND actividades.Status='Sin Comenzar')", cn)
            Dim dtac = New DataTable
            daac.Fill(dtac)
            DGA.DataSource = dtac
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Function busquedaxletraA(ByVal busquedaA As String) As DataTable
        Dim dt As New DataTable
        Dim da As New OdbcDataAdapter("SELECT actividades.Cod_act AS 'Código', actividades.Job AS 'Job', actividades.Fecha AS 'Fecha de Actividad', actividades.Fecha_Meta AS 'Fecha Meta', actividades.Nombre AS 'Nombre', seguimiento.Cliente AS 'Empresa', seguimiento.Contactos AS 'Contacto', actividades.Titulo AS 'Título', actividades.Descripcion AS 'Descripción', actividades.Comentario AS 'Comentario', actividades.Nota AS 'Nota', actividades.Status AS 'Status'
FROM gestióndeproyectossicma.actividades INNER JOIN gestióndeproyectossicma.seguimiento ON actividades.Job = seguimiento.Job WHERE actividades.Cod_act LIKE '%" & busquedaA & "%' " & "AND ((actividades.Nombre='" & MensajeAP.Text.ToArray & "' AND actividades.Status='Sin Comenzar') OR (actividades.Nombre='" & MensajeAP.Text.ToArray & "' AND actividades.Status='En Proceso'))", cn)
        da.Fill(dt)
        Return dt
    End Function

    Private Sub BusquedaLetraA_TextChanged(sender As Object, e As EventArgs) Handles BusquedaLetraA.TextChanged
        If busquedaxletraA(BusquedaLetraA.Text).Rows.Count > 0 Then
            DGA.DataSource = busquedaxletraA(BusquedaLetraA.Text)
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim SAVE As New SaveFileDialog
        Dim ruta As String
        Dim xlApp As Object = CreateObject("Excel.Application")
        Dim pth As String = ""
        'crearemos una nueva hoja de calculo
        Dim xlwb As Object = xlApp.WorkBooks.add
        Dim xlws As Object = xlwb.WorkSheets(1)
        Try
            'exportaremos los caracteres de las columnas
            For c As Integer = 0 To DGA.Columns.Count - 1
                xlws.cells(1, c + 1).value = DGA.Columns(c).HeaderText
            Next
            'exportaremos las cabeceras de las calumnas
            For r As Integer = 0 To DGA.RowCount - 1
                For c As Integer = 0 To DGA.Columns.Count - 1
                    xlws.cells(r + 2, c + 1).value = Convert.ToString(DGA.Item(c, r).Value)
                Next
            Next
            'guardamos la hoja de excel en la ruta especifica
            Dim SaveFileDialog1 As SaveFileDialog = New SaveFileDialog
            SaveFileDialog1.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            SaveFileDialog1.Filter = "Archivo Excel| *.xlsx"
            SaveFileDialog1.FilterIndex = 2
            If SaveFileDialog1.ShowDialog = DialogResult.OK Then
                ruta = SaveFileDialog1.FileName
                xlwb.saveas(ruta)
                xlws = Nothing
                xlwb = Nothing
                xlApp.quit()
                MsgBox("Exportado Correctamente", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        CNAV.Show()
        CNAV.MAV.Text = MensajeAP.Text
    End Sub

    Private Sub DGA_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles DGA.CellFormatting
        If DGA.Columns(e.ColumnIndex).Name.Equals("Fecha Meta") Then
            If CDate(e.Value) = Hoy Then
                e.CellStyle.BackColor = Color.Red
            End If
            If CDate(e.Value) >= Antes And CDate(e.Value) < Hoy Then
                e.CellStyle.BackColor = Color.Yellow
            End If
        End If
    End Sub
End Class