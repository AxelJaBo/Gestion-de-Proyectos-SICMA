﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class VerBloqueEstacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(VerBloqueEstacion))
        Me.BE = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.NBBE = New System.Windows.Forms.ComboBox()
        Me.DGBE = New System.Windows.Forms.DataGridView()
        Me.JBE = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MBE = New System.Windows.Forms.Button()
        CType(Me.DGBE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BE
        '
        Me.BE.AutoSize = True
        Me.BE.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BE.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE.Location = New System.Drawing.Point(462, 7)
        Me.BE.Name = "BE"
        Me.BE.Size = New System.Drawing.Size(258, 38)
        Me.BE.TabIndex = 255
        Me.BE.Text = "Bloque Estación"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label41.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label41.Location = New System.Drawing.Point(380, 56)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(182, 23)
        Me.Label41.TabIndex = 424
        Me.Label41.Text = "Nombre del Bloque:"
        '
        'NBBE
        '
        Me.NBBE.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.NBBE.FormattingEnabled = True
        Me.NBBE.Location = New System.Drawing.Point(568, 48)
        Me.NBBE.Name = "NBBE"
        Me.NBBE.Size = New System.Drawing.Size(300, 31)
        Me.NBBE.TabIndex = 425
        '
        'DGBE
        '
        Me.DGBE.AllowUserToAddRows = False
        Me.DGBE.AllowUserToDeleteRows = False
        Me.DGBE.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGBE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGBE.Location = New System.Drawing.Point(12, 85)
        Me.DGBE.Name = "DGBE"
        Me.DGBE.Size = New System.Drawing.Size(1153, 514)
        Me.DGBE.TabIndex = 433
        '
        'JBE
        '
        Me.JBE.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.JBE.FormattingEnabled = True
        Me.JBE.Location = New System.Drawing.Point(62, 48)
        Me.JBE.Name = "JBE"
        Me.JBE.Size = New System.Drawing.Size(300, 31)
        Me.JBE.TabIndex = 435
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Location = New System.Drawing.Point(8, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 23)
        Me.Label1.TabIndex = 434
        Me.Label1.Text = "Job:"
        '
        'MBE
        '
        Me.MBE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MBE.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MBE.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MBE.Image = CType(resources.GetObject("MBE.Image"), System.Drawing.Image)
        Me.MBE.Location = New System.Drawing.Point(1105, 7)
        Me.MBE.Name = "MBE"
        Me.MBE.Size = New System.Drawing.Size(60, 60)
        Me.MBE.TabIndex = 436
        Me.MBE.UseVisualStyleBackColor = True
        '
        'VerBloqueEstacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1184, 611)
        Me.Controls.Add(Me.MBE)
        Me.Controls.Add(Me.JBE)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DGBE)
        Me.Controls.Add(Me.NBBE)
        Me.Controls.Add(Me.Label41)
        Me.Controls.Add(Me.BE)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "VerBloqueEstacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.DGBE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BE As Label
    Friend WithEvents Label41 As Label
    Friend WithEvents NBBE As ComboBox
    Friend WithEvents DGBE As DataGridView
    Friend WithEvents JBE As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents MBE As Button
End Class
