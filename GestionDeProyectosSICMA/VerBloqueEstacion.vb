﻿Imports System.Data.Odbc

Public Class VerBloqueEstacion
    Private Sub VerBloqueEstacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MsgBox("Estatus: Por default es SIN COMENZAR; Si tiene Fecha de Inicio Real es COMENZADA; Si tiene Fecha Fin Real es TERMINADA.")
        MsgBox("IMPORTANTE: Cada que se modifique algo de la tabla es necesario guardar, por que si se modifican varias cosa de la tabla no se guarda bien.")
        Llenar0()
        Llenar1()
    End Sub
    Public Sub Llenar0()
        Dim daP2 = New OdbcDataAdapter("SELECT DISTINCT job FROM gestióndeproyectossicma.bloqueestacion ORDER BY job ASC", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        JBE.DataSource = dtP2
        JBE.DisplayMember = "job"
    End Sub
    Public Sub Llenar1()
        Dim da = New OdbcDataAdapter("SELECT DISTINCT nombre FROM gestióndeproyectossicma.bloqueestacion WHERE job ='" & JBE.Text & "' ORDER BY nombre ASC", cn)
        Dim dt = New DataTable
        da.Fill(dt)
        NBBE.DataSource = dt
        NBBE.DisplayMember = "nombre"
    End Sub

    Public Sub Llenar2()
        Try
            Dim davis2 = New OdbcDataAdapter("SELECT idbloqueestacion AS 'ID', actividad AS 'Actividad', horas AS 'Horas', fechaI AS 'Fecha Inicio', fechaF AS 'Fecha Fin', fechaIR AS 'Fecha Inicio Real', fechaFR AS 'Fecha Fin Real', estatus AS 'Estatus', comentario AS 'Comentario' FROM gestióndeproyectossicma.bloqueestacion WHERE job= '" & JBE.Text.ToArray & "' " & "AND nombre='" & NBBE.Text.ToArray & "'", cn)
            Dim dtvis2 = New DataTable
            davis2.Fill(dtvis2)
            DGBE.DataSource = dtvis2
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub JBE_TextChanged(sender As Object, e As EventArgs) Handles JBE.TextChanged
        Llenar1()
    End Sub

    Private Sub NBBE_TextChanged(sender As Object, e As EventArgs) Handles NBBE.TextChanged
        Llenar2()
    End Sub

    Private Sub MBE_Click(sender As Object, e As EventArgs) Handles MBE.Click
        Dim row, id As Integer
        Dim FechaIR, FechaFR, estatus, comentario As String
        row = DGBE.CurrentRow.Index
        id = DGBE(0, row).Value
        FechaIR = Convert.ToString(DGBE(5, row).Value)
        FechaFR = Convert.ToString(DGBE(6, row).Value)
        estatus = Convert.ToString(DGBE(7, row).Value)
        comentario = Convert.ToString(DGBE(8, row).Value)
        MessageBox.Show("Los datos se han Modificado Correctamente.")
        Try
            Dim modificarjob As New OdbcCommand("UPDATE gestióndeproyectossicma.bloqueestacion SET fechaIR= '" & FechaIR & "', fechaFR='" & FechaFR & "', estatus='" & estatus & "', comentario='" & comentario & "' WHERE idbloqueestacion='" & id & "'", cn)
            modificarjob.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Llenar2()
    End Sub
End Class