﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class VerBloqueGeneral
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(VerBloqueGeneral))
        Me.BE = New System.Windows.Forms.Label()
        Me.MBG = New System.Windows.Forms.Button()
        Me.JBG = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DGBG = New System.Windows.Forms.DataGridView()
        CType(Me.DGBG, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BE
        '
        Me.BE.AutoSize = True
        Me.BE.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BE.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BE.Location = New System.Drawing.Point(485, 9)
        Me.BE.Name = "BE"
        Me.BE.Size = New System.Drawing.Size(243, 38)
        Me.BE.TabIndex = 427
        Me.BE.Text = "Bloque General"
        '
        'MBG
        '
        Me.MBG.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MBG.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MBG.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MBG.Image = CType(resources.GetObject("MBG.Image"), System.Drawing.Image)
        Me.MBG.Location = New System.Drawing.Point(1112, 9)
        Me.MBG.Name = "MBG"
        Me.MBG.Size = New System.Drawing.Size(60, 60)
        Me.MBG.TabIndex = 433
        Me.MBG.UseVisualStyleBackColor = True
        '
        'JBG
        '
        Me.JBG.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.JBG.FormattingEnabled = True
        Me.JBG.Location = New System.Drawing.Point(68, 50)
        Me.JBG.Name = "JBG"
        Me.JBG.Size = New System.Drawing.Size(300, 31)
        Me.JBG.TabIndex = 437
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Location = New System.Drawing.Point(14, 58)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 23)
        Me.Label1.TabIndex = 436
        Me.Label1.Text = "Job:"
        '
        'DGBG
        '
        Me.DGBG.AllowUserToAddRows = False
        Me.DGBG.AllowUserToDeleteRows = False
        Me.DGBG.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGBG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGBG.Location = New System.Drawing.Point(18, 87)
        Me.DGBG.Name = "DGBG"
        Me.DGBG.Size = New System.Drawing.Size(1153, 514)
        Me.DGBG.TabIndex = 438
        '
        'VerBloqueGeneral
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1184, 611)
        Me.Controls.Add(Me.DGBG)
        Me.Controls.Add(Me.JBG)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MBG)
        Me.Controls.Add(Me.BE)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "VerBloqueGeneral"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.DGBG, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BE As Label
    Friend WithEvents MBG As Button
    Friend WithEvents JBG As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents DGBG As DataGridView
End Class
