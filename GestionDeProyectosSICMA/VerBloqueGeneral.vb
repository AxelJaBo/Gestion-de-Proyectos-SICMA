﻿Imports System.Data.Odbc

Public Class VerBloqueGeneral
    Private Sub VerBloqueGeneral_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MsgBox("Estatus: Por default es SIN COMENZAR; Si tiene Fecha de Inicio Real es COMENZADA; Si tiene Fecha Fin Real es TERMINADA.")
        MsgBox("IMPORTANTE: Cada que se modifique algo de la tabla es necesario guardar, por que si se modifican varias cosa de la tabla no se guarda bien.")
        Llenar1()
    End Sub

    Public Sub Llenar0()
        Try
            Dim davis2 = New OdbcDataAdapter("SELECT idbloquegeneral AS 'ID', actividad AS 'Actividad', horasestimadas AS 'Horas', fechaI AS 'Fecha Inicio', fechaF AS 'Fecha Fin', fechaIR AS 'Fecha Inicio Real', fechaFR AS 'Fecha Fin Real', estatus AS 'Estatus', comentarios AS 'Comentarios' FROM gestióndeproyectossicma.bloquegeneral WHERE job= '" & JBG.Text.ToArray & "'", cn)
            Dim dtvis2 = New DataTable
            davis2.Fill(dtvis2)
            DGBG.DataSource = dtvis2
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub Llenar1()
        Dim daP2 = New OdbcDataAdapter("SELECT DISTINCT job FROM gestióndeproyectossicma.bloquegeneral ORDER BY job ASC", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        JBG.DataSource = dtP2
        JBG.DisplayMember = "job"
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs)
        Llenar0()
    End Sub

    Private Sub MDG_Click(sender As Object, e As EventArgs) Handles MBG.Click
        Dim row, id As Integer
        Dim FechaIR, FechaFR, estatus, comentarios As String
        row = DGBG.CurrentRow.Index
        id = DGBG(0, row).Value
        FechaIR = Convert.ToString(DGBG(5, row).Value)
        FechaFR = Convert.ToString(DGBG(6, row).Value)
        estatus = Convert.ToString(DGBG(7, row).Value)
        comentarios = Convert.ToString(DGBG(8, row).Value)
        MessageBox.Show("Los datos se han Modificado Correctamente.")
        Try
            Dim modificarjob As New OdbcCommand("UPDATE gestióndeproyectossicma.bloquegeneral SET fechaIR='" & FechaIR & "', fechaFR='" & FechaFR & "', estatus='" & estatus & "', comentarios='" & comentarios & "' WHERE idbloquegeneral='" & id & "'", cn)
            modificarjob.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Llenar0()
    End Sub

    Private Sub JBG_TextChanged(sender As Object, e As EventArgs) Handles JBG.TextChanged
        Llenar0()
    End Sub
End Class