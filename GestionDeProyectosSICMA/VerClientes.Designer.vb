﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VerClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(VerClientes))
        Me.DGC = New System.Windows.Forms.DataGridView()
        Me.BGen = New System.Windows.Forms.Label()
        Me.BusquedaLetra = New System.Windows.Forms.TextBox()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Clientes = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.DGC, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGC
        '
        Me.DGC.AllowUserToAddRows = False
        Me.DGC.AllowUserToDeleteRows = False
        Me.DGC.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGC.Location = New System.Drawing.Point(16, 89)
        Me.DGC.Name = "DGC"
        Me.DGC.ReadOnly = True
        Me.DGC.RowHeadersVisible = False
        Me.DGC.Size = New System.Drawing.Size(1156, 560)
        Me.DGC.TabIndex = 46
        '
        'BGen
        '
        Me.BGen.AutoSize = True
        Me.BGen.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BGen.ForeColor = System.Drawing.Color.RoyalBlue
        Me.BGen.Location = New System.Drawing.Point(12, 53)
        Me.BGen.Name = "BGen"
        Me.BGen.Size = New System.Drawing.Size(204, 23)
        Me.BGen.TabIndex = 45
        Me.BGen.Text = "Busqueda por Cliente:"
        '
        'BusquedaLetra
        '
        Me.BusquedaLetra.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BusquedaLetra.Location = New System.Drawing.Point(221, 53)
        Me.BusquedaLetra.Name = "BusquedaLetra"
        Me.BusquedaLetra.Size = New System.Drawing.Size(250, 30)
        Me.BusquedaLetra.TabIndex = 44
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Black
        Me.Button8.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button8.Location = New System.Drawing.Point(1062, 12)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(110, 60)
        Me.Button8.TabIndex = 94
        Me.Button8.Text = "Actualizar"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Clientes
        '
        Me.Clientes.AutoSize = True
        Me.Clientes.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clientes.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Clientes.Location = New System.Drawing.Point(447, 12)
        Me.Clientes.Name = "Clientes"
        Me.Clientes.Size = New System.Drawing.Size(139, 38)
        Me.Clientes.TabIndex = 93
        Me.Clientes.Text = "Clientes"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Black
        Me.Button1.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button1.Location = New System.Drawing.Point(944, 12)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(112, 33)
        Me.Button1.TabIndex = 104
        Me.Button1.Text = "Exportar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'VerClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1184, 661)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Clientes)
        Me.Controls.Add(Me.DGC)
        Me.Controls.Add(Me.BGen)
        Me.Controls.Add(Me.BusquedaLetra)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "VerClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.DGC, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DGC As DataGridView
    Friend WithEvents BGen As Label
    Friend WithEvents BusquedaLetra As TextBox
    Friend WithEvents Button8 As Button
    Friend WithEvents Clientes As Label
    Friend WithEvents Button1 As Button
End Class
