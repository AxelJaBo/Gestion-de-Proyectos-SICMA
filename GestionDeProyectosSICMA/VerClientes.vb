﻿Imports System.Data.Odbc

Public Class VerClientes
    Private Sub VerClientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call conectar()
        Try
            Dim da = New OdbcDataAdapter("SELECT Cliente AS 'Cliente', Contacto AS 'Contacto', Departamento AS 'Departamento', CorreoElectronico AS 'Correo Electrónico', TelOfi AS 'Teléfono de Oficina', TelCel AS 'Teléfono Celular', RazonSocial AS 'Razón Social', RFC AS 'RFC', Direccion AS 'Dirección', CodigoPostal AS 'Código Postal' FROM gestióndeproyectossicma.clientes", cn)
            Dim dt = New DataTable
            da.Fill(dt)
            DGC.DataSource = dt
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Function busquedaxletra(ByVal busqueda As String) As DataTable
        Dim dt As New DataTable
        Dim da As New OdbcDataAdapter("SELECT Cliente AS 'Cliente', Contacto AS 'Contacto', Departamento AS 'Departamento', CorreoElectronico AS 'Correo Eletrónico', TelOfi AS 'Teléfono de Oficina', TelCel AS 'Telefono Celular', RazonSocial AS 'Razón Social', RFC AS 'RFC', Direccion AS 'Dirección', CodigoPostal AS 'Código Postal' FROM gestióndeproyectossicma.clientes WHERE Cliente LIKE '%" & busqueda & "%' " & "OR Contacto LIKE '%" & busqueda & "%'", cn)
        da.Fill(dt)
        Return dt
    End Function

    Private Sub BusquedaLetra_TextChanged(sender As Object, e As EventArgs) Handles BusquedaLetra.TextChanged
        If busquedaxletra(BusquedaLetra.Text).Rows.Count > 0 Then
            DGC.DataSource = busquedaxletra(BusquedaLetra.Text)
        End If
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Try
            Dim da = New OdbcDataAdapter("SELECT Cliente AS 'Cliente', Contacto AS 'Contacto', Departamento AS 'Departamento', CorreoElectronico AS 'Correo Electrónico', TelOfi AS 'Teléfono de Oficina', TelCel AS 'Teléfono Celular', RazonSocial AS 'Razón Social', RFC AS 'RFC', Direccion AS 'Dirección', CodigoPostal AS 'Código Postal' FROM gestióndeproyectossicma.clientes", cn)
            Dim dt = New DataTable
            da.Fill(dt)
            DGC.DataSource = dt
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim SAVE As New SaveFileDialog
        Dim ruta As String
        Dim xlApp As Object = CreateObject("Excel.Application")
        Dim pth As String = ""
        'crearemos una nueva hoja de calculo
        Dim xlwb As Object = xlApp.WorkBooks.add
        Dim xlws As Object = xlwb.WorkSheets(1)
        Try
            'exportaremos los caracteres de las columnas
            For c As Integer = 0 To DGC.Columns.Count - 1
                xlws.cells(1, c + 1).value = DGC.Columns(c).HeaderText
            Next
            'exportaremos las cabeceras de las calumnas
            For r As Integer = 0 To DGC.RowCount - 1
                For c As Integer = 0 To DGC.Columns.Count - 1
                    xlws.cells(r + 2, c + 1).value = Convert.ToString(DGC.Item(c, r).Value)
                Next
            Next
            'guardamos la hoja de excel en la ruta especifica
            Dim SaveFileDialog1 As SaveFileDialog = New SaveFileDialog
            SaveFileDialog1.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            SaveFileDialog1.Filter = "Archivo Excel| *.xlsx"
            SaveFileDialog1.FilterIndex = 2
            If SaveFileDialog1.ShowDialog = DialogResult.OK Then
                ruta = SaveFileDialog1.FileName
                xlwb.saveas(ruta)
                xlws = Nothing
                xlwb = Nothing
                xlApp.quit()
                MsgBox("Exportado Correctamente", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class