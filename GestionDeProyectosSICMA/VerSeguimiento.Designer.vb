﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class VerSeguimiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(VerSeguimiento))
        Me.DGS = New System.Windows.Forms.DataGridView()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.BusquedaLetraS = New System.Windows.Forms.TextBox()
        Me.SeguimientoG = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.MensajeSeguimiento = New System.Windows.Forms.Label()
        Me.Prioridad = New System.Windows.Forms.ComboBox()
        Me.P = New System.Windows.Forms.Label()
        Me.Cliente = New System.Windows.Forms.ComboBox()
        Me.Clien = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.DGS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGS
        '
        Me.DGS.AllowUserToAddRows = False
        Me.DGS.AllowUserToDeleteRows = False
        Me.DGS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DGS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGS.Location = New System.Drawing.Point(12, 86)
        Me.DGS.Name = "DGS"
        Me.DGS.ReadOnly = True
        Me.DGS.RowHeadersVisible = False
        Me.DGS.Size = New System.Drawing.Size(1160, 563)
        Me.DGS.TabIndex = 81
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label27.Location = New System.Drawing.Point(10, 52)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(71, 23)
        Me.Label27.TabIndex = 80
        Me.Label27.Text = "N. Job:"
        '
        'BusquedaLetraS
        '
        Me.BusquedaLetraS.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BusquedaLetraS.Location = New System.Drawing.Point(87, 50)
        Me.BusquedaLetraS.Name = "BusquedaLetraS"
        Me.BusquedaLetraS.Size = New System.Drawing.Size(75, 30)
        Me.BusquedaLetraS.TabIndex = 79
        '
        'SeguimientoG
        '
        Me.SeguimientoG.AutoSize = True
        Me.SeguimientoG.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SeguimientoG.ForeColor = System.Drawing.Color.RoyalBlue
        Me.SeguimientoG.Location = New System.Drawing.Point(442, 9)
        Me.SeguimientoG.Name = "SeguimientoG"
        Me.SeguimientoG.Size = New System.Drawing.Size(203, 38)
        Me.SeguimientoG.TabIndex = 82
        Me.SeguimientoG.Text = "Seguimiento"
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Black
        Me.Button8.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button8.Location = New System.Drawing.Point(1062, 9)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(110, 60)
        Me.Button8.TabIndex = 83
        Me.Button8.Text = "Actualizar"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'MensajeSeguimiento
        '
        Me.MensajeSeguimiento.AutoSize = True
        Me.MensajeSeguimiento.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensajeSeguimiento.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MensajeSeguimiento.Location = New System.Drawing.Point(645, 9)
        Me.MensajeSeguimiento.Name = "MensajeSeguimiento"
        Me.MensajeSeguimiento.Size = New System.Drawing.Size(0, 38)
        Me.MensajeSeguimiento.TabIndex = 98
        '
        'Prioridad
        '
        Me.Prioridad.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Prioridad.FormattingEnabled = True
        Me.Prioridad.Items.AddRange(New Object() {"1-En proceso", "2-Cotizando", "3-Cotizado/Dar seguimiento", "4-Cotizado/Detenido", "5-Por cotizar/Venta poco probable", "6-Pendientes administrativos", "0-CERRADO", "Cotizando-Proyecto"})
        Me.Prioridad.Location = New System.Drawing.Point(284, 50)
        Me.Prioridad.Name = "Prioridad"
        Me.Prioridad.Size = New System.Drawing.Size(317, 31)
        Me.Prioridad.TabIndex = 100
        Me.Prioridad.Text = "2-Cotizando"
        '
        'P
        '
        Me.P.AutoSize = True
        Me.P.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.P.ForeColor = System.Drawing.Color.RoyalBlue
        Me.P.Location = New System.Drawing.Point(183, 51)
        Me.P.Name = "P"
        Me.P.Size = New System.Drawing.Size(95, 23)
        Me.P.TabIndex = 99
        Me.P.Text = "Prioridad:"
        '
        'Cliente
        '
        Me.Cliente.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cliente.FormattingEnabled = True
        Me.Cliente.Location = New System.Drawing.Point(692, 48)
        Me.Cliente.Name = "Cliente"
        Me.Cliente.Size = New System.Drawing.Size(317, 31)
        Me.Cliente.TabIndex = 101
        '
        'Clien
        '
        Me.Clien.AutoSize = True
        Me.Clien.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clien.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Clien.Location = New System.Drawing.Point(609, 56)
        Me.Clien.Name = "Clien"
        Me.Clien.Size = New System.Drawing.Size(77, 23)
        Me.Clien.TabIndex = 102
        Me.Clien.Text = "Cliente:"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Black
        Me.Button1.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button1.Location = New System.Drawing.Point(897, 9)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(112, 33)
        Me.Button1.TabIndex = 103
        Me.Button1.Text = "Exportar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'VerSeguimiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1184, 661)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Cliente)
        Me.Controls.Add(Me.Clien)
        Me.Controls.Add(Me.Prioridad)
        Me.Controls.Add(Me.P)
        Me.Controls.Add(Me.MensajeSeguimiento)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.SeguimientoG)
        Me.Controls.Add(Me.DGS)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.BusquedaLetraS)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "VerSeguimiento"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.DGS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DGS As DataGridView
    Friend WithEvents Label27 As Label
    Friend WithEvents BusquedaLetraS As TextBox
    Friend WithEvents SeguimientoG As Label
    Friend WithEvents Button8 As Button
    Public WithEvents MensajeSeguimiento As Label
    Friend WithEvents Prioridad As ComboBox
    Friend WithEvents P As Label
    Friend WithEvents Cliente As ComboBox
    Friend WithEvents Clien As Label
    Friend WithEvents Button1 As Button
End Class
