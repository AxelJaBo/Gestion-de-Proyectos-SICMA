﻿Imports System.Data.Odbc

Public Class VerSeguimiento
    Dim Hoy As Date = DateTime.Now.ToString("yyyy/MM/dd")
    Dim Antes As Date = DateTime.Now.AddDays(-5).ToString("yyyy/MM/dd")
    Private Sub VerSeguimiento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call conectar()
        Try
            Dim davis2 = New OdbcDataAdapter("SELECT FechaSolicitudCotizacion AS 'Fecha Solicitud', TIMESTAMPDIFF(DAY, FechaSolicitudCotizacion , CURDATE()) AS 'Días', Job AS 'Job', Prioridad AS 'Prioridad', PO AS 'PO', FechaReciboPO AS 'Fecha PO', Cliente AS 'Cliente', Contactos AS 'Contacto', Categoria AS 'Categoria', Descripcion AS 'Descripción', FechaCompromisoEntrega AS 'Fecha Entrega', Cotizacion AS 'Cotización', MontoCotizado AS 'Monto', Moneda AS 'Moneda', Comentarios AS 'Comentarios', Nombre AS 'Nombre', lider AS 'Líder' FROM gestióndeproyectossicma.seguimiento", cn)
            Dim dtvis2 = New DataTable
            davis2.Fill(dtvis2)
            DGS.DataSource = dtvis2
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Function busquedaxletraS(ByVal busquedaS As String) As DataTable
        Dim dt As New DataTable
        Dim da As New OdbcDataAdapter("SELECT FechaSolicitudCotizacion AS 'Fecha Solicitud', TIMESTAMPDIFF(DAY, FechaSolicitudCotizacion , CURDATE()) AS 'Días', Job AS 'Job', Prioridad AS 'Prioridad', PO AS 'PO', FechaReciboPO AS 'Fecha PO', Cliente AS 'Cliente', Contactos AS 'Contacto', Categoria AS 'Categoria', Descripcion AS 'Descripción', FechaCompromisoEntrega AS 'Fecha Entrega', Cotizacion AS 'Cotización', MontoCotizado AS 'Monto', Moneda AS 'Moneda', Comentarios AS 'Comentarios', Nombre AS 'Nombre', lider AS 'Líder de Proyecto' FROM gestióndeproyectossicma.seguimiento WHERE Job LIKE '%" & busquedaS & "%'", cn)
        da.Fill(dt)
        Return dt
    End Function
    Private Sub BusquedaLetraS_TextChanged(sender As Object, e As EventArgs) Handles BusquedaLetraS.TextChanged
        If busquedaxletraS(BusquedaLetraS.Text).Rows.Count > 0 Then
            DGS.DataSource = busquedaxletraS(BusquedaLetraS.Text)
        End If
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Try
            Dim davis2 = New OdbcDataAdapter("SELECT FechaSolicitudCotizacion AS 'Fecha Solicitud', TIMESTAMPDIFF(DAY, FechaSolicitudCotizacion , CURDATE()) AS 'Días', Job AS 'Job', Prioridad AS 'Prioridad', PO AS 'PO', FechaReciboPO AS 'Fecha PO', Cliente AS 'Cliente', Contactos AS 'Contacto', Categoria AS 'Categoria', Descripcion AS 'Descripción', FechaCompromisoEntrega AS 'Fecha Entrega', Cotizacion AS 'Cotización', MontoCotizado AS 'Monto', Moneda AS 'Moneda', Comentarios AS 'Comentarios', Nombre AS 'Nombre', lider AS 'Líder' FROM gestióndeproyectossicma.seguimiento", cn)
            Dim dtvis2 = New DataTable
            davis2.Fill(dtvis2)
            DGS.DataSource = dtvis2
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Prioridad_TextChanged(sender As Object, e As EventArgs) Handles Prioridad.TextChanged
        If Prioridad.Text = "Cotizando-Proyecto" Then
            Try
                Dim davis2 = New OdbcDataAdapter("SELECT FechaSolicitudCotizacion AS 'Fecha Solicitud', TIMESTAMPDIFF(DAY, FechaSolicitudCotizacion , CURDATE()) AS 'Días', Job AS 'Job', Prioridad AS 'Prioridad', etapa AS 'Etapa', descripcionetapa AS 'Descrición de la Etapa', responsable AS 'Responsable', PO AS 'PO', FechaReciboPO AS 'Fecha PO', Cliente AS 'Cliente', Contactos AS 'Contacto', Categoria AS 'Categoria', Descripcion AS 'Descripción', FechaCompromisoEntrega AS 'Fecha Entrega', Cotizacion AS 'Cotización', MontoCotizado AS 'Monto', Moneda AS 'Moneda', Comentarios AS 'Comentarios', Nombre AS 'Nombre', lider AS 'Líder de Proyecto' FROM gestióndeproyectossicma.seguimiento WHERE Prioridad= '2-Cotizando' AND Categoria='Proyecto'", cn)
                Dim dtvis2 = New DataTable
                davis2.Fill(dtvis2)
                DGS.DataSource = dtvis2
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            Try
                Dim davis2 = New OdbcDataAdapter("SELECT FechaSolicitudCotizacion AS 'Fecha Solicitud', TIMESTAMPDIFF(DAY, FechaSolicitudCotizacion , CURDATE()) AS 'Días', Job AS 'Job', Prioridad AS 'Prioridad', PO AS 'PO', FechaReciboPO AS 'Fecha PO', Cliente AS 'Cliente', Contactos AS 'Contacto', Categoria AS 'Categoria', Descripcion AS 'Descripción', FechaCompromisoEntrega AS 'Fecha Entrega', Cotizacion AS 'Cotización', MontoCotizado AS 'Monto', Moneda AS 'Moneda', Comentarios AS 'Comentarios', Nombre AS 'Nombre', lider AS 'Líder de Proyecto' FROM gestióndeproyectossicma.seguimiento WHERE Prioridad= '" & Prioridad.Text & "'", cn)
                Dim dtvis2 = New DataTable
                davis2.Fill(dtvis2)
                DGS.DataSource = dtvis2
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub Cliente_TextChanged(sender As Object, e As EventArgs) Handles Cliente.TextChanged
        Try
            Dim davis2 = New OdbcDataAdapter("SELECT FechaSolicitudCotizacion AS 'Fecha Solicitud', TIMESTAMPDIFF(DAY, FechaSolicitudCotizacion , CURDATE()) AS 'Días', Job AS 'Job', Prioridad AS 'Prioridad', PO AS 'PO', FechaReciboPO AS 'Fecha PO', Cliente AS 'Cliente', Contactos AS 'Contacto', Categoria AS 'Categoria', Descripcion AS 'Descripción', FechaCompromisoEntrega AS 'Fecha Entrega', Cotizacion AS 'Cotización', MontoCotizado AS 'Monto', Moneda AS 'Moneda', Comentarios AS 'Comentarios', Nombre AS 'Nombre', lider AS 'Líder de Proyecto' FROM gestióndeproyectossicma.seguimiento WHERE Cliente= '" & Cliente.Text & "'", cn)
            Dim dtvis2 = New DataTable
            davis2.Fill(dtvis2)
            DGS.DataSource = dtvis2
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Cliente_Click(sender As Object, e As EventArgs) Handles Cliente.Click
        Dim da = New OdbcDataAdapter("SELECT DISTINCT Cliente FROM gestióndeproyectossicma.clientes ORDER BY Cliente ASC", cn)
        Dim dt = New DataTable
        da.Fill(dt)
        Cliente.DataSource = dt
        Cliente.DisplayMember = "Cliente"
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim SAVE As New SaveFileDialog
        Dim ruta As String
        Dim xlApp As Object = CreateObject("Excel.Application")
        Dim pth As String = ""
        'crearemos una nueva hoja de calculo
        Dim xlwb As Object = xlApp.WorkBooks.add
        Dim xlws As Object = xlwb.WorkSheets(1)
        Try
            'exportaremos los caracteres de las columnas
            For c As Integer = 0 To DGS.Columns.Count - 1
                xlws.cells(1, c + 1).value = DGS.Columns(c).HeaderText
            Next
            'exportaremos las cabeceras de las calumnas
            For r As Integer = 0 To DGS.RowCount - 1
                For c As Integer = 0 To DGS.Columns.Count - 1
                    xlws.cells(r + 2, c + 1).value = Convert.ToString(DGS.Item(c, r).Value)
                Next
            Next
            'guardamos la hoja de excel en la ruta especifica
            Dim SaveFileDialog1 As SaveFileDialog = New SaveFileDialog
            SaveFileDialog1.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            SaveFileDialog1.Filter = "Archivo Excel| *.xlsx"
            SaveFileDialog1.FilterIndex = 2
            If SaveFileDialog1.ShowDialog = DialogResult.OK Then
                ruta = SaveFileDialog1.FileName
                xlwb.saveas(ruta)
                xlws = Nothing
                xlwb = Nothing
                xlApp.quit()
                MsgBox("Exportado Correctamente", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub DGS_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles DGS.CellFormatting
        If DGS.Columns(e.ColumnIndex).Name.Equals("Prioridad") Then
            If CStr(e.Value) = "1-En proceso" Then
                e.CellStyle.BackColor = Color.Pink
            End If
            If CStr(e.Value) = "6-Pendientes administrativos" Then
                e.CellStyle.BackColor = Color.Yellow
            End If
            If CStr(e.Value) = "0-CERRADO" Then
                e.CellStyle.BackColor = Color.DarkGray
            End If
        End If
        If DGS.Columns(e.ColumnIndex).Name.Equals("Fecha Entrega") Then
            If CDate(e.Value) = Hoy Then
                e.CellStyle.BackColor = Color.Red
            End If
            If CDate(e.Value) >= Antes And CDate(e.Value) < Hoy Then
                e.CellStyle.BackColor = Color.Yellow
            End If
        End If
    End Sub
End Class