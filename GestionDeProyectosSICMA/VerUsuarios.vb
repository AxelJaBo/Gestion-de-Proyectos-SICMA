﻿Imports System.Data.Odbc

Public Class VerUsuarios
    Private Sub VerUsuarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Realiza una consulta a la tabla de usuarios de la base de datos.'
        Llenar()
        Try
            Dim da = New OdbcDataAdapter("SELECT nombre AS 'Nombre', usuario AS 'Usuario', contraseña AS 'Contraseña', Tipo_Usuario AS 'Tipo de Usuario', control_user AS 'Control', CorreoE AS 'Email', TelCel AS 'Teléfono' FROM gestióndeproyectossicma.usuarios", cn)
            Dim dt = New DataTable
            da.Fill(dt)
            DGU.DataSource = dt
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub Llenar()
        Dim daP2 = New OdbcDataAdapter("SELECT idUsuarios, usuario FROM gestióndeproyectossicma.usuarios WHERE Tipo_Usuario= 'Proyectos' OR Tipo_Usuario='Ventas'", cn)
        Dim dtP2 = New DataTable
        daP2.Fill(dtP2)
        Usuario.DataSource = dtP2
        Usuario.DisplayMember = "usuario"
        Usuario.ValueMember = "idUsuarios"
    End Sub

    Function busquedaxletra(ByVal busqueda As String) As DataTable
        Dim dt As New DataTable
        Dim da As New OdbcDataAdapter("SELECT nombre AS 'Nombre', usuario AS 'Usuario', contraseña AS 'Contraseña', Tipo_Usuario AS 'Tipo de Usuario', control_user AS 'Control', CorreoE AS 'Email', TelCel AS 'Teléfono' FROM gestióndeproyectossicma.usuarios WHERE nombre LIKE '%" & busqueda & "%'", cn)
        da.Fill(dt)
        Return dt
    End Function
    Private Sub BusquedaLetra_TextChanged(sender As Object, e As EventArgs) Handles BusquedaLetra.TextChanged
        If busquedaxletra(BusquedaLetra.Text).Rows.Count > 0 Then
            DGU.DataSource = busquedaxletra(BusquedaLetra.Text)
        End If
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        'Realiza una consulta a la tabla de usuarios de la base de datos.'
        Llenar()
        Try
            Dim da = New OdbcDataAdapter("SELECT nombre AS 'Nombre', usuario AS 'Usuario', contraseña AS 'Contraseña', Tipo_Usuario AS 'Tipo de Usuario', control_user AS 'Control', CorreoE AS 'Email', TelCel AS 'Teléfono' FROM gestióndeproyectossicma.usuarios", cn)
            Dim dt = New DataTable
            da.Fill(dt)
            DGU.DataSource = dt
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim SAVE As New SaveFileDialog
        Dim ruta As String
        Dim xlApp As Object = CreateObject("Excel.Application")
        Dim pth As String = ""
        'crearemos una nueva hoja de calculo
        Dim xlwb As Object = xlApp.WorkBooks.add
        Dim xlws As Object = xlwb.WorkSheets(1)
        Try
            'exportaremos los caracteres de las columnas
            For c As Integer = 0 To DGU.Columns.Count - 1
                xlws.cells(1, c + 1).value = DGU.Columns(c).HeaderText
            Next
            'exportaremos las cabeceras de las calumnas
            For r As Integer = 0 To DGU.RowCount - 1
                For c As Integer = 0 To DGU.Columns.Count - 1
                    xlws.cells(r + 2, c + 1).value = Convert.ToString(DGU.Item(c, r).Value)
                Next
            Next
            'guardamos la hoja de excel en la ruta especifica
            Dim SaveFileDialog1 As SaveFileDialog = New SaveFileDialog
            SaveFileDialog1.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            SaveFileDialog1.Filter = "Archivo Excel| *.xlsx"
            SaveFileDialog1.FilterIndex = 2
            If SaveFileDialog1.ShowDialog = DialogResult.OK Then
                ruta = SaveFileDialog1.FileName
                xlwb.saveas(ruta)
                xlws = Nothing
                xlwb = Nothing
                xlApp.quit()
                MsgBox("Exportado Correctamente", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            MsgBox("La Sesión del Usuario se ha cerrado correctamente.")
            Dim controldeusuario As New OdbcCommand("UPDATE gestióndeproyectossicma.usuarios SET control_user='0' where usuario ='" & Usuario.Text.ToArray & "'", cn)
            controldeusuario.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Try
            Dim da = New OdbcDataAdapter("SELECT nombre AS 'Nombre', usuario AS 'Usuario', contraseña AS 'Contraseña', Tipo_Usuario AS 'Tipo de Usuario', control_user AS 'Control', CorreoE AS 'Email', TelCel AS 'Teléfono' FROM gestióndeproyectossicma.usuarios", cn)
            Dim dt = New DataTable
            da.Fill(dt)
            DGU.DataSource = dt
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class