﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VerVisitasP
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(VerVisitasP))
        Me.Label18 = New System.Windows.Forms.Label()
        Me.BusquedaLetraV = New System.Windows.Forms.TextBox()
        Me.DGVP = New System.Windows.Forms.DataGridView()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Clientes = New System.Windows.Forms.Label()
        Me.MensajeVisitas2 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.DGVP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label18.Location = New System.Drawing.Point(8, 60)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(177, 23)
        Me.Label18.TabIndex = 48
        Me.Label18.Text = "Busqueda General:"
        '
        'BusquedaLetraV
        '
        Me.BusquedaLetraV.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BusquedaLetraV.Location = New System.Drawing.Point(191, 53)
        Me.BusquedaLetraV.Name = "BusquedaLetraV"
        Me.BusquedaLetraV.Size = New System.Drawing.Size(250, 30)
        Me.BusquedaLetraV.TabIndex = 47
        '
        'DGVP
        '
        Me.DGVP.AllowUserToAddRows = False
        Me.DGVP.AllowUserToDeleteRows = False
        Me.DGVP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGVP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVP.Location = New System.Drawing.Point(12, 89)
        Me.DGVP.Name = "DGVP"
        Me.DGVP.ReadOnly = True
        Me.DGVP.RowHeadersVisible = False
        Me.DGVP.Size = New System.Drawing.Size(1159, 568)
        Me.DGVP.TabIndex = 46
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Black
        Me.Button8.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button8.Location = New System.Drawing.Point(1062, 12)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(110, 60)
        Me.Button8.TabIndex = 98
        Me.Button8.Text = "Actualizar"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Clientes
        '
        Me.Clientes.AutoSize = True
        Me.Clientes.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clientes.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Clientes.Location = New System.Drawing.Point(447, 12)
        Me.Clientes.Name = "Clientes"
        Me.Clientes.Size = New System.Drawing.Size(120, 38)
        Me.Clientes.TabIndex = 97
        Me.Clientes.Text = "Visitas"
        '
        'MensajeVisitas2
        '
        Me.MensajeVisitas2.AutoSize = True
        Me.MensajeVisitas2.Font = New System.Drawing.Font("Arial Black", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensajeVisitas2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.MensajeVisitas2.Location = New System.Drawing.Point(12, 9)
        Me.MensajeVisitas2.Name = "MensajeVisitas2"
        Me.MensajeVisitas2.Size = New System.Drawing.Size(0, 38)
        Me.MensajeVisitas2.TabIndex = 99
        Me.MensajeVisitas2.Visible = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Black
        Me.Button1.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Button1.Location = New System.Drawing.Point(944, 12)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(112, 33)
        Me.Button1.TabIndex = 105
        Me.Button1.Text = "Exportar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'VerVisitasP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1184, 661)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.MensajeVisitas2)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Clientes)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.BusquedaLetraV)
        Me.Controls.Add(Me.DGVP)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "VerVisitasP"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.DGVP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label18 As Label
    Friend WithEvents BusquedaLetraV As TextBox
    Friend WithEvents DGVP As DataGridView
    Friend WithEvents Button8 As Button
    Friend WithEvents Clientes As Label
    Public WithEvents MensajeVisitas2 As Label
    Friend WithEvents Button1 As Button
End Class
