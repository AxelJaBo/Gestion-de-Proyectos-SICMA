﻿Imports System.Data.Odbc

Public Class VerVisitasP
    Private Sub VerVisitasP_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Dim daac = New OdbcDataAdapter("SELECT Cod_Visita AS 'Código de Visita', fechavisita AS 'Fecha de Visita', fechaproxvisita AS 'Próxima Visita', vendedor AS 'Nombre', cliente AS 'Cliente', contacto AS 'Contacto', descripcion AS 'Descripción' FROM gestióndeproyectossicma.visitas WHERE vendedor='" & MensajeVisitas2.Text.ToArray & "'", cn)
            Dim dtac = New DataTable
            daac.Fill(dtac)
            DGVP.DataSource = dtac
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Function busquedaxletraV(ByVal busquedaV As String) As DataTable
        Dim dt As New DataTable
        Dim da As New OdbcDataAdapter("SELECT Cod_Visita AS 'Código de Visita', fechavisita AS 'Fecha de Visita', fechaproxvisita AS 'Próxima Visita', vendedor AS 'Nombre', cliente AS 'Cliente', contacto AS 'Contacto', descripcion AS 'Descripción' FROM gestióndeproyectossicma.visitas WHERE cliente LIKE '%" & busquedaV & "%' " & "AND vendedor='" & MensajeVisitas2.Text.ToArray & "'", cn)
        da.Fill(dt)
        Return dt
    End Function

    Private Sub BusquedaLetraV_TextChanged(sender As Object, e As EventArgs) Handles BusquedaLetraV.TextChanged
        If busquedaxletraV(BusquedaLetraV.Text).Rows.Count > 0 Then
            DGVP.DataSource = busquedaxletraV(BusquedaLetraV.Text)
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim SAVE As New SaveFileDialog
        Dim ruta As String
        Dim xlApp As Object = CreateObject("Excel.Application")
        Dim pth As String = ""
        'crearemos una nueva hoja de calculo
        Dim xlwb As Object = xlApp.WorkBooks.add
        Dim xlws As Object = xlwb.WorkSheets(1)
        Try
            'exportaremos los caracteres de las columnas
            For c As Integer = 0 To DGVP.Columns.Count - 1
                xlws.cells(1, c + 1).value = DGVP.Columns(c).HeaderText
            Next
            'exportaremos las cabeceras de las calumnas
            For r As Integer = 0 To DGVP.RowCount - 1
                For c As Integer = 0 To DGVP.Columns.Count - 1
                    xlws.cells(r + 2, c + 1).value = Convert.ToString(DGVP.Item(c, r).Value)
                Next
            Next
            'guardamos la hoja de excel en la ruta especifica
            Dim SaveFileDialog1 As SaveFileDialog = New SaveFileDialog
            SaveFileDialog1.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            SaveFileDialog1.Filter = "Archivo Excel| *.xlsx"
            SaveFileDialog1.FilterIndex = 2
            If SaveFileDialog1.ShowDialog = DialogResult.OK Then
                ruta = SaveFileDialog1.FileName
                xlwb.saveas(ruta)
                xlws = Nothing
                xlwb = Nothing
                xlApp.quit()
                MsgBox("Exportado Correctamente", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class